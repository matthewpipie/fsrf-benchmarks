#!/usr/bin/python3

from pathlib import Path
import itertools
import datetime
import subprocess
import os
import signal
import sys

logpath = Path(sys.argv[1])
logs = list(logpath.glob("*"))

csvf = Path("logs.csv")

with open(csvf, 'w') as csv:
	csv.write("# apps,bench,system,size,temp,total bytes,runtime 1,runtime 2,runtime 3,runtime 4\n")
	for log in logs:
		# log is a file object
		text = log.read_text()
		filename = log.name[:-4]
		benchmark_name = "_".join(filename.split("_")[:-1])
		run_num = filename.split("_")[-1]
		text_split = text.split("\n")
		relevant = [line for line in text_split if benchmark_name in line]
		#print(relevant)
		data = []
		seenSet = set()
		for i in range(0, len(relevant), 2):
			seen = relevant[i] in seenSet
			data.append((relevant[i] + " " + relevant[i+1], seen))
			seenSet.add(relevant[i])
		for run, seen in data:
			datas = run.split(" ")
			n_apps = datas[6]
			bench = datas[7]
			system = "Physical" if datas[4] == '2' else "FSRF DRAM" if datas[4] == '0' else "FSRF SRAM" if datas[5] == '2' else "Coyote 2M" if datas[5] == '1' else "Coyote 4K"
			size = datas[1]
			temp = "hot" if system == "Physical" or datas[3] == "1" else "mid" if seen else "cold"
			total_bytes = datas[9]
			runtime1 = datas[10]
			runtime2 = datas[11]
			runtime3 = datas[12]
			runtime4 = datas[13]
			csv.write(",".join([n_apps,bench,system,size,temp,total_bytes,runtime1,runtime2,runtime3,runtime4]) + "\n")
# ['./ptri', '31', '4', '0', '1', '1', '4', 'ptri', 'e2e', '380888545504', '109472', '109357', '109316', '109329', '3.32129', '3.31815']
