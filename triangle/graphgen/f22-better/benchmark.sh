#!/bin/bash
degrees="1 5 10 15 20 30 40 50 100"
logfsizes="18 20 22 24"
for deg in $degrees; do
	for fs in $logfsizes; do
		bash run_all.sh $deg $fs
	done
done
