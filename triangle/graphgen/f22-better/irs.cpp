#include "shared.h"

#define DEBUG 0

int main(int argc, char* argv[]) {
    Ret ret = open_graph(argv[1], "irs", [](uint64_t vertices, uint64_t edges) -> uint64_t { return (1 * vertices + edges * 2) * 8; });
    uint64_t &vertices = ret.vertices;
    uint64_t &edges = ret.edges;
    uint64_t &length = ret.length;

    uint64_t *graph = ret.ptr;

    printf("Starting counting\n");
    uint64_t num_tris = 0;
    uint64_t current_vertex = 0;

    for (uint64_t i = 0; i < length/8; i+=1) {
        uint64_t start1 = graph[i];
        if (start1 == UL_MAX) {
            current_vertex = 1 + i;
            continue;
        }
        //uint64_t end1 = graph[i+1];
        //if (DEBUG && start1 > end1) printf("Error %lu %lu %lu %lu\n", start1, end1, i, current_vertex);
        if (start1 < current_vertex) continue;
        for (uint64_t j = start1; ; j+=1) {
        //for (uint64_t j = start1; j < end1; j+=2) {
            uint64_t start2 = graph[j];
            if (start2 == UL_MAX) break;
            //uint64_t end2 = graph[j+1];
            //if (DEBUG && start2 > end2) printf("Error2\n");
            //if (DEBUG && start2 == UL_MAX || end2 == UL_MAX) printf("Error4\n");
            if (start2 < start1) continue;
            uint64_t k = start2;
            for (uint64_t k = start2; ; k+=1) {
                uint64_t start3 = graph[k];
                if (start3 == UL_MAX) break;
                //uint64_t end3 = graph[k+1];
                //if (DEBUG && start3 > end3) printf("Error3\n");
                //if (DEBUG && start3 == UL_MAX || end3 == UL_MAX) printf("Error4\n");
                if (start3 == current_vertex) {
                    num_tris++; break;
                }
            }
        }
    }

    printf("Done counting, found %ld triangles.\n", num_tris);

    return 0;
}
