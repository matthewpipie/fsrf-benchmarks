#!/bin/bash
set -e
deg="$1"
siz="$2"
log="logs/$deg-$siz.log"
./gen $deg $siz 2>&1 | tee tmp
outfile=`grep Filename: tmp | cut -c11-`
rm tmp
touch "$log"
sync; echo 3 > /proc/sys/vm/drop_caches
echo "do_csr 0" | tee -a "$log"
time ./do_csr $outfile |& tee -a "$log"
echo "do_csr 1" | tee -a "$log"
time ./do_csr $outfile |& tee -a "$log"
sync; echo 3 > /proc/sys/vm/drop_caches
echo "do_mrg 0" | tee -a "$log"
time ./do_mrg $outfile |& tee -a "$log"
echo "do_mrg 1" | tee -a "$log"
time ./do_mrg $outfile |& tee -a "$log"
sync; echo 3 > /proc/sys/vm/drop_caches
echo "do_irs 0" | tee -a "$log"
time ./do_irs $outfile |& tee -a "$log"
echo "do_irs 1" | tee -a "$log"
time ./do_irs $outfile |& tee -a "$log"
