#include "shared.h"

int main(int argc, char* argv[]) {
    Ret ret = open_graph(argv[1], "csr", [](uint64_t vertices, uint64_t edges) -> uint64_t { return (vertices + 1 + edges * 2) * 8; });
    uint64_t &vertices = ret.vertices;
    uint64_t &edges = ret.edges;
    uint64_t &length = ret.length;

    uint64_t *graph_headers = ret.ptr;
    uint64_t *graph_edgelist = &graph_headers[vertices+1];

    printf("Starting counting\n");
    // Example triangle 3-4-5 on https://www.researchgate.net/figure/An-example-of-the-compressed-sparse-row-CSR-format-For-this-graph-at-least-three_fig1_304163670
    uint64_t num_tris = 0;
    for (uint64_t i = 0; i < vertices; i++) { // 3
        uint64_t start_edgelist1 = graph_headers[i]; // 8
        uint64_t end_edgelist1 = graph_headers[i+1]; // 11
        for (uint64_t edge_ind1 = start_edgelist1; edge_ind1 < end_edgelist1; edge_ind1++) {
            uint64_t elem = graph_edgelist[edge_ind1]; // 4
            if (elem < i) continue;
            uint64_t start_edgelist2 = graph_headers[elem]; // 11 (can't be reused bc not always consecutive edges)
            uint64_t end_edgelist2 = graph_headers[elem+1]; // 14
            for (uint64_t edge_ind2 = start_edgelist2; edge_ind2 < end_edgelist2; edge_ind2++) {
                uint64_t elem2 = graph_edgelist[edge_ind2]; // 5
                if (elem2 < elem) continue;
                uint64_t start_edgelist3 = graph_headers[elem2]; // 14
                uint64_t end_edgelist3 = graph_headers[elem2+1]; // 16
                for (uint64_t edge_ind3 = start_edgelist3; edge_ind3 < end_edgelist3; edge_ind3++) {
                    if (graph_edgelist[edge_ind3] == i) {
                        num_tris++;
                        break;
                    }
                }
            }
        }
    }

    printf("Done counting, found %ld triangles.\n", num_tris);

    return 0;
}
