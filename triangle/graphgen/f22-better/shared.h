#include <stdio.h>
#include <algorithm>
#include <stdlib.h>
#include <stdint.h>
#include <vector>
#include <unordered_set>
#include <set>
#include <iostream>
#include <limits>
#include <fcntl.h>
#include <sys/mman.h>
#include <cstring>
#include <unistd.h>
#include <functional>
using namespace std;

const uint64_t UL_MAX = 0xFFFFFFFFFFFFFFFF;

struct Ret {
	uint64_t vertices;
	uint64_t edges;
	uint64_t length;
	uint64_t *ptr;
};

Ret open_graph(char * base_file, char * extension, function<uint64_t(uint64_t, uint64_t)> compute_length) {
    Ret ret;
    char buffer[128] = {0};
    snprintf(buffer, sizeof(buffer), "%s.%s", base_file, extension);

    // load the metadata file
    int fd = open(base_file, O_RDONLY);
    if (fd < 0) {
        std::cout << "Error reading from file: " << fd << std::endl;
        exit(1);
    }

    uint64_t length_met = 4 * sizeof(uint64_t);
    uint64_t *fm_met = (uint64_t*)mmap(NULL, length_met, PROT_READ, MAP_SHARED, fd, 0);
    ret.vertices = fm_met[2];
    ret.edges = fm_met[3];
    munmap((void*)fm_met, length_met);

    // read actual graph file
    fd = open(buffer, O_RDONLY);
    if (fd < 0) {
        std::cout << "Error reading from file: " << fd << std::endl;
        exit(2);
    }
    ret.length = compute_length(ret.vertices, ret.edges);
    ret.ptr = (uint64_t*)mmap(NULL, ret.length, PROT_READ, MAP_SHARED, fd, 0);
    return ret;
}
