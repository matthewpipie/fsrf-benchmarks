#include <stdio.h>
#include <algorithm>
#include <stdlib.h>
#include <stdint.h>
#include <vector>
#include <unordered_set>
#include <set>
#include <iostream>
#include <limits>
#include <fcntl.h>
#include <sys/mman.h>
#include <cstring>
#include <unistd.h>

const uint64_t UL_MAX = 0xFFFFFFFFFFFFFFFF;

int main(int argc, char* argv[]) {
    char* p;
    uint64_t padded_vertices = strtoul(argv[1], &p, 10);
    uint64_t edges = strtoul(argv[2], &p, 10);

    // load the file
    int fd = open(argv[3], O_RDONLY);
    if (fd < 0) {
        std::cout << "Error reading from file: " << fd << std::endl;
        return 0;
    }
    
    uint64_t length = (padded_vertices + edges * 2) * 8;

    std::cout << "Opened file successfully, bytes = " << length << std::endl;

    typedef uint64_t data_t ;
    void * fm = mmap(NULL, length, PROT_READ, MAP_SHARED, fd, 0);
    data_t *graph_headers = (data_t*)fm;
    data_t *graph_edgelist = &graph_headers[padded_vertices];

    // Conversion

    printf("Starting conversion\n");
    // Example triangle 3-4-5 on https://www.researchgate.net/figure/An-example-of-the-compressed-sparse-row-CSR-format-For-this-graph-at-least-three_fig1_304163670
    //
    FILE* outfd = fopen(argv[4], "w+");
    
    for (uint64_t i = 0; i < padded_vertices; i++) {
        fprintf(outfd, "vertex_array[%llu] = %llu;\n", i, graph_headers[i]);
    }
    for (uint64_t i = 0; i < edges*2; i++) {
        fprintf(outfd, "edgelist_array[%llu] = %llu;\n", i, graph_edgelist[i]);
    }

    printf("Done\n");

    return 0;
}
