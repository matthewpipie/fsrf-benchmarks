#include <stdio.h>
#include <algorithm>
#include <stdlib.h>
#include <stdint.h>
#include <vector>
#include <unordered_set>
#include <set>
#include <iostream>
#include <limits>
#include <fcntl.h>
#include <sys/mman.h>
#include <cstring>
#include <unistd.h>

#define DEBUG 0

#define UNORDY
#ifdef UNORDY
typedef std::unordered_set<uint64_t> myset;
#else
typedef std::set<uint64_t> myset;
#endif

uint64_t separator = 0;

const uint64_t UL_MAX = 0xFFFFFFFFFFFFFFFF;

void * create_open(char *fname, uint64_t length) {
    int fd = creat(fname, 0600);
    if (fd < 0) {
        std::cout << "Error creating file: " << fd << std::endl;
        return 0;
    }
    std::cout << "Opened file successfully" << std::endl;

    lseek(fd, length, SEEK_SET);
    const char buf[1] = {0};
    write(fd, (void*)buf, 1);
    fsync(fd);
    lseek(fd, 0, SEEK_SET);
    close(fd);

    fd = open(fname, O_RDWR);
    if (fd < 0) {
        std::cout << "Error writing to file: " << fd << std::endl;
        return 0;
    }
    std::cout << "Opened file successfully, bytes = " << length << std::endl;

    //AddrMCB * fm = (AddrMCB *)mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    void * fm = mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    return fm;
}

int main(int argc, char* argv[]) {
    char* p;
    uint64_t vertices = strtoul(argv[1], &p, 10);
    uint64_t edges = strtoul(argv[2], &p, 10);
    //uint64_t target_deg = strtoul(argv[1], &p, 10);
    //uint64_t vertices = strtoul(argv[1], &p, 10);
    //uint64_t log_filesize = strtoul(argv[2], &p, 10);
    //FILE* fp = fopen("mem_init.hex", "w+");

    //uint64_t filesize = 1 << log_filesize;

    //uint64_t vertices = filesize / (16 + 32 * target_deg/2); // 16 bytes per vertex, 32 bytes per edge

    //uint64_t edges = target_deg * vertices / 2;

    //printf("%lu vertices, %lu log_filesize\n", vertices, log_filesize);
    printf("%lu vertices, %lu edges\n", vertices, edges);

    std::vector<myset> graph(vertices, myset());
    std::vector<uint64_t> deg(vertices);

    // generate graph
    uint64_t n_full_vertices = 0;
    srand(12344321);
    uint64_t e = 0;
    uint64_t num_tris = 0;
    while (e < edges) {
        uint64_t src = rand() % vertices;
        uint64_t dest = rand() % vertices;
        if (src != dest && graph[dest].find(src) == graph[dest].end()) {
            // see if it would add a triangle

            #if DEBUG
            for (const auto& elem : graph[dest]) {
                
                //if (graph[elem].contains(src)) {
                auto search = graph[elem].find(src);
                if (search != graph[elem].end()) {
                    num_tris++;
                }
            }
            #endif


            // add the edge
            e++;
            graph[src].insert(dest);
            graph[dest].insert(src);
            deg[src]++;
            deg[dest]++;
            if (graph[src].size() == vertices-1) n_full_vertices++;
            if (graph[dest].size() == vertices-1) n_full_vertices++;
            if (n_full_vertices == vertices) {
                printf("Error: all %ld vertices have %ld edges\n", vertices, graph[src].size());
                exit(1);
            }
        }
    }

    printf("Found %lu triangles\n", num_tris);

    std::vector<uint64_t> graph_csr_headers;
    std::vector<uint64_t> graph_csr_edgelist;

    for (uint64_t i = 0; i < vertices; i++) {
        graph_csr_headers.push_back(graph_csr_edgelist.size());
        myset &this_edgelist = graph[i];
#ifdef UNORDY
        // need to sort list
        std::vector<uint64_t> this_edgelist_sorted;
        this_edgelist_sorted.insert(this_edgelist_sorted.end(), this_edgelist.begin(), this_edgelist.end());
        std::sort(this_edgelist_sorted.begin(), this_edgelist_sorted.end());
        graph_csr_edgelist.insert(graph_csr_edgelist.end(), this_edgelist_sorted.begin(), this_edgelist_sorted.end());
#else
        graph_csr_edgelist.insert(graph_csr_edgelist.end(), this_edgelist.begin(), this_edgelist.end());
#endif
    }
    graph_csr_headers.push_back(graph_csr_edgelist.size());

    std::vector<uint64_t> graph_csr;
    graph_csr.insert(graph_csr.end(), graph_csr_headers.begin(), graph_csr_headers.end());
    graph_csr.insert(graph_csr.end(), graph_csr_edgelist.begin(), graph_csr_edgelist.end());


    // CSR done, now my turn

    std::vector<uint64_t> graph_mrg;

    uint64_t offset = 0;

    // calculate vertex offsets

    std::vector<uint64_t> vert_to_addr(vertices);
    for (uint64_t i = 0; i < vertices; i++) {
        vert_to_addr[i] = offset;
        offset += deg[i]*2+2;
    }

    // write to output

    for (uint64_t i = 0; i < vertices; i++) {
        myset &this_edgelist = graph[i];
        // need to sort list
        std::vector<uint64_t> this_edgelist_sorted;
        this_edgelist_sorted.insert(this_edgelist_sorted.end(), this_edgelist.begin(), this_edgelist.end());
#ifdef UNORDY
        std::sort(this_edgelist_sorted.begin(), this_edgelist_sorted.end());
#endif

        for (uint64_t i = 0; i < this_edgelist_sorted.size(); i++) {
            uint64_t elem = this_edgelist_sorted[i];
            uint64_t neighbor_start = vert_to_addr[elem];
            uint64_t neighbor_end = neighbor_start + deg[elem]*2;
            graph_mrg.push_back(neighbor_start);
            graph_mrg.push_back(neighbor_end);
            if (neighbor_start > neighbor_end) printf("ERROR\n");
        }
        graph_mrg.push_back(UL_MAX);
        graph_mrg.push_back(UL_MAX);
    }

    // now my turn #2

    std::vector<uint64_t> graph_irs;

    offset = 0;
    for (uint64_t i = 0; i < vertices; i++) {
        vert_to_addr[i] = offset;
        offset += deg[i]*1+1;
    }

    for (uint64_t i = 0; i < vertices; i++) {
        myset &this_edgelist = graph[i];
        // need to sort list
        std::vector<uint64_t> this_edgelist_sorted;
        this_edgelist_sorted.insert(this_edgelist_sorted.end(), this_edgelist.begin(), this_edgelist.end());
#ifdef UNORDY
        std::sort(this_edgelist_sorted.begin(), this_edgelist_sorted.end());
#endif

        for (uint64_t i = 0; i < this_edgelist_sorted.size(); i++) {
            uint64_t elem = this_edgelist_sorted[i];
            uint64_t neighbor_start = vert_to_addr[elem];
            //uint64_t neighbor_end = neighbor_start + deg[elem]*2;
            graph_irs.push_back(neighbor_start);
            //graph_mrg.push_back(neighbor_end);
            //if (neighbor_start > neighbor_end) printf("ERROR\n");
        }
        graph_irs.push_back(UL_MAX);
        //graph_mrg.push_back(UL_MAX);
    }



    // make output files
    uint64_t length_csr = sizeof(uint64_t) * (graph_csr.size());
    uint64_t length_mrg = sizeof(uint64_t) * (graph_mrg.size());
    uint64_t length_irs = sizeof(uint64_t) * (graph_irs.size());

    uint64_t * f_csr = (uint64_t*)create_open(argv[3], length_csr);
    uint64_t * f_mrg = (uint64_t*)create_open(argv[4], length_mrg);
    uint64_t * f_irs = (uint64_t*)create_open(argv[5], length_irs);

    std::memcpy(f_csr, graph_csr.data(), length_csr);
    std::memcpy(f_mrg, graph_mrg.data(), length_mrg);
    std::memcpy(f_irs, graph_irs.data(), length_irs);

    munmap(f_csr, length_csr);
    munmap(f_mrg, length_mrg);
    munmap(f_irs, length_irs);

    if (vertices <= 100) {
        for (uint64_t i = 0; i < vertices; i++) {
            printf("Vertex %ld: ", i);
            for (const auto& elem: graph[i]) {
                printf("%ld, ", elem);
            }
            printf("\n");
        }
    }
    printf("Average Degree: %lu\n", 2 * edges / vertices);

    return 0;
}
