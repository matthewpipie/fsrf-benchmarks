#include <stdio.h>
#include <algorithm>
#include <stdlib.h>
#include <stdint.h>
#include <vector>
#include <unordered_set>
#include <set>
#include <iostream>
#include <limits>
#include <fcntl.h>
#include <sys/mman.h>
#include <cstring>
#include <unistd.h>

const uint64_t UL_MAX = 0xFFFFFFFFFFFFFFFF;

int main(int argc, char* argv[]) {
    char* p;
    uint64_t vertices = strtoul(argv[1], &p, 10);
    uint64_t edges = strtoul(argv[2], &p, 10);
    //uint64_t target_deg = strtoul(argv[1], &p, 10);
    //uint64_t vertices = strtoul(argv[1], &p, 10);
    //uint64_t log_filesize = strtoul(argv[2], &p, 10);
    //FILE* fp = fopen("mem_init.hex", "w+");
    uint64_t padded_vertices = strtoul(argv[3], &p, 10);

    // load the file
    int fd = open(argv[4], O_RDONLY);
    if (fd < 0) {
        std::cout << "Error reading from file: " << fd << std::endl;
        return 0;
    }
    
    typedef uint64_t data_t;
    uint64_t length = (padded_vertices + edges * 2) * sizeof(data_t);

    std::cout << "Opened file successfully, bytes = " << length << std::endl;

    void * fm = mmap(NULL, length, PROT_READ, MAP_SHARED, fd, 0);
    data_t *graph_headers = (data_t*)fm;
    data_t *graph_edgelist = &graph_headers[padded_vertices];

    // now we count on fm

    printf("Starting counting\n");
    // Example triangle 3-4-5 on https://www.researchgate.net/figure/An-example-of-the-compressed-sparse-row-CSR-format-For-this-graph-at-least-three_fig1_304163670
    uint64_t num_tris = 0;
    for (uint64_t i = 0; i < vertices; i++) { // 3
        uint64_t start_edgelist1 = graph_headers[i]; // 8
        uint64_t end_edgelist1 = graph_headers[i+1]; // 11
        for (uint64_t edge_ind1 = start_edgelist1; edge_ind1 < end_edgelist1; edge_ind1++) {
            uint64_t elem = graph_edgelist[edge_ind1]; // 4
            if (elem <= i) continue;
            uint64_t start_edgelist2 = graph_headers[elem]; // 11 (can't be reused bc not always consecutive edges)
            uint64_t end_edgelist2 = graph_headers[elem+1]; // 14
            for (uint64_t edge_ind2 = start_edgelist2; edge_ind2 < end_edgelist2; edge_ind2++) {
                uint64_t elem2 = graph_edgelist[edge_ind2]; // 5
                if (elem2 <= elem) continue;
                uint64_t start_edgelist3 = graph_headers[elem2]; // 14
                uint64_t end_edgelist3 = graph_headers[elem2+1]; // 16
                for (uint64_t edge_ind3 = start_edgelist3; edge_ind3 < end_edgelist3; edge_ind3++) {
                    if (graph_edgelist[edge_ind3] == i) {
                        num_tris++;
                        //std::cout << i << " " << elem << " " << elem2 << std::endl;
                        break;
                    }
                }
            }
        }
    }

    printf("Done counting, found %ld triangles.\n", num_tris);

    return 0;
}
