`ifndef USER_PARAMS_SV_INCLUDED
`define USER_PARAMS_SV_INCLUDED
                        
package UserParams;

parameter NUM_APPS = 4;
parameter ALL_APPS_SAME = 0;
parameter CONFIG_APPS = 12;

endpackage
`endif 
