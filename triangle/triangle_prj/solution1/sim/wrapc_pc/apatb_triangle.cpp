#include <systemc>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <stdint.h>
#include "SysCFileHandler.h"
#include "ap_int.h"
#include "ap_fixed.h"
#include <complex>
#include <stdbool.h>
#include "autopilot_cbe.h"
#include "hls_stream.h"
#include "hls_half.h"
#include "hls_signal_handler.h"

using namespace std;
using namespace sc_core;
using namespace sc_dt;

// wrapc file define:
#define AUTOTB_TVIN_mem1 "../tv/cdatafile/c.triangle.autotvin_mem1.dat"
#define AUTOTB_TVOUT_mem1 "../tv/cdatafile/c.triangle.autotvout_mem1.dat"
// wrapc file define:
#define AUTOTB_TVIN_mem2 "../tv/cdatafile/c.triangle.autotvin_mem2.dat"
#define AUTOTB_TVOUT_mem2 "../tv/cdatafile/c.triangle.autotvout_mem2.dat"
// wrapc file define:
#define AUTOTB_TVIN_mem3 "../tv/cdatafile/c.triangle.autotvin_mem3.dat"
#define AUTOTB_TVOUT_mem3 "../tv/cdatafile/c.triangle.autotvout_mem3.dat"
// wrapc file define:
#define AUTOTB_TVIN_gmem "../tv/cdatafile/c.triangle.autotvin_gmem.dat"
#define AUTOTB_TVOUT_gmem "../tv/cdatafile/c.triangle.autotvout_gmem.dat"
// wrapc file define:
#define AUTOTB_TVIN_mem1_offset "../tv/cdatafile/c.triangle.autotvin_mem1_offset.dat"
#define AUTOTB_TVOUT_mem1_offset "../tv/cdatafile/c.triangle.autotvout_mem1_offset.dat"
// wrapc file define:
#define AUTOTB_TVIN_mem2_offset "../tv/cdatafile/c.triangle.autotvin_mem2_offset.dat"
#define AUTOTB_TVOUT_mem2_offset "../tv/cdatafile/c.triangle.autotvout_mem2_offset.dat"
// wrapc file define:
#define AUTOTB_TVIN_mem3_offset "../tv/cdatafile/c.triangle.autotvin_mem3_offset.dat"
#define AUTOTB_TVOUT_mem3_offset "../tv/cdatafile/c.triangle.autotvout_mem3_offset.dat"
// wrapc file define:
#define AUTOTB_TVIN_len_in_big_words "../tv/cdatafile/c.triangle.autotvin_len_in_big_words.dat"
#define AUTOTB_TVOUT_len_in_big_words "../tv/cdatafile/c.triangle.autotvout_len_in_big_words.dat"
// wrapc file define:
#define AUTOTB_TVIN_outs "../tv/cdatafile/c.triangle.autotvin_outs.dat"
#define AUTOTB_TVOUT_outs "../tv/cdatafile/c.triangle.autotvout_outs.dat"
// wrapc file define:
#define AUTOTB_TVOUT_return "../tv/cdatafile/c.triangle.autotvout_ap_return.dat"

#define INTER_TCL "../tv/cdatafile/ref.tcl"

// tvout file define:
#define AUTOTB_TVOUT_PC_mem1 "../tv/rtldatafile/rtl.triangle.autotvout_mem1.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_mem2 "../tv/rtldatafile/rtl.triangle.autotvout_mem2.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_mem3 "../tv/rtldatafile/rtl.triangle.autotvout_mem3.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_gmem "../tv/rtldatafile/rtl.triangle.autotvout_gmem.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_mem1_offset "../tv/rtldatafile/rtl.triangle.autotvout_mem1_offset.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_mem2_offset "../tv/rtldatafile/rtl.triangle.autotvout_mem2_offset.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_mem3_offset "../tv/rtldatafile/rtl.triangle.autotvout_mem3_offset.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_len_in_big_words "../tv/rtldatafile/rtl.triangle.autotvout_len_in_big_words.dat"
// tvout file define:
#define AUTOTB_TVOUT_PC_outs "../tv/rtldatafile/rtl.triangle.autotvout_outs.dat"
#define AUTOTB_TVOUT_PC_return "../tv/rtldatafile/rtl.triangle.autotvout_ap_return.dat"

class INTER_TCL_FILE {
  public:
INTER_TCL_FILE(const char* name) {
  mName = name; 
  mem1_depth = 0;
  mem2_depth = 0;
  mem3_depth = 0;
  gmem_depth = 0;
  mem1_offset_depth = 0;
  mem2_offset_depth = 0;
  mem3_offset_depth = 0;
  len_in_big_words_depth = 0;
  outs_depth = 0;
  return_depth = 0;
  trans_num =0;
}
~INTER_TCL_FILE() {
  mFile.open(mName);
  if (!mFile.good()) {
    cout << "Failed to open file ref.tcl" << endl;
    exit (1); 
  }
  string total_list = get_depth_list();
  mFile << "set depth_list {\n";
  mFile << total_list;
  mFile << "}\n";
  mFile << "set trans_num "<<trans_num<<endl;
  mFile.close();
}
string get_depth_list () {
  stringstream total_list;
  total_list << "{mem1 " << mem1_depth << "}\n";
  total_list << "{mem2 " << mem2_depth << "}\n";
  total_list << "{mem3 " << mem3_depth << "}\n";
  total_list << "{gmem " << gmem_depth << "}\n";
  total_list << "{mem1_offset " << mem1_offset_depth << "}\n";
  total_list << "{mem2_offset " << mem2_offset_depth << "}\n";
  total_list << "{mem3_offset " << mem3_offset_depth << "}\n";
  total_list << "{len_in_big_words " << len_in_big_words_depth << "}\n";
  total_list << "{outs " << outs_depth << "}\n";
  total_list << "{ap_return " << return_depth << "}\n";
  return total_list.str();
}
void set_num (int num , int* class_num) {
  (*class_num) = (*class_num) > num ? (*class_num) : num;
}
void set_string(std::string list, std::string* class_list) {
  (*class_list) = list;
}
  public:
    int mem1_depth;
    int mem2_depth;
    int mem3_depth;
    int gmem_depth;
    int mem1_offset_depth;
    int mem2_offset_depth;
    int mem3_offset_depth;
    int len_in_big_words_depth;
    int outs_depth;
    int return_depth;
    int trans_num;
  private:
    ofstream mFile;
    const char* mName;
};

static void RTLOutputCheckAndReplacement(std::string &AESL_token, std::string PortName) {
  bool no_x = false;
  bool err = false;

  no_x = false;
  // search and replace 'X' with '0' from the 3rd char of token
  while (!no_x) {
    size_t x_found = AESL_token.find('X', 0);
    if (x_found != string::npos) {
      if (!err) { 
        cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'X' on port" 
             << PortName << ", possible cause: There are uninitialized variables in the C design."
             << endl; 
        err = true;
      }
      AESL_token.replace(x_found, 1, "0");
    } else
      no_x = true;
  }
  no_x = false;
  // search and replace 'x' with '0' from the 3rd char of token
  while (!no_x) {
    size_t x_found = AESL_token.find('x', 2);
    if (x_found != string::npos) {
      if (!err) { 
        cerr << "WARNING: [SIM 212-201] RTL produces unknown value 'x' on port" 
             << PortName << ", possible cause: There are uninitialized variables in the C design."
             << endl; 
        err = true;
      }
      AESL_token.replace(x_found, 1, "0");
    } else
      no_x = true;
  }
}
struct __cosim_s38__ { char data[56]; };
struct __cosim_s40__ { char data[64]; };
extern "C" void triangle_hw_stub_wrapper(__cosim_s38__*, volatile void *, volatile void *, volatile void *, long long, volatile void *);

extern "C" void  apatb_triangle_hw(__cosim_s38__* ap_return, volatile void * __xlx_apatb_param_mem1, volatile void * __xlx_apatb_param_mem2, volatile void * __xlx_apatb_param_mem3, long long __xlx_apatb_param_len_in_big_words, volatile void * __xlx_apatb_param_outs) {
  refine_signal_handler();
  fstream wrapc_switch_file_token;
  wrapc_switch_file_token.open(".hls_cosim_wrapc_switch.log");
  int AESL_i;
  if (wrapc_switch_file_token.good())
  {

    CodeState = ENTER_WRAPC_PC;
    static unsigned AESL_transaction_pc = 0;
    string AESL_token;
    string AESL_num;{
      static ifstream rtl_tv_out_file;
      if (!rtl_tv_out_file.is_open()) {
        rtl_tv_out_file.open(AUTOTB_TVOUT_PC_return);
        if (rtl_tv_out_file.good()) {
          rtl_tv_out_file >> AESL_token;
          if (AESL_token != "[[[runtime]]]")
            exit(1);
        }
      }
  
      if (rtl_tv_out_file.good()) {
        rtl_tv_out_file >> AESL_token; 
        rtl_tv_out_file >> AESL_num;  // transaction number
        if (AESL_token != "[[transaction]]") {
          cerr << "Unexpected token: " << AESL_token << endl;
          exit(1);
        }
        if (atoi(AESL_num.c_str()) == AESL_transaction_pc) {
          std::vector<sc_bv<448> > return_pc_buffer(1);
          int i = 0;

          rtl_tv_out_file >> AESL_token; //data
          while (AESL_token != "[[/transaction]]"){

            RTLOutputCheckAndReplacement(AESL_token, "return");
  
            // push token into output port buffer
            if (AESL_token != "") {
              return_pc_buffer[i] = AESL_token.c_str();;
              i++;
            }
  
            rtl_tv_out_file >> AESL_token; //data or [[/transaction]]
            if (AESL_token == "[[[/runtime]]]" || rtl_tv_out_file.eof())
              exit(1);
          }
          if (i > 0) {((long long*)ap_return)[0*7+0] = return_pc_buffer[0].range(63,0).to_int64();
((long long*)ap_return)[0*7+1] = return_pc_buffer[0].range(127,64).to_int64();
((long long*)ap_return)[0*7+2] = return_pc_buffer[0].range(191,128).to_int64();
((long long*)ap_return)[0*7+3] = return_pc_buffer[0].range(255,192).to_int64();
((long long*)ap_return)[0*7+4] = return_pc_buffer[0].range(319,256).to_int64();
((long long*)ap_return)[0*7+5] = return_pc_buffer[0].range(383,320).to_int64();
((long long*)ap_return)[0*7+6] = return_pc_buffer[0].range(447,384).to_int64();
}
        } // end transaction
      } // end file is good
    } // end post check logic bolck
  
    AESL_transaction_pc++;
    return ;
  }
static unsigned AESL_transaction;
static AESL_FILE_HANDLER aesl_fh;
static INTER_TCL_FILE tcl_file(INTER_TCL);
std::vector<char> __xlx_sprintf_buffer(1024);
CodeState = ENTER_WRAPC;
//mem1
aesl_fh.touch(AUTOTB_TVIN_mem1);
aesl_fh.touch(AUTOTB_TVOUT_mem1);
//mem2
aesl_fh.touch(AUTOTB_TVIN_mem2);
aesl_fh.touch(AUTOTB_TVOUT_mem2);
//mem3
aesl_fh.touch(AUTOTB_TVIN_mem3);
aesl_fh.touch(AUTOTB_TVOUT_mem3);
//gmem
aesl_fh.touch(AUTOTB_TVIN_gmem);
aesl_fh.touch(AUTOTB_TVOUT_gmem);
//mem1_offset
aesl_fh.touch(AUTOTB_TVIN_mem1_offset);
aesl_fh.touch(AUTOTB_TVOUT_mem1_offset);
//mem2_offset
aesl_fh.touch(AUTOTB_TVIN_mem2_offset);
aesl_fh.touch(AUTOTB_TVOUT_mem2_offset);
//mem3_offset
aesl_fh.touch(AUTOTB_TVIN_mem3_offset);
aesl_fh.touch(AUTOTB_TVOUT_mem3_offset);
//len_in_big_words
aesl_fh.touch(AUTOTB_TVIN_len_in_big_words);
aesl_fh.touch(AUTOTB_TVOUT_len_in_big_words);
//outs
aesl_fh.touch(AUTOTB_TVIN_outs);
aesl_fh.touch(AUTOTB_TVOUT_outs);
CodeState = DUMP_INPUTS;
unsigned __xlx_offset_byte_param_mem1 = 0;
// print mem1 Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_mem1, __xlx_sprintf_buffer.data());
  {  __xlx_offset_byte_param_mem1 = 0*64;
  if (__xlx_apatb_param_mem1) {
    for (int j = 0  - 0, e = 5 - 0; j != e; ++j) {
sc_bv<512> __xlx_tmp_lv;
__xlx_tmp_lv.range(63,0) = ((long long*)__xlx_apatb_param_mem1)[j*8+0];
__xlx_tmp_lv.range(127,64) = ((long long*)__xlx_apatb_param_mem1)[j*8+1];
__xlx_tmp_lv.range(191,128) = ((long long*)__xlx_apatb_param_mem1)[j*8+2];
__xlx_tmp_lv.range(255,192) = ((long long*)__xlx_apatb_param_mem1)[j*8+3];
__xlx_tmp_lv.range(319,256) = ((long long*)__xlx_apatb_param_mem1)[j*8+4];
__xlx_tmp_lv.range(383,320) = ((long long*)__xlx_apatb_param_mem1)[j*8+5];
__xlx_tmp_lv.range(447,384) = ((long long*)__xlx_apatb_param_mem1)[j*8+6];
__xlx_tmp_lv.range(511,448) = ((long long*)__xlx_apatb_param_mem1)[j*8+7];

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_mem1, __xlx_sprintf_buffer.data()); 
      }
  }
}
  tcl_file.set_num(5, &tcl_file.mem1_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_mem1, __xlx_sprintf_buffer.data());
}
unsigned __xlx_offset_byte_param_mem2 = 0;
// print mem2 Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_mem2, __xlx_sprintf_buffer.data());
  {  __xlx_offset_byte_param_mem2 = 0*64;
  if (__xlx_apatb_param_mem2) {
    for (int j = 0  - 0, e = 5 - 0; j != e; ++j) {
sc_bv<512> __xlx_tmp_lv;
__xlx_tmp_lv.range(63,0) = ((long long*)__xlx_apatb_param_mem2)[j*8+0];
__xlx_tmp_lv.range(127,64) = ((long long*)__xlx_apatb_param_mem2)[j*8+1];
__xlx_tmp_lv.range(191,128) = ((long long*)__xlx_apatb_param_mem2)[j*8+2];
__xlx_tmp_lv.range(255,192) = ((long long*)__xlx_apatb_param_mem2)[j*8+3];
__xlx_tmp_lv.range(319,256) = ((long long*)__xlx_apatb_param_mem2)[j*8+4];
__xlx_tmp_lv.range(383,320) = ((long long*)__xlx_apatb_param_mem2)[j*8+5];
__xlx_tmp_lv.range(447,384) = ((long long*)__xlx_apatb_param_mem2)[j*8+6];
__xlx_tmp_lv.range(511,448) = ((long long*)__xlx_apatb_param_mem2)[j*8+7];

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_mem2, __xlx_sprintf_buffer.data()); 
      }
  }
}
  tcl_file.set_num(5, &tcl_file.mem2_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_mem2, __xlx_sprintf_buffer.data());
}
unsigned __xlx_offset_byte_param_mem3 = 0;
// print mem3 Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_mem3, __xlx_sprintf_buffer.data());
  {  __xlx_offset_byte_param_mem3 = 0*64;
  if (__xlx_apatb_param_mem3) {
    for (int j = 0  - 0, e = 5 - 0; j != e; ++j) {
sc_bv<512> __xlx_tmp_lv;
__xlx_tmp_lv.range(63,0) = ((long long*)__xlx_apatb_param_mem3)[j*8+0];
__xlx_tmp_lv.range(127,64) = ((long long*)__xlx_apatb_param_mem3)[j*8+1];
__xlx_tmp_lv.range(191,128) = ((long long*)__xlx_apatb_param_mem3)[j*8+2];
__xlx_tmp_lv.range(255,192) = ((long long*)__xlx_apatb_param_mem3)[j*8+3];
__xlx_tmp_lv.range(319,256) = ((long long*)__xlx_apatb_param_mem3)[j*8+4];
__xlx_tmp_lv.range(383,320) = ((long long*)__xlx_apatb_param_mem3)[j*8+5];
__xlx_tmp_lv.range(447,384) = ((long long*)__xlx_apatb_param_mem3)[j*8+6];
__xlx_tmp_lv.range(511,448) = ((long long*)__xlx_apatb_param_mem3)[j*8+7];

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_mem3, __xlx_sprintf_buffer.data()); 
      }
  }
}
  tcl_file.set_num(5, &tcl_file.mem3_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_mem3, __xlx_sprintf_buffer.data());
}
unsigned __xlx_offset_byte_param_outs = 0;
// print gmem Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_gmem, __xlx_sprintf_buffer.data());
  {  __xlx_offset_byte_param_outs = 0*64;
  if (__xlx_apatb_param_outs) {
    for (int j = 0  - 0, e = 5 - 0; j != e; ++j) {
sc_bv<512> __xlx_tmp_lv;
__xlx_tmp_lv.range(63,0) = ((long long*)__xlx_apatb_param_outs)[j*8+0];
__xlx_tmp_lv.range(127,64) = ((long long*)__xlx_apatb_param_outs)[j*8+1];
__xlx_tmp_lv.range(191,128) = ((long long*)__xlx_apatb_param_outs)[j*8+2];
__xlx_tmp_lv.range(255,192) = ((long long*)__xlx_apatb_param_outs)[j*8+3];
__xlx_tmp_lv.range(319,256) = ((long long*)__xlx_apatb_param_outs)[j*8+4];
__xlx_tmp_lv.range(383,320) = ((long long*)__xlx_apatb_param_outs)[j*8+5];
__xlx_tmp_lv.range(447,384) = ((long long*)__xlx_apatb_param_outs)[j*8+6];
__xlx_tmp_lv.range(511,448) = ((long long*)__xlx_apatb_param_outs)[j*8+7];

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_gmem, __xlx_sprintf_buffer.data()); 
      }
  }
}
  tcl_file.set_num(5, &tcl_file.gmem_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_gmem, __xlx_sprintf_buffer.data());
}
// print mem1_offset Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_mem1_offset, __xlx_sprintf_buffer.data());
  {
    sc_bv<64> __xlx_tmp_lv = __xlx_offset_byte_param_mem1;

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_mem1_offset, __xlx_sprintf_buffer.data()); 
  }
  tcl_file.set_num(1, &tcl_file.mem1_offset_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_mem1_offset, __xlx_sprintf_buffer.data());
}
// print mem2_offset Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_mem2_offset, __xlx_sprintf_buffer.data());
  {
    sc_bv<64> __xlx_tmp_lv = __xlx_offset_byte_param_mem2;

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_mem2_offset, __xlx_sprintf_buffer.data()); 
  }
  tcl_file.set_num(1, &tcl_file.mem2_offset_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_mem2_offset, __xlx_sprintf_buffer.data());
}
// print mem3_offset Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_mem3_offset, __xlx_sprintf_buffer.data());
  {
    sc_bv<64> __xlx_tmp_lv = __xlx_offset_byte_param_mem3;

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_mem3_offset, __xlx_sprintf_buffer.data()); 
  }
  tcl_file.set_num(1, &tcl_file.mem3_offset_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_mem3_offset, __xlx_sprintf_buffer.data());
}
// print len_in_big_words Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_len_in_big_words, __xlx_sprintf_buffer.data());
  {
    sc_bv<64> __xlx_tmp_lv = *((long long*)&__xlx_apatb_param_len_in_big_words);

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_len_in_big_words, __xlx_sprintf_buffer.data()); 
  }
  tcl_file.set_num(1, &tcl_file.len_in_big_words_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_len_in_big_words, __xlx_sprintf_buffer.data());
}
// print outs Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVIN_outs, __xlx_sprintf_buffer.data());
  {
    sc_bv<64> __xlx_tmp_lv = __xlx_offset_byte_param_outs;

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVIN_outs, __xlx_sprintf_buffer.data()); 
  }
  tcl_file.set_num(1, &tcl_file.outs_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVIN_outs, __xlx_sprintf_buffer.data());
}
CodeState = CALL_C_DUT;
triangle_hw_stub_wrapper(ap_return, __xlx_apatb_param_mem1, __xlx_apatb_param_mem2, __xlx_apatb_param_mem3, __xlx_apatb_param_len_in_big_words, __xlx_apatb_param_outs);
CodeState = DUMP_OUTPUTS;
// print return Transactions
{
  sprintf(__xlx_sprintf_buffer.data(), "[[transaction]] %d\n", AESL_transaction);
  aesl_fh.write(AUTOTB_TVOUT_return, __xlx_sprintf_buffer.data());
  sc_bv<448> __xlx_tmp_lv;
__xlx_tmp_lv.range(63,0) = ((long long*)ap_return)[0*7+0];
__xlx_tmp_lv.range(127,64) = ((long long*)ap_return)[0*7+1];
__xlx_tmp_lv.range(191,128) = ((long long*)ap_return)[0*7+2];
__xlx_tmp_lv.range(255,192) = ((long long*)ap_return)[0*7+3];
__xlx_tmp_lv.range(319,256) = ((long long*)ap_return)[0*7+4];
__xlx_tmp_lv.range(383,320) = ((long long*)ap_return)[0*7+5];
__xlx_tmp_lv.range(447,384) = ((long long*)ap_return)[0*7+6];

    sprintf(__xlx_sprintf_buffer.data(), "%s\n", __xlx_tmp_lv.to_string(SC_HEX).c_str());
    aesl_fh.write(AUTOTB_TVOUT_return, __xlx_sprintf_buffer.data()); 
  
  tcl_file.set_num(1, &tcl_file.return_depth);
  sprintf(__xlx_sprintf_buffer.data(), "[[/transaction]] \n");
  aesl_fh.write(AUTOTB_TVOUT_return, __xlx_sprintf_buffer.data());
}
CodeState = DELETE_CHAR_BUFFERS;
AESL_transaction++;
tcl_file.set_num(AESL_transaction , &tcl_file.trans_num);
}
