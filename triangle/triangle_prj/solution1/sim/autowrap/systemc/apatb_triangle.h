// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================

extern "C" __cosim_s38__ AESL_WRAP_triangle (
volatile void* mem1,
volatile void* mem2,
volatile void* mem3,
long long len_in_big_words,
volatile void* outs);
