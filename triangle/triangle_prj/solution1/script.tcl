############################################################
## This file is generated automatically by Vitis HLS.
## Please DO NOT edit it.
## Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
############################################################
open_project triangle_prj
set_top triangle
add_files src/triangle.h
add_files src/triangle.cpp
add_files -tb src/test_tri.cpp -cflags "-Wno-unknown-pragmas" -csimflags "-Wno-unknown-pragmas"
open_solution "solution1" -flow_target vitis
set_part {xcvu9p-flga2104-1-e}
create_clock -period 4 -name default
config_rtl -register_reset_num 3
config_interface -m_axi_alignment_byte_size 64 -m_axi_latency 64 -m_axi_max_read_burst_length 64 -m_axi_max_widen_bitwidth 512 -m_axi_max_write_burst_length 2 -m_axi_num_read_outstanding 64 -m_axi_num_write_outstanding 2 -m_axi_offset slave -s_axilite_data64
source "./triangle_prj/solution1/directives.tcl"
csim_design -O
csynth_design
cosim_design
export_design -format ip_catalog
