-- ==============================================================
-- Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- ==============================================================
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity triangle_control_s_axi is
generic (
    C_S_AXI_ADDR_WIDTH    : INTEGER := 8;
    C_S_AXI_DATA_WIDTH    : INTEGER := 64);
port (
    ACLK                  :in   STD_LOGIC;
    ARESET                :in   STD_LOGIC;
    ACLK_EN               :in   STD_LOGIC;
    AWADDR                :in   STD_LOGIC_VECTOR(C_S_AXI_ADDR_WIDTH-1 downto 0);
    AWVALID               :in   STD_LOGIC;
    AWREADY               :out  STD_LOGIC;
    WDATA                 :in   STD_LOGIC_VECTOR(C_S_AXI_DATA_WIDTH-1 downto 0);
    WSTRB                 :in   STD_LOGIC_VECTOR(C_S_AXI_DATA_WIDTH/8-1 downto 0);
    WVALID                :in   STD_LOGIC;
    WREADY                :out  STD_LOGIC;
    BRESP                 :out  STD_LOGIC_VECTOR(1 downto 0);
    BVALID                :out  STD_LOGIC;
    BREADY                :in   STD_LOGIC;
    ARADDR                :in   STD_LOGIC_VECTOR(C_S_AXI_ADDR_WIDTH-1 downto 0);
    ARVALID               :in   STD_LOGIC;
    ARREADY               :out  STD_LOGIC;
    RDATA                 :out  STD_LOGIC_VECTOR(C_S_AXI_DATA_WIDTH-1 downto 0);
    RRESP                 :out  STD_LOGIC_VECTOR(1 downto 0);
    RVALID                :out  STD_LOGIC;
    RREADY                :in   STD_LOGIC;
    interrupt             :out  STD_LOGIC;
    ap_start              :out  STD_LOGIC;
    ap_done               :in   STD_LOGIC;
    ap_ready              :in   STD_LOGIC;
    ap_continue           :out  STD_LOGIC;
    ap_idle               :in   STD_LOGIC;
    ap_return             :in   STD_LOGIC_VECTOR(447 downto 0);
    mem1_offset           :out  STD_LOGIC_VECTOR(63 downto 0);
    mem2_offset           :out  STD_LOGIC_VECTOR(63 downto 0);
    mem3_offset           :out  STD_LOGIC_VECTOR(63 downto 0);
    len_in_big_words      :out  STD_LOGIC_VECTOR(63 downto 0);
    outs                  :out  STD_LOGIC_VECTOR(63 downto 0)
);
end entity triangle_control_s_axi;

-- ------------------------Address Info-------------------
-- 0x00 : Control signals
--        bit 0  - ap_start (Read/Write/COH)
--        bit 1  - ap_done (Read)
--        bit 2  - ap_idle (Read)
--        bit 3  - ap_ready (Read)
--        bit 4  - ap_continue (Read/Write/SC)
--        bit 7  - auto_restart (Read/Write)
--        others - reserved
-- 0x08 : Global Interrupt Enable Register
--        bit 0  - Global Interrupt Enable (Read/Write)
--        others - reserved
-- 0x10 : IP Interrupt Enable Register (Read/Write)
--        bit 0  - enable ap_done interrupt (Read/Write)
--        bit 1  - enable ap_ready interrupt (Read/Write)
--        others - reserved
-- 0x18 : IP Interrupt Status Register (Read/TOW)
--        bit 0  - ap_done (COR/TOW)
--        bit 1  - ap_ready (COR/TOW)
--        others - reserved
-- 0x20 : Data signal of ap_return
--        bit 63~0 - ap_return[63:0] (Read)
-- 0x28 : Data signal of ap_return
--        bit 63~0 - ap_return[127:64] (Read)
-- 0x30 : Data signal of ap_return
--        bit 63~0 - ap_return[191:128] (Read)
-- 0x38 : Data signal of ap_return
--        bit 63~0 - ap_return[255:192] (Read)
-- 0x40 : Data signal of ap_return
--        bit 63~0 - ap_return[319:256] (Read)
-- 0x48 : Data signal of ap_return
--        bit 63~0 - ap_return[383:320] (Read)
-- 0x50 : Data signal of ap_return
--        bit 63~0 - ap_return[447:384] (Read)
-- 0x60 : Data signal of mem1_offset
--        bit 63~0 - mem1_offset[63:0] (Read/Write)
-- 0x68 : reserved
-- 0x70 : Data signal of mem2_offset
--        bit 63~0 - mem2_offset[63:0] (Read/Write)
-- 0x78 : reserved
-- 0x80 : Data signal of mem3_offset
--        bit 63~0 - mem3_offset[63:0] (Read/Write)
-- 0x88 : reserved
-- 0x90 : Data signal of len_in_big_words
--        bit 63~0 - len_in_big_words[63:0] (Read/Write)
-- 0x98 : reserved
-- 0xa0 : Data signal of outs
--        bit 63~0 - outs[63:0] (Read/Write)
-- 0xa8 : reserved
-- (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

architecture behave of triangle_control_s_axi is
    type states is (wridle, wrdata, wrresp, wrreset, rdidle, rddata, rdreset);  -- read and write fsm states
    signal wstate  : states := wrreset;
    signal rstate  : states := rdreset;
    signal wnext, rnext: states;
    constant ADDR_AP_CTRL                 : INTEGER := 16#00#;
    constant ADDR_GIE                     : INTEGER := 16#08#;
    constant ADDR_IER                     : INTEGER := 16#10#;
    constant ADDR_ISR                     : INTEGER := 16#18#;
    constant ADDR_AP_RETURN_0             : INTEGER := 16#20#;
    constant ADDR_AP_RETURN_1             : INTEGER := 16#28#;
    constant ADDR_AP_RETURN_2             : INTEGER := 16#30#;
    constant ADDR_AP_RETURN_3             : INTEGER := 16#38#;
    constant ADDR_AP_RETURN_4             : INTEGER := 16#40#;
    constant ADDR_AP_RETURN_5             : INTEGER := 16#48#;
    constant ADDR_AP_RETURN_6             : INTEGER := 16#50#;
    constant ADDR_MEM1_OFFSET_DATA_0      : INTEGER := 16#60#;
    constant ADDR_MEM1_OFFSET_CTRL        : INTEGER := 16#68#;
    constant ADDR_MEM2_OFFSET_DATA_0      : INTEGER := 16#70#;
    constant ADDR_MEM2_OFFSET_CTRL        : INTEGER := 16#78#;
    constant ADDR_MEM3_OFFSET_DATA_0      : INTEGER := 16#80#;
    constant ADDR_MEM3_OFFSET_CTRL        : INTEGER := 16#88#;
    constant ADDR_LEN_IN_BIG_WORDS_DATA_0 : INTEGER := 16#90#;
    constant ADDR_LEN_IN_BIG_WORDS_CTRL   : INTEGER := 16#98#;
    constant ADDR_OUTS_DATA_0             : INTEGER := 16#a0#;
    constant ADDR_OUTS_CTRL               : INTEGER := 16#a8#;
    constant ADDR_BITS         : INTEGER := 8;

    signal waddr               : UNSIGNED(ADDR_BITS-1 downto 0);
    signal wmask               : UNSIGNED(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal aw_hs               : STD_LOGIC;
    signal w_hs                : STD_LOGIC;
    signal rdata_data          : UNSIGNED(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal ar_hs               : STD_LOGIC;
    signal raddr               : UNSIGNED(ADDR_BITS-1 downto 0);
    signal AWREADY_t           : STD_LOGIC;
    signal WREADY_t            : STD_LOGIC;
    signal ARREADY_t           : STD_LOGIC;
    signal RVALID_t            : STD_LOGIC;
    -- internal registers
    signal int_ap_idle         : STD_LOGIC;
    signal int_ap_continue     : STD_LOGIC;
    signal int_ap_ready        : STD_LOGIC;
    signal int_ap_done         : STD_LOGIC;
    signal int_ap_start        : STD_LOGIC := '0';
    signal int_auto_restart    : STD_LOGIC := '0';
    signal int_gie             : STD_LOGIC := '0';
    signal int_ier             : UNSIGNED(1 downto 0) := (others => '0');
    signal int_isr             : UNSIGNED(1 downto 0) := (others => '0');
    signal int_ap_return       : UNSIGNED(447 downto 0);
    signal int_mem1_offset     : UNSIGNED(63 downto 0) := (others => '0');
    signal int_mem2_offset     : UNSIGNED(63 downto 0) := (others => '0');
    signal int_mem3_offset     : UNSIGNED(63 downto 0) := (others => '0');
    signal int_len_in_big_words : UNSIGNED(63 downto 0) := (others => '0');
    signal int_outs            : UNSIGNED(63 downto 0) := (others => '0');


begin
-- ----------------------- Instantiation------------------


-- ----------------------- AXI WRITE ---------------------
    AWREADY_t <=  '1' when wstate = wridle else '0';
    AWREADY   <=  AWREADY_t;
    WREADY_t  <=  '1' when wstate = wrdata else '0';
    WREADY    <=  WREADY_t;
    BRESP     <=  "00";  -- OKAY
    BVALID    <=  '1' when wstate = wrresp else '0';
    wmask     <=  (63 downto 56 => WSTRB(7), 55 downto 48 => WSTRB(6), 47 downto 40 => WSTRB(5), 39 downto 32 => WSTRB(4), 31 downto 24 => WSTRB(3), 23 downto 16 => WSTRB(2), 15 downto 8 => WSTRB(1), 7 downto 0 => WSTRB(0));
    aw_hs     <=  AWVALID and AWREADY_t;
    w_hs      <=  WVALID and WREADY_t;

    -- write FSM
    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ARESET = '1') then
                wstate <= wrreset;
            elsif (ACLK_EN = '1') then
                wstate <= wnext;
            end if;
        end if;
    end process;

    process (wstate, AWVALID, WVALID, BREADY)
    begin
        case (wstate) is
        when wridle =>
            if (AWVALID = '1') then
                wnext <= wrdata;
            else
                wnext <= wridle;
            end if;
        when wrdata =>
            if (WVALID = '1') then
                wnext <= wrresp;
            else
                wnext <= wrdata;
            end if;
        when wrresp =>
            if (BREADY = '1') then
                wnext <= wridle;
            else
                wnext <= wrresp;
            end if;
        when others =>
            wnext <= wridle;
        end case;
    end process;

    waddr_proc : process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ACLK_EN = '1') then
                if (aw_hs = '1') then
                    waddr <= UNSIGNED(AWADDR(ADDR_BITS-1 downto 0));
                end if;
            end if;
        end if;
    end process;

-- ----------------------- AXI READ ----------------------
    ARREADY_t <= '1' when (rstate = rdidle) else '0';
    ARREADY <= ARREADY_t;
    RDATA   <= STD_LOGIC_VECTOR(rdata_data);
    RRESP   <= "00";  -- OKAY
    RVALID_t  <= '1' when (rstate = rddata) else '0';
    RVALID    <= RVALID_t;
    ar_hs   <= ARVALID and ARREADY_t;
    raddr   <= UNSIGNED(ARADDR(ADDR_BITS-1 downto 0));

    -- read FSM
    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ARESET = '1') then
                rstate <= rdreset;
            elsif (ACLK_EN = '1') then
                rstate <= rnext;
            end if;
        end if;
    end process;

    process (rstate, ARVALID, RREADY, RVALID_t)
    begin
        case (rstate) is
        when rdidle =>
            if (ARVALID = '1') then
                rnext <= rddata;
            else
                rnext <= rdidle;
            end if;
        when rddata =>
            if (RREADY = '1' and RVALID_t = '1') then
                rnext <= rdidle;
            else
                rnext <= rddata;
            end if;
        when others =>
            rnext <= rdidle;
        end case;
    end process;

    rdata_proc : process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ACLK_EN = '1') then
                if (ar_hs = '1') then
                    rdata_data <= (others => '0');
                    case (TO_INTEGER(raddr)) is
                    when ADDR_AP_CTRL =>
                        rdata_data(7) <= int_auto_restart;
                        rdata_data(4) <= int_ap_continue;
                        rdata_data(3) <= int_ap_ready;
                        rdata_data(2) <= int_ap_idle;
                        rdata_data(1) <= int_ap_done;
                        rdata_data(0) <= int_ap_start;
                    when ADDR_GIE =>
                        rdata_data(0) <= int_gie;
                    when ADDR_IER =>
                        rdata_data(1 downto 0) <= int_ier;
                    when ADDR_ISR =>
                        rdata_data(1 downto 0) <= int_isr;
                    when ADDR_AP_RETURN_0 =>
                        rdata_data <= RESIZE(int_ap_return(63 downto 0), 64);
                    when ADDR_AP_RETURN_1 =>
                        rdata_data <= RESIZE(int_ap_return(127 downto 64), 64);
                    when ADDR_AP_RETURN_2 =>
                        rdata_data <= RESIZE(int_ap_return(191 downto 128), 64);
                    when ADDR_AP_RETURN_3 =>
                        rdata_data <= RESIZE(int_ap_return(255 downto 192), 64);
                    when ADDR_AP_RETURN_4 =>
                        rdata_data <= RESIZE(int_ap_return(319 downto 256), 64);
                    when ADDR_AP_RETURN_5 =>
                        rdata_data <= RESIZE(int_ap_return(383 downto 320), 64);
                    when ADDR_AP_RETURN_6 =>
                        rdata_data <= RESIZE(int_ap_return(447 downto 384), 64);
                    when ADDR_MEM1_OFFSET_DATA_0 =>
                        rdata_data <= RESIZE(int_mem1_offset(63 downto 0), 64);
                    when ADDR_MEM2_OFFSET_DATA_0 =>
                        rdata_data <= RESIZE(int_mem2_offset(63 downto 0), 64);
                    when ADDR_MEM3_OFFSET_DATA_0 =>
                        rdata_data <= RESIZE(int_mem3_offset(63 downto 0), 64);
                    when ADDR_LEN_IN_BIG_WORDS_DATA_0 =>
                        rdata_data <= RESIZE(int_len_in_big_words(63 downto 0), 64);
                    when ADDR_OUTS_DATA_0 =>
                        rdata_data <= RESIZE(int_outs(63 downto 0), 64);
                    when others =>
                        NULL;
                    end case;
                end if;
            end if;
        end if;
    end process;

-- ----------------------- Register logic ----------------
    interrupt            <= int_gie and (int_isr(0) or int_isr(1));
    ap_start             <= int_ap_start;
    int_ap_done          <= ap_done;
    ap_continue          <= int_ap_continue;
    mem1_offset          <= STD_LOGIC_VECTOR(int_mem1_offset);
    mem2_offset          <= STD_LOGIC_VECTOR(int_mem2_offset);
    mem3_offset          <= STD_LOGIC_VECTOR(int_mem3_offset);
    len_in_big_words     <= STD_LOGIC_VECTOR(int_len_in_big_words);
    outs                 <= STD_LOGIC_VECTOR(int_outs);

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ARESET = '1') then
                int_ap_start <= '0';
            elsif (ACLK_EN = '1') then
                if (w_hs = '1' and waddr = ADDR_AP_CTRL and WSTRB(0) = '1' and WDATA(0) = '1') then
                    int_ap_start <= '1';
                elsif (ap_ready = '1') then
                    int_ap_start <= int_auto_restart; -- clear on handshake/auto restart
                end if;
            end if;
        end if;
    end process;

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ARESET = '1') then
                int_ap_idle <= '0';
            elsif (ACLK_EN = '1') then
                if (true) then
                    int_ap_idle <= ap_idle;
                end if;
            end if;
        end if;
    end process;

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ARESET = '1') then
                int_ap_ready <= '0';
            elsif (ACLK_EN = '1') then
                if (true) then
                    int_ap_ready <= ap_ready;
                end if;
            end if;
        end if;
    end process;

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ARESET = '1') then
                int_ap_continue <= '0';
            elsif (ACLK_EN = '1') then
                if (w_hs = '1' and waddr = ADDR_AP_CTRL and WSTRB(0) = '1' and WDATA(4) = '1') then
                    int_ap_continue <= '1';
                elsif (ap_done = '1' and int_ap_continue = '0' and int_auto_restart = '1') then
                    int_ap_continue <= '1'; -- auto restart
                else
                    int_ap_continue <= '0'; -- self clear
                end if;
            end if;
        end if;
    end process;

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ARESET = '1') then
                int_auto_restart <= '0';
            elsif (ACLK_EN = '1') then
                if (w_hs = '1' and waddr = ADDR_AP_CTRL and WSTRB(0) = '1') then
                    int_auto_restart <= WDATA(7);
                end if;
            end if;
        end if;
    end process;

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ARESET = '1') then
                int_gie <= '0';
            elsif (ACLK_EN = '1') then
                if (w_hs = '1' and waddr = ADDR_GIE and WSTRB(0) = '1') then
                    int_gie <= WDATA(0);
                end if;
            end if;
        end if;
    end process;

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ARESET = '1') then
                int_ier <= "00";
            elsif (ACLK_EN = '1') then
                if (w_hs = '1' and waddr = ADDR_IER and WSTRB(0) = '1') then
                    int_ier <= UNSIGNED(WDATA(1 downto 0));
                end if;
            end if;
        end if;
    end process;

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ARESET = '1') then
                int_isr(0) <= '0';
            elsif (ACLK_EN = '1') then
                if (int_ier(0) = '1' and ap_done = '1') then
                    int_isr(0) <= '1';
                elsif (w_hs = '1' and waddr = ADDR_ISR and WSTRB(0) = '1') then
                    int_isr(0) <= int_isr(0) xor WDATA(0); -- toggle on write
                end if;
            end if;
        end if;
    end process;

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ARESET = '1') then
                int_isr(1) <= '0';
            elsif (ACLK_EN = '1') then
                if (int_ier(1) = '1' and ap_ready = '1') then
                    int_isr(1) <= '1';
                elsif (w_hs = '1' and waddr = ADDR_ISR and WSTRB(0) = '1') then
                    int_isr(1) <= int_isr(1) xor WDATA(1); -- toggle on write
                end if;
            end if;
        end if;
    end process;

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ARESET = '1') then
                int_ap_return <= (others => '0');
            elsif (ACLK_EN = '1') then
                if (ap_done = '1') then
                    int_ap_return <= UNSIGNED(ap_return);
                end if;
            end if;
        end if;
    end process;

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ACLK_EN = '1') then
                if (w_hs = '1' and waddr = ADDR_MEM1_OFFSET_DATA_0) then
                    int_mem1_offset(63 downto 0) <= (UNSIGNED(WDATA(63 downto 0)) and wmask(63 downto 0)) or ((not wmask(63 downto 0)) and int_mem1_offset(63 downto 0));
                end if;
            end if;
        end if;
    end process;

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ACLK_EN = '1') then
                if (w_hs = '1' and waddr = ADDR_MEM2_OFFSET_DATA_0) then
                    int_mem2_offset(63 downto 0) <= (UNSIGNED(WDATA(63 downto 0)) and wmask(63 downto 0)) or ((not wmask(63 downto 0)) and int_mem2_offset(63 downto 0));
                end if;
            end if;
        end if;
    end process;

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ACLK_EN = '1') then
                if (w_hs = '1' and waddr = ADDR_MEM3_OFFSET_DATA_0) then
                    int_mem3_offset(63 downto 0) <= (UNSIGNED(WDATA(63 downto 0)) and wmask(63 downto 0)) or ((not wmask(63 downto 0)) and int_mem3_offset(63 downto 0));
                end if;
            end if;
        end if;
    end process;

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ACLK_EN = '1') then
                if (w_hs = '1' and waddr = ADDR_LEN_IN_BIG_WORDS_DATA_0) then
                    int_len_in_big_words(63 downto 0) <= (UNSIGNED(WDATA(63 downto 0)) and wmask(63 downto 0)) or ((not wmask(63 downto 0)) and int_len_in_big_words(63 downto 0));
                end if;
            end if;
        end if;
    end process;

    process (ACLK)
    begin
        if (ACLK'event and ACLK = '1') then
            if (ACLK_EN = '1') then
                if (w_hs = '1' and waddr = ADDR_OUTS_DATA_0) then
                    int_outs(63 downto 0) <= (UNSIGNED(WDATA(63 downto 0)) and wmask(63 downto 0)) or ((not wmask(63 downto 0)) and int_outs(63 downto 0));
                end if;
            end if;
        end if;
    end process;


-- ----------------------- Memory logic ------------------

end architecture behave;
