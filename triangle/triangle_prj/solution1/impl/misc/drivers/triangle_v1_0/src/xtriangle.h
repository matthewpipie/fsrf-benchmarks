// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XTRIANGLE_H
#define XTRIANGLE_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xtriangle_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
#else
typedef struct {
    u16 DeviceId;
    u32 Control_BaseAddress;
} XTriangle_Config;
#endif

typedef struct {
    u64 Control_BaseAddress;
    u64 IsReady;
} XTriangle;

typedef u64 word_type;

typedef struct {
    u64 word_0;
    u64 word_1;
    u64 word_2;
    u64 word_3;
    u64 word_4;
    u64 word_5;
    u64 word_6;
} XTriangle_Return;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XTriangle_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out64((BaseAddress) + (RegOffset), (u64)(Data))
#define XTriangle_ReadReg(BaseAddress, RegOffset) \
    Xil_In64((BaseAddress) + (RegOffset))
#else
#define XTriangle_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u64*)((BaseAddress) + (RegOffset)) = (u64)(Data)
#define XTriangle_ReadReg(BaseAddress, RegOffset) \
    *(volatile u64*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XTriangle_Initialize(XTriangle *InstancePtr, u16 DeviceId);
XTriangle_Config* XTriangle_LookupConfig(u16 DeviceId);
int XTriangle_CfgInitialize(XTriangle *InstancePtr, XTriangle_Config *ConfigPtr);
#else
int XTriangle_Initialize(XTriangle *InstancePtr, const char* InstanceName);
int XTriangle_Release(XTriangle *InstancePtr);
#endif

void XTriangle_Start(XTriangle *InstancePtr);
u64 XTriangle_IsDone(XTriangle *InstancePtr);
u64 XTriangle_IsIdle(XTriangle *InstancePtr);
u64 XTriangle_IsReady(XTriangle *InstancePtr);
void XTriangle_Continue(XTriangle *InstancePtr);
void XTriangle_EnableAutoRestart(XTriangle *InstancePtr);
void XTriangle_DisableAutoRestart(XTriangle *InstancePtr);
XTriangle_Return XTriangle_Get_return(XTriangle *InstancePtr);

void XTriangle_Set_mem1_offset(XTriangle *InstancePtr, u64 Data);
u64 XTriangle_Get_mem1_offset(XTriangle *InstancePtr);
void XTriangle_Set_mem2_offset(XTriangle *InstancePtr, u64 Data);
u64 XTriangle_Get_mem2_offset(XTriangle *InstancePtr);
void XTriangle_Set_mem3_offset(XTriangle *InstancePtr, u64 Data);
u64 XTriangle_Get_mem3_offset(XTriangle *InstancePtr);
void XTriangle_Set_len_in_big_words(XTriangle *InstancePtr, u64 Data);
u64 XTriangle_Get_len_in_big_words(XTriangle *InstancePtr);
void XTriangle_Set_outs(XTriangle *InstancePtr, u64 Data);
u64 XTriangle_Get_outs(XTriangle *InstancePtr);

void XTriangle_InterruptGlobalEnable(XTriangle *InstancePtr);
void XTriangle_InterruptGlobalDisable(XTriangle *InstancePtr);
void XTriangle_InterruptEnable(XTriangle *InstancePtr, u64 Mask);
void XTriangle_InterruptDisable(XTriangle *InstancePtr, u64 Mask);
void XTriangle_InterruptClear(XTriangle *InstancePtr, u64 Mask);
u64 XTriangle_InterruptGetEnabled(XTriangle *InstancePtr);
u64 XTriangle_InterruptGetStatus(XTriangle *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
