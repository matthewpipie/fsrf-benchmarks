# This script segment is generated automatically by AutoPilot

set axilite_register_dict [dict create]
set port_control {
ap_start { }
ap_done { }
ap_ready { }
ap_continue { }
ap_idle { }
ap_return { 
	dir o
	width 448
	depth 1
	mode ap_ctrl_chain
	offset 32
	offset_end 0
}
mem1_offset { 
	dir I
	width 64
	depth 1
	mode ap_none
	offset 96
	offset_end 111
}
mem2_offset { 
	dir I
	width 64
	depth 1
	mode ap_none
	offset 112
	offset_end 127
}
mem3_offset { 
	dir I
	width 64
	depth 1
	mode ap_none
	offset 128
	offset_end 143
}
len_in_big_words { 
	dir I
	width 64
	depth 1
	mode ap_none
	offset 144
	offset_end 159
}
outs { 
	dir I
	width 64
	depth 1
	mode ap_none
	offset 160
	offset_end 175
}
}
dict set axilite_register_dict control $port_control


