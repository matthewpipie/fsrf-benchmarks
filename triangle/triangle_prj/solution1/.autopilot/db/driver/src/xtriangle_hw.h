// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
// control
// 0x00 : Control signals
//        bit 0  - ap_start (Read/Write/COH)
//        bit 1  - ap_done (Read)
//        bit 2  - ap_idle (Read)
//        bit 3  - ap_ready (Read)
//        bit 4  - ap_continue (Read/Write/SC)
//        bit 7  - auto_restart (Read/Write)
//        others - reserved
// 0x08 : Global Interrupt Enable Register
//        bit 0  - Global Interrupt Enable (Read/Write)
//        others - reserved
// 0x10 : IP Interrupt Enable Register (Read/Write)
//        bit 0  - enable ap_done interrupt (Read/Write)
//        bit 1  - enable ap_ready interrupt (Read/Write)
//        others - reserved
// 0x18 : IP Interrupt Status Register (Read/TOW)
//        bit 0  - ap_done (COR/TOW)
//        bit 1  - ap_ready (COR/TOW)
//        others - reserved
// 0x20 : Data signal of ap_return
//        bit 63~0 - ap_return[63:0] (Read)
// 0x28 : Data signal of ap_return
//        bit 63~0 - ap_return[127:64] (Read)
// 0x30 : Data signal of ap_return
//        bit 63~0 - ap_return[191:128] (Read)
// 0x38 : Data signal of ap_return
//        bit 63~0 - ap_return[255:192] (Read)
// 0x40 : Data signal of ap_return
//        bit 63~0 - ap_return[319:256] (Read)
// 0x48 : Data signal of ap_return
//        bit 63~0 - ap_return[383:320] (Read)
// 0x50 : Data signal of ap_return
//        bit 63~0 - ap_return[447:384] (Read)
// 0x60 : Data signal of mem1_offset
//        bit 63~0 - mem1_offset[63:0] (Read/Write)
// 0x68 : reserved
// 0x70 : Data signal of mem2_offset
//        bit 63~0 - mem2_offset[63:0] (Read/Write)
// 0x78 : reserved
// 0x80 : Data signal of mem3_offset
//        bit 63~0 - mem3_offset[63:0] (Read/Write)
// 0x88 : reserved
// 0x90 : Data signal of len_in_big_words
//        bit 63~0 - len_in_big_words[63:0] (Read/Write)
// 0x98 : reserved
// 0xa0 : Data signal of outs
//        bit 63~0 - outs[63:0] (Read/Write)
// 0xa8 : reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define XTRIANGLE_CONTROL_ADDR_AP_CTRL               0x00
#define XTRIANGLE_CONTROL_ADDR_GIE                   0x08
#define XTRIANGLE_CONTROL_ADDR_IER                   0x10
#define XTRIANGLE_CONTROL_ADDR_ISR                   0x18
#define XTRIANGLE_CONTROL_ADDR_AP_RETURN             0x20
#define XTRIANGLE_CONTROL_BITS_AP_RETURN             448
#define XTRIANGLE_CONTROL_ADDR_MEM1_OFFSET_DATA      0x60
#define XTRIANGLE_CONTROL_BITS_MEM1_OFFSET_DATA      64
#define XTRIANGLE_CONTROL_ADDR_MEM2_OFFSET_DATA      0x70
#define XTRIANGLE_CONTROL_BITS_MEM2_OFFSET_DATA      64
#define XTRIANGLE_CONTROL_ADDR_MEM3_OFFSET_DATA      0x80
#define XTRIANGLE_CONTROL_BITS_MEM3_OFFSET_DATA      64
#define XTRIANGLE_CONTROL_ADDR_LEN_IN_BIG_WORDS_DATA 0x90
#define XTRIANGLE_CONTROL_BITS_LEN_IN_BIG_WORDS_DATA 64
#define XTRIANGLE_CONTROL_ADDR_OUTS_DATA             0xa0
#define XTRIANGLE_CONTROL_BITS_OUTS_DATA             64

