// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xtriangle.h"

extern XTriangle_Config XTriangle_ConfigTable[];

XTriangle_Config *XTriangle_LookupConfig(u16 DeviceId) {
	XTriangle_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XTRIANGLE_NUM_INSTANCES; Index++) {
		if (XTriangle_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XTriangle_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XTriangle_Initialize(XTriangle *InstancePtr, u16 DeviceId) {
	XTriangle_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XTriangle_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XTriangle_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

