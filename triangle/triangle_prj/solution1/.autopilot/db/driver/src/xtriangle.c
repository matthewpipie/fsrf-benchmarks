// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xtriangle.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XTriangle_CfgInitialize(XTriangle *InstancePtr, XTriangle_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Control_BaseAddress = ConfigPtr->Control_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XTriangle_Start(XTriangle *InstancePtr) {
    u64 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_CTRL) & 0x80;
    XTriangle_WriteReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_CTRL, Data | 0x01);
}

u64 XTriangle_IsDone(XTriangle *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u64 XTriangle_IsIdle(XTriangle *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u64 XTriangle_IsReady(XTriangle *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XTriangle_Continue(XTriangle *InstancePtr) {
    u64 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_CTRL) & 0x80;
    XTriangle_WriteReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_CTRL, Data | 0x10);
}

void XTriangle_EnableAutoRestart(XTriangle *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XTriangle_WriteReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_CTRL, 0x80);
}

void XTriangle_DisableAutoRestart(XTriangle *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XTriangle_WriteReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_CTRL, 0);
}

XTriangle_Return XTriangle_Get_return(XTriangle *InstancePtr) {
    XTriangle_Return Data;

    Data.word_0 = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_RETURN + 0);
    Data.word_1 = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_RETURN + 8);
    Data.word_2 = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_RETURN + 16);
    Data.word_3 = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_RETURN + 24);
    Data.word_4 = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_RETURN + 32);
    Data.word_5 = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_RETURN + 40);
    Data.word_6 = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_AP_RETURN + 48);
    return Data;
}
void XTriangle_Set_mem1_offset(XTriangle *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XTriangle_WriteReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_MEM1_OFFSET_DATA, Data);
}

u64 XTriangle_Get_mem1_offset(XTriangle *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_MEM1_OFFSET_DATA);
    return Data;
}

void XTriangle_Set_mem2_offset(XTriangle *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XTriangle_WriteReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_MEM2_OFFSET_DATA, Data);
}

u64 XTriangle_Get_mem2_offset(XTriangle *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_MEM2_OFFSET_DATA);
    return Data;
}

void XTriangle_Set_mem3_offset(XTriangle *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XTriangle_WriteReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_MEM3_OFFSET_DATA, Data);
}

u64 XTriangle_Get_mem3_offset(XTriangle *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_MEM3_OFFSET_DATA);
    return Data;
}

void XTriangle_Set_len_in_big_words(XTriangle *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XTriangle_WriteReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_LEN_IN_BIG_WORDS_DATA, Data);
}

u64 XTriangle_Get_len_in_big_words(XTriangle *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_LEN_IN_BIG_WORDS_DATA);
    return Data;
}

void XTriangle_Set_outs(XTriangle *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XTriangle_WriteReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_OUTS_DATA, Data);
}

u64 XTriangle_Get_outs(XTriangle *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_OUTS_DATA);
    return Data;
}

void XTriangle_InterruptGlobalEnable(XTriangle *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XTriangle_WriteReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_GIE, 1);
}

void XTriangle_InterruptGlobalDisable(XTriangle *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XTriangle_WriteReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_GIE, 0);
}

void XTriangle_InterruptEnable(XTriangle *InstancePtr, u64 Mask) {
    u64 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_IER);
    XTriangle_WriteReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_IER, Register | Mask);
}

void XTriangle_InterruptDisable(XTriangle *InstancePtr, u64 Mask) {
    u64 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_IER);
    XTriangle_WriteReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_IER, Register & (~Mask));
}

void XTriangle_InterruptClear(XTriangle *InstancePtr, u64 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XTriangle_WriteReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_ISR, Mask);
}

u64 XTriangle_InterruptGetEnabled(XTriangle *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_IER);
}

u64 XTriangle_InterruptGetStatus(XTriangle *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XTriangle_ReadReg(InstancePtr->Control_BaseAddress, XTRIANGLE_CONTROL_ADDR_ISR);
}

