// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
 `timescale 1ns/1ps


`define AUTOTB_DUT      triangle
`define AUTOTB_DUT_INST AESL_inst_triangle
`define AUTOTB_TOP      apatb_triangle_top
`define AUTOTB_LAT_RESULT_FILE "triangle.result.lat.rb"
`define AUTOTB_PER_RESULT_TRANS_FILE "triangle.performance.result.transaction.xml"
`define AUTOTB_TOP_INST AESL_inst_apatb_triangle_top
`define AUTOTB_MAX_ALLOW_LATENCY  15000000
`define AUTOTB_CLOCK_PERIOD_DIV2 2.00

`define AESL_DEPTH_mem1 1
`define AESL_DEPTH_mem2 1
`define AESL_DEPTH_mem3 1
`define AESL_DEPTH_gmem 1
`define AESL_DEPTH_mem1_offset 1
`define AESL_DEPTH_mem2_offset 1
`define AESL_DEPTH_mem3_offset 1
`define AESL_DEPTH_len_in_big_words 1
`define AESL_DEPTH_outs 1
`define AUTOTB_TVIN_mem1  "./c.triangle.autotvin_mem1.dat"
`define AUTOTB_TVIN_mem2  "./c.triangle.autotvin_mem2.dat"
`define AUTOTB_TVIN_mem3  "./c.triangle.autotvin_mem3.dat"
`define AUTOTB_TVIN_mem1_offset  "./c.triangle.autotvin_mem1_offset.dat"
`define AUTOTB_TVIN_mem2_offset  "./c.triangle.autotvin_mem2_offset.dat"
`define AUTOTB_TVIN_mem3_offset  "./c.triangle.autotvin_mem3_offset.dat"
`define AUTOTB_TVIN_len_in_big_words  "./c.triangle.autotvin_len_in_big_words.dat"
`define AUTOTB_TVIN_mem1_out_wrapc  "./rtl.triangle.autotvin_mem1.dat"
`define AUTOTB_TVIN_mem2_out_wrapc  "./rtl.triangle.autotvin_mem2.dat"
`define AUTOTB_TVIN_mem3_out_wrapc  "./rtl.triangle.autotvin_mem3.dat"
`define AUTOTB_TVIN_mem1_offset_out_wrapc  "./rtl.triangle.autotvin_mem1_offset.dat"
`define AUTOTB_TVIN_mem2_offset_out_wrapc  "./rtl.triangle.autotvin_mem2_offset.dat"
`define AUTOTB_TVIN_mem3_offset_out_wrapc  "./rtl.triangle.autotvin_mem3_offset.dat"
`define AUTOTB_TVIN_len_in_big_words_out_wrapc  "./rtl.triangle.autotvin_len_in_big_words.dat"
`define AUTOTB_TVOUT_ap_return  "./c.triangle.autotvout_ap_return.dat"
`define AUTOTB_TVOUT_ap_return_out_wrapc  "./impl_rtl.triangle.autotvout_ap_return.dat"
module `AUTOTB_TOP;

parameter AUTOTB_TRANSACTION_NUM = 1;
parameter PROGRESS_TIMEOUT = 10000000;
parameter LATENCY_ESTIMATION = -1;
parameter LENGTH_mem1 = 5;
parameter LENGTH_mem2 = 5;
parameter LENGTH_mem3 = 5;
parameter LENGTH_gmem = 5;
parameter LENGTH_mem1_offset = 1;
parameter LENGTH_mem2_offset = 1;
parameter LENGTH_mem3_offset = 1;
parameter LENGTH_len_in_big_words = 1;
parameter LENGTH_outs = 1;
parameter LENGTH_ap_return = 1;

task read_token;
    input integer fp;
    output reg [1047 : 0] token;
    integer ret;
    begin
        token = "";
        ret = 0;
        ret = $fscanf(fp,"%s",token);
    end
endtask

task post_check;
    input integer fp1;
    input integer fp2;
    reg [1047 : 0] token1;
    reg [1047 : 0] token2;
    reg [1047 : 0] golden;
    reg [1047 : 0] result;
    integer ret;
    begin
        read_token(fp1, token1);
        read_token(fp2, token2);
        if (token1 != "[[[runtime]]]" || token2 != "[[[runtime]]]") begin
            $display("ERROR: Simulation using HLS TB failed.");
            $finish;
        end
        read_token(fp1, token1);
        read_token(fp2, token2);
        while (token1 != "[[[/runtime]]]" && token2 != "[[[/runtime]]]") begin
            if (token1 != "[[transaction]]" || token2 != "[[transaction]]") begin
                $display("ERROR: Simulation using HLS TB failed.");
                  $finish;
            end
            read_token(fp1, token1);  // skip transaction number
            read_token(fp2, token2);  // skip transaction number
              read_token(fp1, token1);
              read_token(fp2, token2);
            while(token1 != "[[/transaction]]" && token2 != "[[/transaction]]") begin
                ret = $sscanf(token1, "0x%x", golden);
                  if (ret != 1) begin
                      $display("Failed to parse token!");
                    $display("ERROR: Simulation using HLS TB failed.");
                      $finish;
                  end
                ret = $sscanf(token2, "0x%x", result);
                  if (ret != 1) begin
                      $display("Failed to parse token!");
                    $display("ERROR: Simulation using HLS TB failed.");
                      $finish;
                  end
                if(golden != result) begin
                      $display("%x (expected) vs. %x (actual) - mismatch", golden, result);
                    $display("ERROR: Simulation using HLS TB failed.");
                      $finish;
                end
                  read_token(fp1, token1);
                  read_token(fp2, token2);
            end
              read_token(fp1, token1);
              read_token(fp2, token2);
        end
    end
endtask

reg AESL_clock;
reg rst;
reg dut_rst;
reg start;
reg ce;
reg tb_continue;
wire AESL_start;
wire AESL_reset;
wire AESL_ce;
wire AESL_ready;
wire AESL_idle;
wire AESL_continue;
wire AESL_done;
reg AESL_done_delay = 0;
reg AESL_done_delay2 = 0;
reg AESL_ready_delay = 0;
wire ready;
wire ready_wire;
wire [7 : 0] control_AWADDR;
wire  control_AWVALID;
wire  control_AWREADY;
wire  control_WVALID;
wire  control_WREADY;
wire [63 : 0] control_WDATA;
wire [7 : 0] control_WSTRB;
wire [7 : 0] control_ARADDR;
wire  control_ARVALID;
wire  control_ARREADY;
wire  control_RVALID;
wire  control_RREADY;
wire [63 : 0] control_RDATA;
wire [1 : 0] control_RRESP;
wire  control_BVALID;
wire  control_BREADY;
wire [1 : 0] control_BRESP;
wire  control_INTERRUPT;
wire  mem1_AWVALID;
wire  mem1_AWREADY;
wire [63 : 0] mem1_AWADDR;
wire [0 : 0] mem1_AWID;
wire [7 : 0] mem1_AWLEN;
wire [2 : 0] mem1_AWSIZE;
wire [1 : 0] mem1_AWBURST;
wire [1 : 0] mem1_AWLOCK;
wire [3 : 0] mem1_AWCACHE;
wire [2 : 0] mem1_AWPROT;
wire [3 : 0] mem1_AWQOS;
wire [3 : 0] mem1_AWREGION;
wire [0 : 0] mem1_AWUSER;
wire  mem1_WVALID;
wire  mem1_WREADY;
wire [511 : 0] mem1_WDATA;
wire [63 : 0] mem1_WSTRB;
wire  mem1_WLAST;
wire [0 : 0] mem1_WID;
wire [0 : 0] mem1_WUSER;
wire  mem1_ARVALID;
wire  mem1_ARREADY;
wire [63 : 0] mem1_ARADDR;
wire [0 : 0] mem1_ARID;
wire [7 : 0] mem1_ARLEN;
wire [2 : 0] mem1_ARSIZE;
wire [1 : 0] mem1_ARBURST;
wire [1 : 0] mem1_ARLOCK;
wire [3 : 0] mem1_ARCACHE;
wire [2 : 0] mem1_ARPROT;
wire [3 : 0] mem1_ARQOS;
wire [3 : 0] mem1_ARREGION;
wire [0 : 0] mem1_ARUSER;
wire  mem1_RVALID;
wire  mem1_RREADY;
wire [511 : 0] mem1_RDATA;
wire  mem1_RLAST;
wire [0 : 0] mem1_RID;
wire [0 : 0] mem1_RUSER;
wire [1 : 0] mem1_RRESP;
wire  mem1_BVALID;
wire  mem1_BREADY;
wire [1 : 0] mem1_BRESP;
wire [0 : 0] mem1_BID;
wire [0 : 0] mem1_BUSER;
wire  mem2_AWVALID;
wire  mem2_AWREADY;
wire [63 : 0] mem2_AWADDR;
wire [0 : 0] mem2_AWID;
wire [7 : 0] mem2_AWLEN;
wire [2 : 0] mem2_AWSIZE;
wire [1 : 0] mem2_AWBURST;
wire [1 : 0] mem2_AWLOCK;
wire [3 : 0] mem2_AWCACHE;
wire [2 : 0] mem2_AWPROT;
wire [3 : 0] mem2_AWQOS;
wire [3 : 0] mem2_AWREGION;
wire [0 : 0] mem2_AWUSER;
wire  mem2_WVALID;
wire  mem2_WREADY;
wire [511 : 0] mem2_WDATA;
wire [63 : 0] mem2_WSTRB;
wire  mem2_WLAST;
wire [0 : 0] mem2_WID;
wire [0 : 0] mem2_WUSER;
wire  mem2_ARVALID;
wire  mem2_ARREADY;
wire [63 : 0] mem2_ARADDR;
wire [0 : 0] mem2_ARID;
wire [7 : 0] mem2_ARLEN;
wire [2 : 0] mem2_ARSIZE;
wire [1 : 0] mem2_ARBURST;
wire [1 : 0] mem2_ARLOCK;
wire [3 : 0] mem2_ARCACHE;
wire [2 : 0] mem2_ARPROT;
wire [3 : 0] mem2_ARQOS;
wire [3 : 0] mem2_ARREGION;
wire [0 : 0] mem2_ARUSER;
wire  mem2_RVALID;
wire  mem2_RREADY;
wire [511 : 0] mem2_RDATA;
wire  mem2_RLAST;
wire [0 : 0] mem2_RID;
wire [0 : 0] mem2_RUSER;
wire [1 : 0] mem2_RRESP;
wire  mem2_BVALID;
wire  mem2_BREADY;
wire [1 : 0] mem2_BRESP;
wire [0 : 0] mem2_BID;
wire [0 : 0] mem2_BUSER;
wire  mem3_AWVALID;
wire  mem3_AWREADY;
wire [63 : 0] mem3_AWADDR;
wire [0 : 0] mem3_AWID;
wire [7 : 0] mem3_AWLEN;
wire [2 : 0] mem3_AWSIZE;
wire [1 : 0] mem3_AWBURST;
wire [1 : 0] mem3_AWLOCK;
wire [3 : 0] mem3_AWCACHE;
wire [2 : 0] mem3_AWPROT;
wire [3 : 0] mem3_AWQOS;
wire [3 : 0] mem3_AWREGION;
wire [0 : 0] mem3_AWUSER;
wire  mem3_WVALID;
wire  mem3_WREADY;
wire [511 : 0] mem3_WDATA;
wire [63 : 0] mem3_WSTRB;
wire  mem3_WLAST;
wire [0 : 0] mem3_WID;
wire [0 : 0] mem3_WUSER;
wire  mem3_ARVALID;
wire  mem3_ARREADY;
wire [63 : 0] mem3_ARADDR;
wire [0 : 0] mem3_ARID;
wire [7 : 0] mem3_ARLEN;
wire [2 : 0] mem3_ARSIZE;
wire [1 : 0] mem3_ARBURST;
wire [1 : 0] mem3_ARLOCK;
wire [3 : 0] mem3_ARCACHE;
wire [2 : 0] mem3_ARPROT;
wire [3 : 0] mem3_ARQOS;
wire [3 : 0] mem3_ARREGION;
wire [0 : 0] mem3_ARUSER;
wire  mem3_RVALID;
wire  mem3_RREADY;
wire [511 : 0] mem3_RDATA;
wire  mem3_RLAST;
wire [0 : 0] mem3_RID;
wire [0 : 0] mem3_RUSER;
wire [1 : 0] mem3_RRESP;
wire  mem3_BVALID;
wire  mem3_BREADY;
wire [1 : 0] mem3_BRESP;
wire [0 : 0] mem3_BID;
wire [0 : 0] mem3_BUSER;
wire  m_axi_gmem_AWVALID;
wire  m_axi_gmem_AWREADY;
wire [63 : 0] m_axi_gmem_AWADDR;
wire [0 : 0] m_axi_gmem_AWID;
wire [7 : 0] m_axi_gmem_AWLEN;
wire [2 : 0] m_axi_gmem_AWSIZE;
wire [1 : 0] m_axi_gmem_AWBURST;
wire [1 : 0] m_axi_gmem_AWLOCK;
wire [3 : 0] m_axi_gmem_AWCACHE;
wire [2 : 0] m_axi_gmem_AWPROT;
wire [3 : 0] m_axi_gmem_AWQOS;
wire [3 : 0] m_axi_gmem_AWREGION;
wire [0 : 0] m_axi_gmem_AWUSER;
wire  m_axi_gmem_WVALID;
wire  m_axi_gmem_WREADY;
wire [511 : 0] m_axi_gmem_WDATA;
wire [63 : 0] m_axi_gmem_WSTRB;
wire  m_axi_gmem_WLAST;
wire [0 : 0] m_axi_gmem_WID;
wire [0 : 0] m_axi_gmem_WUSER;
wire  m_axi_gmem_ARVALID;
wire  m_axi_gmem_ARREADY;
wire [63 : 0] m_axi_gmem_ARADDR;
wire [0 : 0] m_axi_gmem_ARID;
wire [7 : 0] m_axi_gmem_ARLEN;
wire [2 : 0] m_axi_gmem_ARSIZE;
wire [1 : 0] m_axi_gmem_ARBURST;
wire [1 : 0] m_axi_gmem_ARLOCK;
wire [3 : 0] m_axi_gmem_ARCACHE;
wire [2 : 0] m_axi_gmem_ARPROT;
wire [3 : 0] m_axi_gmem_ARQOS;
wire [3 : 0] m_axi_gmem_ARREGION;
wire [0 : 0] m_axi_gmem_ARUSER;
wire  m_axi_gmem_RVALID;
wire  m_axi_gmem_RREADY;
wire [511 : 0] m_axi_gmem_RDATA;
wire  m_axi_gmem_RLAST;
wire [0 : 0] m_axi_gmem_RID;
wire [0 : 0] m_axi_gmem_RUSER;
wire [1 : 0] m_axi_gmem_RRESP;
wire  m_axi_gmem_BVALID;
wire  m_axi_gmem_BREADY;
wire [1 : 0] m_axi_gmem_BRESP;
wire [0 : 0] m_axi_gmem_BID;
wire [0 : 0] m_axi_gmem_BUSER;
integer done_cnt = 0;
integer AESL_ready_cnt = 0;
integer ready_cnt = 0;
reg ready_initial;
reg ready_initial_n;
reg ready_last_n;
reg ready_delay_last_n;
reg done_delay_last_n;
reg interface_done = 0;
wire control_read_data_finish;
wire control_write_data_finish;
wire AESL_slave_start;
reg AESL_slave_start_lock = 0;
wire AESL_slave_write_start_in;
wire AESL_slave_write_start_finish;
reg AESL_slave_ready;
wire AESL_slave_output_done;
wire AESL_slave_done;
reg ready_rise = 0;
reg start_rise = 0;
reg slave_start_status = 0;
reg slave_done_status = 0;
reg ap_done_lock = 0;

wire ap_clk;
wire ap_rst_n;
wire ap_rst_n_n;

`AUTOTB_DUT `AUTOTB_DUT_INST(
    .s_axi_control_AWADDR(control_AWADDR),
    .s_axi_control_AWVALID(control_AWVALID),
    .s_axi_control_AWREADY(control_AWREADY),
    .s_axi_control_WVALID(control_WVALID),
    .s_axi_control_WREADY(control_WREADY),
    .s_axi_control_WDATA(control_WDATA),
    .s_axi_control_WSTRB(control_WSTRB),
    .s_axi_control_ARADDR(control_ARADDR),
    .s_axi_control_ARVALID(control_ARVALID),
    .s_axi_control_ARREADY(control_ARREADY),
    .s_axi_control_RVALID(control_RVALID),
    .s_axi_control_RREADY(control_RREADY),
    .s_axi_control_RDATA(control_RDATA),
    .s_axi_control_RRESP(control_RRESP),
    .s_axi_control_BVALID(control_BVALID),
    .s_axi_control_BREADY(control_BREADY),
    .s_axi_control_BRESP(control_BRESP),
    .interrupt(control_INTERRUPT),
    .ap_clk(ap_clk),
    .ap_rst_n(ap_rst_n),
    .m_axi_mem1_AWVALID(mem1_AWVALID),
    .m_axi_mem1_AWREADY(mem1_AWREADY),
    .m_axi_mem1_AWADDR(mem1_AWADDR),
    .m_axi_mem1_AWID(mem1_AWID),
    .m_axi_mem1_AWLEN(mem1_AWLEN),
    .m_axi_mem1_AWSIZE(mem1_AWSIZE),
    .m_axi_mem1_AWBURST(mem1_AWBURST),
    .m_axi_mem1_AWLOCK(mem1_AWLOCK),
    .m_axi_mem1_AWCACHE(mem1_AWCACHE),
    .m_axi_mem1_AWPROT(mem1_AWPROT),
    .m_axi_mem1_AWQOS(mem1_AWQOS),
    .m_axi_mem1_AWREGION(mem1_AWREGION),
    .m_axi_mem1_AWUSER(mem1_AWUSER),
    .m_axi_mem1_WVALID(mem1_WVALID),
    .m_axi_mem1_WREADY(mem1_WREADY),
    .m_axi_mem1_WDATA(mem1_WDATA),
    .m_axi_mem1_WSTRB(mem1_WSTRB),
    .m_axi_mem1_WLAST(mem1_WLAST),
    .m_axi_mem1_WID(mem1_WID),
    .m_axi_mem1_WUSER(mem1_WUSER),
    .m_axi_mem1_ARVALID(mem1_ARVALID),
    .m_axi_mem1_ARREADY(mem1_ARREADY),
    .m_axi_mem1_ARADDR(mem1_ARADDR),
    .m_axi_mem1_ARID(mem1_ARID),
    .m_axi_mem1_ARLEN(mem1_ARLEN),
    .m_axi_mem1_ARSIZE(mem1_ARSIZE),
    .m_axi_mem1_ARBURST(mem1_ARBURST),
    .m_axi_mem1_ARLOCK(mem1_ARLOCK),
    .m_axi_mem1_ARCACHE(mem1_ARCACHE),
    .m_axi_mem1_ARPROT(mem1_ARPROT),
    .m_axi_mem1_ARQOS(mem1_ARQOS),
    .m_axi_mem1_ARREGION(mem1_ARREGION),
    .m_axi_mem1_ARUSER(mem1_ARUSER),
    .m_axi_mem1_RVALID(mem1_RVALID),
    .m_axi_mem1_RREADY(mem1_RREADY),
    .m_axi_mem1_RDATA(mem1_RDATA),
    .m_axi_mem1_RLAST(mem1_RLAST),
    .m_axi_mem1_RID(mem1_RID),
    .m_axi_mem1_RUSER(mem1_RUSER),
    .m_axi_mem1_RRESP(mem1_RRESP),
    .m_axi_mem1_BVALID(mem1_BVALID),
    .m_axi_mem1_BREADY(mem1_BREADY),
    .m_axi_mem1_BRESP(mem1_BRESP),
    .m_axi_mem1_BID(mem1_BID),
    .m_axi_mem1_BUSER(mem1_BUSER),
    .m_axi_mem2_AWVALID(mem2_AWVALID),
    .m_axi_mem2_AWREADY(mem2_AWREADY),
    .m_axi_mem2_AWADDR(mem2_AWADDR),
    .m_axi_mem2_AWID(mem2_AWID),
    .m_axi_mem2_AWLEN(mem2_AWLEN),
    .m_axi_mem2_AWSIZE(mem2_AWSIZE),
    .m_axi_mem2_AWBURST(mem2_AWBURST),
    .m_axi_mem2_AWLOCK(mem2_AWLOCK),
    .m_axi_mem2_AWCACHE(mem2_AWCACHE),
    .m_axi_mem2_AWPROT(mem2_AWPROT),
    .m_axi_mem2_AWQOS(mem2_AWQOS),
    .m_axi_mem2_AWREGION(mem2_AWREGION),
    .m_axi_mem2_AWUSER(mem2_AWUSER),
    .m_axi_mem2_WVALID(mem2_WVALID),
    .m_axi_mem2_WREADY(mem2_WREADY),
    .m_axi_mem2_WDATA(mem2_WDATA),
    .m_axi_mem2_WSTRB(mem2_WSTRB),
    .m_axi_mem2_WLAST(mem2_WLAST),
    .m_axi_mem2_WID(mem2_WID),
    .m_axi_mem2_WUSER(mem2_WUSER),
    .m_axi_mem2_ARVALID(mem2_ARVALID),
    .m_axi_mem2_ARREADY(mem2_ARREADY),
    .m_axi_mem2_ARADDR(mem2_ARADDR),
    .m_axi_mem2_ARID(mem2_ARID),
    .m_axi_mem2_ARLEN(mem2_ARLEN),
    .m_axi_mem2_ARSIZE(mem2_ARSIZE),
    .m_axi_mem2_ARBURST(mem2_ARBURST),
    .m_axi_mem2_ARLOCK(mem2_ARLOCK),
    .m_axi_mem2_ARCACHE(mem2_ARCACHE),
    .m_axi_mem2_ARPROT(mem2_ARPROT),
    .m_axi_mem2_ARQOS(mem2_ARQOS),
    .m_axi_mem2_ARREGION(mem2_ARREGION),
    .m_axi_mem2_ARUSER(mem2_ARUSER),
    .m_axi_mem2_RVALID(mem2_RVALID),
    .m_axi_mem2_RREADY(mem2_RREADY),
    .m_axi_mem2_RDATA(mem2_RDATA),
    .m_axi_mem2_RLAST(mem2_RLAST),
    .m_axi_mem2_RID(mem2_RID),
    .m_axi_mem2_RUSER(mem2_RUSER),
    .m_axi_mem2_RRESP(mem2_RRESP),
    .m_axi_mem2_BVALID(mem2_BVALID),
    .m_axi_mem2_BREADY(mem2_BREADY),
    .m_axi_mem2_BRESP(mem2_BRESP),
    .m_axi_mem2_BID(mem2_BID),
    .m_axi_mem2_BUSER(mem2_BUSER),
    .m_axi_mem3_AWVALID(mem3_AWVALID),
    .m_axi_mem3_AWREADY(mem3_AWREADY),
    .m_axi_mem3_AWADDR(mem3_AWADDR),
    .m_axi_mem3_AWID(mem3_AWID),
    .m_axi_mem3_AWLEN(mem3_AWLEN),
    .m_axi_mem3_AWSIZE(mem3_AWSIZE),
    .m_axi_mem3_AWBURST(mem3_AWBURST),
    .m_axi_mem3_AWLOCK(mem3_AWLOCK),
    .m_axi_mem3_AWCACHE(mem3_AWCACHE),
    .m_axi_mem3_AWPROT(mem3_AWPROT),
    .m_axi_mem3_AWQOS(mem3_AWQOS),
    .m_axi_mem3_AWREGION(mem3_AWREGION),
    .m_axi_mem3_AWUSER(mem3_AWUSER),
    .m_axi_mem3_WVALID(mem3_WVALID),
    .m_axi_mem3_WREADY(mem3_WREADY),
    .m_axi_mem3_WDATA(mem3_WDATA),
    .m_axi_mem3_WSTRB(mem3_WSTRB),
    .m_axi_mem3_WLAST(mem3_WLAST),
    .m_axi_mem3_WID(mem3_WID),
    .m_axi_mem3_WUSER(mem3_WUSER),
    .m_axi_mem3_ARVALID(mem3_ARVALID),
    .m_axi_mem3_ARREADY(mem3_ARREADY),
    .m_axi_mem3_ARADDR(mem3_ARADDR),
    .m_axi_mem3_ARID(mem3_ARID),
    .m_axi_mem3_ARLEN(mem3_ARLEN),
    .m_axi_mem3_ARSIZE(mem3_ARSIZE),
    .m_axi_mem3_ARBURST(mem3_ARBURST),
    .m_axi_mem3_ARLOCK(mem3_ARLOCK),
    .m_axi_mem3_ARCACHE(mem3_ARCACHE),
    .m_axi_mem3_ARPROT(mem3_ARPROT),
    .m_axi_mem3_ARQOS(mem3_ARQOS),
    .m_axi_mem3_ARREGION(mem3_ARREGION),
    .m_axi_mem3_ARUSER(mem3_ARUSER),
    .m_axi_mem3_RVALID(mem3_RVALID),
    .m_axi_mem3_RREADY(mem3_RREADY),
    .m_axi_mem3_RDATA(mem3_RDATA),
    .m_axi_mem3_RLAST(mem3_RLAST),
    .m_axi_mem3_RID(mem3_RID),
    .m_axi_mem3_RUSER(mem3_RUSER),
    .m_axi_mem3_RRESP(mem3_RRESP),
    .m_axi_mem3_BVALID(mem3_BVALID),
    .m_axi_mem3_BREADY(mem3_BREADY),
    .m_axi_mem3_BRESP(mem3_BRESP),
    .m_axi_mem3_BID(mem3_BID),
    .m_axi_mem3_BUSER(mem3_BUSER),
    .m_axi_gmem_AWVALID(m_axi_gmem_AWVALID),
    .m_axi_gmem_AWREADY(m_axi_gmem_AWREADY),
    .m_axi_gmem_AWADDR(m_axi_gmem_AWADDR),
    .m_axi_gmem_AWID(m_axi_gmem_AWID),
    .m_axi_gmem_AWLEN(m_axi_gmem_AWLEN),
    .m_axi_gmem_AWSIZE(m_axi_gmem_AWSIZE),
    .m_axi_gmem_AWBURST(m_axi_gmem_AWBURST),
    .m_axi_gmem_AWLOCK(m_axi_gmem_AWLOCK),
    .m_axi_gmem_AWCACHE(m_axi_gmem_AWCACHE),
    .m_axi_gmem_AWPROT(m_axi_gmem_AWPROT),
    .m_axi_gmem_AWQOS(m_axi_gmem_AWQOS),
    .m_axi_gmem_AWREGION(m_axi_gmem_AWREGION),
    .m_axi_gmem_AWUSER(m_axi_gmem_AWUSER),
    .m_axi_gmem_WVALID(m_axi_gmem_WVALID),
    .m_axi_gmem_WREADY(m_axi_gmem_WREADY),
    .m_axi_gmem_WDATA(m_axi_gmem_WDATA),
    .m_axi_gmem_WSTRB(m_axi_gmem_WSTRB),
    .m_axi_gmem_WLAST(m_axi_gmem_WLAST),
    .m_axi_gmem_WID(m_axi_gmem_WID),
    .m_axi_gmem_WUSER(m_axi_gmem_WUSER),
    .m_axi_gmem_ARVALID(m_axi_gmem_ARVALID),
    .m_axi_gmem_ARREADY(m_axi_gmem_ARREADY),
    .m_axi_gmem_ARADDR(m_axi_gmem_ARADDR),
    .m_axi_gmem_ARID(m_axi_gmem_ARID),
    .m_axi_gmem_ARLEN(m_axi_gmem_ARLEN),
    .m_axi_gmem_ARSIZE(m_axi_gmem_ARSIZE),
    .m_axi_gmem_ARBURST(m_axi_gmem_ARBURST),
    .m_axi_gmem_ARLOCK(m_axi_gmem_ARLOCK),
    .m_axi_gmem_ARCACHE(m_axi_gmem_ARCACHE),
    .m_axi_gmem_ARPROT(m_axi_gmem_ARPROT),
    .m_axi_gmem_ARQOS(m_axi_gmem_ARQOS),
    .m_axi_gmem_ARREGION(m_axi_gmem_ARREGION),
    .m_axi_gmem_ARUSER(m_axi_gmem_ARUSER),
    .m_axi_gmem_RVALID(m_axi_gmem_RVALID),
    .m_axi_gmem_RREADY(m_axi_gmem_RREADY),
    .m_axi_gmem_RDATA(m_axi_gmem_RDATA),
    .m_axi_gmem_RLAST(m_axi_gmem_RLAST),
    .m_axi_gmem_RID(m_axi_gmem_RID),
    .m_axi_gmem_RUSER(m_axi_gmem_RUSER),
    .m_axi_gmem_RRESP(m_axi_gmem_RRESP),
    .m_axi_gmem_BVALID(m_axi_gmem_BVALID),
    .m_axi_gmem_BREADY(m_axi_gmem_BREADY),
    .m_axi_gmem_BRESP(m_axi_gmem_BRESP),
    .m_axi_gmem_BID(m_axi_gmem_BID),
    .m_axi_gmem_BUSER(m_axi_gmem_BUSER));

// Assignment for control signal
assign ap_clk = AESL_clock;
assign ap_rst_n = dut_rst;
assign ap_rst_n_n = ~dut_rst;
assign AESL_reset = rst;
assign AESL_start = start;
assign AESL_ce = ce;
assign AESL_continue = tb_continue;
  assign AESL_slave_write_start_in = slave_start_status  & control_write_data_finish;
  assign AESL_slave_start = AESL_slave_write_start_finish;
  assign AESL_done = slave_done_status  & control_read_data_finish;

always @(posedge AESL_clock)
begin
    if(AESL_reset === 0)
    begin
        slave_start_status <= 1;
    end
    else begin
        if (AESL_start == 1 ) begin
            start_rise = 1;
        end
        if (start_rise == 1 && AESL_done == 1 ) begin
            slave_start_status <= 1;
        end
        if (AESL_slave_write_start_in == 1 && AESL_done == 0) begin 
            slave_start_status <= 0;
            start_rise = 0;
        end
    end
end

always @(posedge AESL_clock)
begin
    if(AESL_reset === 0)
    begin
        AESL_slave_ready <= 0;
        ready_rise = 0;
    end
    else begin
        if (AESL_ready == 1 ) begin
            ready_rise = 1;
        end
        if (ready_rise == 1 && AESL_done_delay == 1 ) begin
            AESL_slave_ready <= 1;
        end
        if (AESL_slave_ready == 1) begin 
            AESL_slave_ready <= 0;
            ready_rise = 0;
        end
    end
end

always @ (posedge AESL_clock)
begin
    if (AESL_done == 1) begin
        slave_done_status <= 0;
    end
    else if (AESL_slave_output_done == 1 ) begin
        slave_done_status <= 1;
    end
end









wire    AESL_axi_master_mem1_ready;
wire    AESL_axi_master_mem1_done;
AESL_axi_master_mem1 AESL_AXI_MASTER_mem1(
    .clk   (AESL_clock),
    .reset (AESL_reset),
    .TRAN_mem1_AWVALID (mem1_AWVALID),
    .TRAN_mem1_AWREADY (mem1_AWREADY),
    .TRAN_mem1_AWADDR (mem1_AWADDR),
    .TRAN_mem1_AWID (mem1_AWID),
    .TRAN_mem1_AWLEN (mem1_AWLEN),
    .TRAN_mem1_AWSIZE (mem1_AWSIZE),
    .TRAN_mem1_AWBURST (mem1_AWBURST),
    .TRAN_mem1_AWLOCK (mem1_AWLOCK),
    .TRAN_mem1_AWCACHE (mem1_AWCACHE),
    .TRAN_mem1_AWPROT (mem1_AWPROT),
    .TRAN_mem1_AWQOS (mem1_AWQOS),
    .TRAN_mem1_AWREGION (mem1_AWREGION),
    .TRAN_mem1_AWUSER (mem1_AWUSER),
    .TRAN_mem1_WVALID (mem1_WVALID),
    .TRAN_mem1_WREADY (mem1_WREADY),
    .TRAN_mem1_WDATA (mem1_WDATA),
    .TRAN_mem1_WSTRB (mem1_WSTRB),
    .TRAN_mem1_WLAST (mem1_WLAST),
    .TRAN_mem1_WID (mem1_WID),
    .TRAN_mem1_WUSER (mem1_WUSER),
    .TRAN_mem1_ARVALID (mem1_ARVALID),
    .TRAN_mem1_ARREADY (mem1_ARREADY),
    .TRAN_mem1_ARADDR (mem1_ARADDR),
    .TRAN_mem1_ARID (mem1_ARID),
    .TRAN_mem1_ARLEN (mem1_ARLEN),
    .TRAN_mem1_ARSIZE (mem1_ARSIZE),
    .TRAN_mem1_ARBURST (mem1_ARBURST),
    .TRAN_mem1_ARLOCK (mem1_ARLOCK),
    .TRAN_mem1_ARCACHE (mem1_ARCACHE),
    .TRAN_mem1_ARPROT (mem1_ARPROT),
    .TRAN_mem1_ARQOS (mem1_ARQOS),
    .TRAN_mem1_ARREGION (mem1_ARREGION),
    .TRAN_mem1_ARUSER (mem1_ARUSER),
    .TRAN_mem1_RVALID (mem1_RVALID),
    .TRAN_mem1_RREADY (mem1_RREADY),
    .TRAN_mem1_RDATA (mem1_RDATA),
    .TRAN_mem1_RLAST (mem1_RLAST),
    .TRAN_mem1_RID (mem1_RID),
    .TRAN_mem1_RUSER (mem1_RUSER),
    .TRAN_mem1_RRESP (mem1_RRESP),
    .TRAN_mem1_BVALID (mem1_BVALID),
    .TRAN_mem1_BREADY (mem1_BREADY),
    .TRAN_mem1_BRESP (mem1_BRESP),
    .TRAN_mem1_BID (mem1_BID),
    .TRAN_mem1_BUSER (mem1_BUSER),
    .ready (AESL_axi_master_mem1_ready),
    .done  (AESL_axi_master_mem1_done)
);
assign    AESL_axi_master_mem1_ready    =   ready;
assign    AESL_axi_master_mem1_done    =   AESL_done_delay;
wire    AESL_axi_master_mem2_ready;
wire    AESL_axi_master_mem2_done;
AESL_axi_master_mem2 AESL_AXI_MASTER_mem2(
    .clk   (AESL_clock),
    .reset (AESL_reset),
    .TRAN_mem2_AWVALID (mem2_AWVALID),
    .TRAN_mem2_AWREADY (mem2_AWREADY),
    .TRAN_mem2_AWADDR (mem2_AWADDR),
    .TRAN_mem2_AWID (mem2_AWID),
    .TRAN_mem2_AWLEN (mem2_AWLEN),
    .TRAN_mem2_AWSIZE (mem2_AWSIZE),
    .TRAN_mem2_AWBURST (mem2_AWBURST),
    .TRAN_mem2_AWLOCK (mem2_AWLOCK),
    .TRAN_mem2_AWCACHE (mem2_AWCACHE),
    .TRAN_mem2_AWPROT (mem2_AWPROT),
    .TRAN_mem2_AWQOS (mem2_AWQOS),
    .TRAN_mem2_AWREGION (mem2_AWREGION),
    .TRAN_mem2_AWUSER (mem2_AWUSER),
    .TRAN_mem2_WVALID (mem2_WVALID),
    .TRAN_mem2_WREADY (mem2_WREADY),
    .TRAN_mem2_WDATA (mem2_WDATA),
    .TRAN_mem2_WSTRB (mem2_WSTRB),
    .TRAN_mem2_WLAST (mem2_WLAST),
    .TRAN_mem2_WID (mem2_WID),
    .TRAN_mem2_WUSER (mem2_WUSER),
    .TRAN_mem2_ARVALID (mem2_ARVALID),
    .TRAN_mem2_ARREADY (mem2_ARREADY),
    .TRAN_mem2_ARADDR (mem2_ARADDR),
    .TRAN_mem2_ARID (mem2_ARID),
    .TRAN_mem2_ARLEN (mem2_ARLEN),
    .TRAN_mem2_ARSIZE (mem2_ARSIZE),
    .TRAN_mem2_ARBURST (mem2_ARBURST),
    .TRAN_mem2_ARLOCK (mem2_ARLOCK),
    .TRAN_mem2_ARCACHE (mem2_ARCACHE),
    .TRAN_mem2_ARPROT (mem2_ARPROT),
    .TRAN_mem2_ARQOS (mem2_ARQOS),
    .TRAN_mem2_ARREGION (mem2_ARREGION),
    .TRAN_mem2_ARUSER (mem2_ARUSER),
    .TRAN_mem2_RVALID (mem2_RVALID),
    .TRAN_mem2_RREADY (mem2_RREADY),
    .TRAN_mem2_RDATA (mem2_RDATA),
    .TRAN_mem2_RLAST (mem2_RLAST),
    .TRAN_mem2_RID (mem2_RID),
    .TRAN_mem2_RUSER (mem2_RUSER),
    .TRAN_mem2_RRESP (mem2_RRESP),
    .TRAN_mem2_BVALID (mem2_BVALID),
    .TRAN_mem2_BREADY (mem2_BREADY),
    .TRAN_mem2_BRESP (mem2_BRESP),
    .TRAN_mem2_BID (mem2_BID),
    .TRAN_mem2_BUSER (mem2_BUSER),
    .ready (AESL_axi_master_mem2_ready),
    .done  (AESL_axi_master_mem2_done)
);
assign    AESL_axi_master_mem2_ready    =   ready;
assign    AESL_axi_master_mem2_done    =   AESL_done_delay;
wire    AESL_axi_master_mem3_ready;
wire    AESL_axi_master_mem3_done;
AESL_axi_master_mem3 AESL_AXI_MASTER_mem3(
    .clk   (AESL_clock),
    .reset (AESL_reset),
    .TRAN_mem3_AWVALID (mem3_AWVALID),
    .TRAN_mem3_AWREADY (mem3_AWREADY),
    .TRAN_mem3_AWADDR (mem3_AWADDR),
    .TRAN_mem3_AWID (mem3_AWID),
    .TRAN_mem3_AWLEN (mem3_AWLEN),
    .TRAN_mem3_AWSIZE (mem3_AWSIZE),
    .TRAN_mem3_AWBURST (mem3_AWBURST),
    .TRAN_mem3_AWLOCK (mem3_AWLOCK),
    .TRAN_mem3_AWCACHE (mem3_AWCACHE),
    .TRAN_mem3_AWPROT (mem3_AWPROT),
    .TRAN_mem3_AWQOS (mem3_AWQOS),
    .TRAN_mem3_AWREGION (mem3_AWREGION),
    .TRAN_mem3_AWUSER (mem3_AWUSER),
    .TRAN_mem3_WVALID (mem3_WVALID),
    .TRAN_mem3_WREADY (mem3_WREADY),
    .TRAN_mem3_WDATA (mem3_WDATA),
    .TRAN_mem3_WSTRB (mem3_WSTRB),
    .TRAN_mem3_WLAST (mem3_WLAST),
    .TRAN_mem3_WID (mem3_WID),
    .TRAN_mem3_WUSER (mem3_WUSER),
    .TRAN_mem3_ARVALID (mem3_ARVALID),
    .TRAN_mem3_ARREADY (mem3_ARREADY),
    .TRAN_mem3_ARADDR (mem3_ARADDR),
    .TRAN_mem3_ARID (mem3_ARID),
    .TRAN_mem3_ARLEN (mem3_ARLEN),
    .TRAN_mem3_ARSIZE (mem3_ARSIZE),
    .TRAN_mem3_ARBURST (mem3_ARBURST),
    .TRAN_mem3_ARLOCK (mem3_ARLOCK),
    .TRAN_mem3_ARCACHE (mem3_ARCACHE),
    .TRAN_mem3_ARPROT (mem3_ARPROT),
    .TRAN_mem3_ARQOS (mem3_ARQOS),
    .TRAN_mem3_ARREGION (mem3_ARREGION),
    .TRAN_mem3_ARUSER (mem3_ARUSER),
    .TRAN_mem3_RVALID (mem3_RVALID),
    .TRAN_mem3_RREADY (mem3_RREADY),
    .TRAN_mem3_RDATA (mem3_RDATA),
    .TRAN_mem3_RLAST (mem3_RLAST),
    .TRAN_mem3_RID (mem3_RID),
    .TRAN_mem3_RUSER (mem3_RUSER),
    .TRAN_mem3_RRESP (mem3_RRESP),
    .TRAN_mem3_BVALID (mem3_BVALID),
    .TRAN_mem3_BREADY (mem3_BREADY),
    .TRAN_mem3_BRESP (mem3_BRESP),
    .TRAN_mem3_BID (mem3_BID),
    .TRAN_mem3_BUSER (mem3_BUSER),
    .ready (AESL_axi_master_mem3_ready),
    .done  (AESL_axi_master_mem3_done)
);
assign    AESL_axi_master_mem3_ready    =   ready;
assign    AESL_axi_master_mem3_done    =   AESL_done_delay;

AESL_axi_slave_control AESL_AXI_SLAVE_control(
    .clk   (AESL_clock),
    .reset (AESL_reset),
    .TRAN_s_axi_control_AWADDR (control_AWADDR),
    .TRAN_s_axi_control_AWVALID (control_AWVALID),
    .TRAN_s_axi_control_AWREADY (control_AWREADY),
    .TRAN_s_axi_control_WVALID (control_WVALID),
    .TRAN_s_axi_control_WREADY (control_WREADY),
    .TRAN_s_axi_control_WDATA (control_WDATA),
    .TRAN_s_axi_control_WSTRB (control_WSTRB),
    .TRAN_s_axi_control_ARADDR (control_ARADDR),
    .TRAN_s_axi_control_ARVALID (control_ARVALID),
    .TRAN_s_axi_control_ARREADY (control_ARREADY),
    .TRAN_s_axi_control_RVALID (control_RVALID),
    .TRAN_s_axi_control_RREADY (control_RREADY),
    .TRAN_s_axi_control_RDATA (control_RDATA),
    .TRAN_s_axi_control_RRESP (control_RRESP),
    .TRAN_s_axi_control_BVALID (control_BVALID),
    .TRAN_s_axi_control_BREADY (control_BREADY),
    .TRAN_s_axi_control_BRESP (control_BRESP),
    .TRAN_control_interrupt (control_INTERRUPT),
    .TRAN_control_read_data_finish(control_read_data_finish),
    .TRAN_control_write_data_finish(control_write_data_finish),
    .TRAN_control_ready_out (AESL_ready),
    .TRAN_control_ready_in (AESL_slave_ready),
    .TRAN_control_done_out (AESL_slave_output_done),
    .TRAN_control_idle_out (AESL_idle),
    .TRAN_control_write_start_in     (AESL_slave_write_start_in),
    .TRAN_control_write_start_finish (AESL_slave_write_start_finish),
    .TRAN_control_transaction_done_in (AESL_done_delay),
    .TRAN_control_start_in  (AESL_slave_start)
);


reg dump_tvout_finish_ap_return;

initial begin : dump_tvout_runtime_sign_ap_return
    integer fp;
    dump_tvout_finish_ap_return = 0;
    fp = $fopen(`AUTOTB_TVOUT_ap_return_out_wrapc, "w");
    if (fp == 0) begin
        $display("Failed to open file \"%s\"!", `AUTOTB_TVOUT_ap_return_out_wrapc);
        $display("ERROR: Simulation using HLS TB failed.");
        $finish;
    end
    $fdisplay(fp,"[[[runtime]]]");
    $fclose(fp);
    wait (done_cnt == AUTOTB_TRANSACTION_NUM);
    // last transaction is saved at negedge right after last done
    @ (posedge AESL_clock);
    @ (posedge AESL_clock);
    @ (posedge AESL_clock);
    fp = $fopen(`AUTOTB_TVOUT_ap_return_out_wrapc, "a");
    if (fp == 0) begin
        $display("Failed to open file \"%s\"!", `AUTOTB_TVOUT_ap_return_out_wrapc);
        $display("ERROR: Simulation using HLS TB failed.");
        $finish;
    end
    $fdisplay(fp,"[[[/runtime]]]");
    $fclose(fp);
    dump_tvout_finish_ap_return = 1;
end

initial begin : generate_AESL_ready_cnt_proc
    AESL_ready_cnt = 0;
    wait(AESL_reset === 1);
    while(AESL_ready_cnt != AUTOTB_TRANSACTION_NUM) begin
        while(AESL_ready !== 1) begin
            @(posedge AESL_clock);
            # 0.4;
        end
        @(negedge AESL_clock);
        AESL_ready_cnt = AESL_ready_cnt + 1;
        @(posedge AESL_clock);
        # 0.4;
    end
end

    event next_trigger_ready_cnt;
    
    initial begin : gen_ready_cnt
        ready_cnt = 0;
        wait (AESL_reset === 1);
        forever begin
            @ (posedge AESL_clock);
            if (ready == 1) begin
                if (ready_cnt < AUTOTB_TRANSACTION_NUM) begin
                    ready_cnt = ready_cnt + 1;
                end
            end
            -> next_trigger_ready_cnt;
        end
    end
    
    wire all_finish = (done_cnt == AUTOTB_TRANSACTION_NUM);
    
    // done_cnt
    always @ (posedge AESL_clock) begin
        if (~AESL_reset) begin
            done_cnt <= 0;
        end else begin
            if (AESL_done == 1) begin
                if (done_cnt < AUTOTB_TRANSACTION_NUM) begin
                    done_cnt <= done_cnt + 1;
                end
            end
        end
    end
    
    initial begin : finish_simulation
        integer fp1;
        integer fp2;
        wait (all_finish == 1);
        // last transaction is saved at negedge right after last done
        @ (posedge AESL_clock);
        @ (posedge AESL_clock);
        @ (posedge AESL_clock);
        @ (posedge AESL_clock);
    fp1 = $fopen("./rtl.triangle.autotvout_ap_return.dat", "r");
    fp2 = $fopen("./impl_rtl.triangle.autotvout_ap_return.dat", "r");
    if(fp1 == 0)        // Failed to open file
        $display("Failed to open file \"./rtl.triangle.autotvout_ap_return.dat\"!");
    else if(fp2 == 0)
        $display("Failed to open file \"./impl_rtl.triangle.autotvout_ap_return.dat\"!");
    else begin
        $display("Comparing rtl.triangle.autotvout_ap_return.dat with impl_rtl.triangle.autotvout_ap_return.dat");
        post_check(fp1, fp2);
    end
    $fclose(fp1);
    $fclose(fp2);
        $display("Simulation Passed.");
        $finish;
    end
    
initial begin
    AESL_clock = 0;
    forever #`AUTOTB_CLOCK_PERIOD_DIV2 AESL_clock = ~AESL_clock;
end


reg end_mem1;
reg [31:0] size_mem1;
reg [31:0] size_mem1_backup;
reg end_mem2;
reg [31:0] size_mem2;
reg [31:0] size_mem2_backup;
reg end_mem3;
reg [31:0] size_mem3;
reg [31:0] size_mem3_backup;
reg end_mem1_offset;
reg [31:0] size_mem1_offset;
reg [31:0] size_mem1_offset_backup;
reg end_mem2_offset;
reg [31:0] size_mem2_offset;
reg [31:0] size_mem2_offset_backup;
reg end_mem3_offset;
reg [31:0] size_mem3_offset;
reg [31:0] size_mem3_offset_backup;
reg end_len_in_big_words;
reg [31:0] size_len_in_big_words;
reg [31:0] size_len_in_big_words_backup;
reg end_ap_return;
reg [31:0] size_ap_return;
reg [31:0] size_ap_return_backup;

initial begin : initial_process
    integer proc_rand;
    rst = 0;
    # 100;
    repeat(3+3) @ (posedge AESL_clock);
    rst = 1;
end
initial begin : initial_process_for_dut_rst
    integer proc_rand;
    dut_rst = 0;
    # 100;
    repeat(3) @ (posedge AESL_clock);
    dut_rst = 1;
end
initial begin : start_process
    integer proc_rand;
    reg [31:0] start_cnt;
    ce = 1;
    start = 0;
    start_cnt = 0;
    wait (AESL_reset === 1);
    @ (posedge AESL_clock);
    #0 start = 1;
    start_cnt = start_cnt + 1;
    forever begin
        if (start_cnt >= AUTOTB_TRANSACTION_NUM + 1) begin
            #0 start = 0;
        end
        @ (posedge AESL_clock);
        if (AESL_ready) begin
            start_cnt = start_cnt + 1;
        end
    end
end

always @(AESL_done)
begin
    tb_continue = AESL_done;
end

initial begin : ready_initial_process
    ready_initial = 0;
    wait (AESL_start === 1);
    ready_initial = 1;
    @(posedge AESL_clock);
    ready_initial = 0;
end

always @(posedge AESL_clock)
begin
    if(AESL_reset === 0)
      AESL_ready_delay = 0;
  else
      AESL_ready_delay = AESL_ready;
end
initial begin : ready_last_n_process
  ready_last_n = 1;
  wait(ready_cnt == AUTOTB_TRANSACTION_NUM)
  @(posedge AESL_clock);
  ready_last_n <= 0;
end

always @(posedge AESL_clock)
begin
    if(AESL_reset === 0)
      ready_delay_last_n = 0;
  else
      ready_delay_last_n <= ready_last_n;
end
assign ready = (ready_initial | AESL_ready_delay);
assign ready_wire = ready_initial | AESL_ready_delay;
initial begin : done_delay_last_n_process
  done_delay_last_n = 1;
  while(done_cnt < AUTOTB_TRANSACTION_NUM)
      @(posedge AESL_clock);
  # 0.1;
  done_delay_last_n = 0;
end

always @(posedge AESL_clock)
begin
    if(AESL_reset === 0)
  begin
      AESL_done_delay <= 0;
      AESL_done_delay2 <= 0;
  end
  else begin
      AESL_done_delay <= AESL_done & done_delay_last_n;
      AESL_done_delay2 <= AESL_done_delay;
  end
end
always @(posedge AESL_clock)
begin
    if(AESL_reset === 0)
      interface_done = 0;
  else begin
      # 0.01;
      if(ready === 1 && ready_cnt > 0 && ready_cnt < AUTOTB_TRANSACTION_NUM)
          interface_done = 1;
      else if(AESL_done_delay === 1 && done_cnt == AUTOTB_TRANSACTION_NUM)
          interface_done = 1;
      else
          interface_done = 0;
  end
end

////////////////////////////////////////////
// progress and performance
////////////////////////////////////////////

task wait_start();
    while (~AESL_start) begin
        @ (posedge AESL_clock);
    end
endtask

reg [31:0] clk_cnt = 0;
reg AESL_ready_p1;
reg AESL_start_p1;

always @ (posedge AESL_clock) begin
    if (AESL_reset == 0) begin
        clk_cnt <= 32'h0;
        AESL_ready_p1 <= 1'b0;
        AESL_start_p1 <= 1'b0;
    end
    else begin
        clk_cnt <= clk_cnt + 1;
        AESL_ready_p1 <= AESL_ready;
        AESL_start_p1 <= AESL_start;
    end
end

reg [31:0] start_timestamp [0:AUTOTB_TRANSACTION_NUM - 1];
reg [31:0] start_cnt;
reg [31:0] ready_timestamp [0:AUTOTB_TRANSACTION_NUM - 1];
reg [31:0] ap_ready_cnt;
reg [31:0] finish_timestamp [0:AUTOTB_TRANSACTION_NUM - 1];
reg [31:0] finish_cnt;
reg [31:0] lat_total;
event report_progress;

always @(posedge AESL_clock)
begin
    if (finish_cnt == AUTOTB_TRANSACTION_NUM - 1 && AESL_done == 1'b1)
        lat_total = clk_cnt - start_timestamp[0];
end

initial begin
    start_cnt = 0;
    finish_cnt = 0;
    ap_ready_cnt = 0;
    wait (AESL_reset == 1);
    wait_start();
    start_timestamp[start_cnt] = clk_cnt;
    start_cnt = start_cnt + 1;
    if (AESL_done) begin
        finish_timestamp[finish_cnt] = clk_cnt;
        finish_cnt = finish_cnt + 1;
    end
    -> report_progress;
    forever begin
        @ (posedge AESL_clock);
        if (start_cnt < AUTOTB_TRANSACTION_NUM) begin
            if ((AESL_start && AESL_ready_p1)||(AESL_start && ~AESL_start_p1)) begin
                start_timestamp[start_cnt] = clk_cnt;
                start_cnt = start_cnt + 1;
            end
        end
        if (ap_ready_cnt < AUTOTB_TRANSACTION_NUM) begin
            if (AESL_start_p1 && AESL_ready_p1) begin
                ready_timestamp[ap_ready_cnt] = clk_cnt;
                ap_ready_cnt = ap_ready_cnt + 1;
            end
        end
        if (finish_cnt < AUTOTB_TRANSACTION_NUM) begin
            if (AESL_done) begin
                finish_timestamp[finish_cnt] = clk_cnt;
                finish_cnt = finish_cnt + 1;
            end
        end
        -> report_progress;
    end
end

reg [31:0] progress_timeout;

initial begin : simulation_progress
    real intra_progress;
    wait (AESL_reset == 1);
    progress_timeout = PROGRESS_TIMEOUT;
    $display("////////////////////////////////////////////////////////////////////////////////////");
    $display("// Inter-Transaction Progress: Completed Transaction / Total Transaction");
    $display("// Intra-Transaction Progress: Measured Latency / Latency Estimation * 100%%");
    $display("//");
    $display("// RTL Simulation : \"Inter-Transaction Progress\" [\"Intra-Transaction Progress\"] @ \"Simulation Time\"");
    $display("////////////////////////////////////////////////////////////////////////////////////");
    print_progress();
    while (finish_cnt < AUTOTB_TRANSACTION_NUM) begin
        @ (report_progress);
        if (finish_cnt < AUTOTB_TRANSACTION_NUM) begin
            if (AESL_done) begin
                print_progress();
                progress_timeout = PROGRESS_TIMEOUT;
            end else begin
                if (progress_timeout == 0) begin
                    print_progress();
                    progress_timeout = PROGRESS_TIMEOUT;
                end else begin
                    progress_timeout = progress_timeout - 1;
                end
            end
        end
    end
    print_progress();
    $display("////////////////////////////////////////////////////////////////////////////////////");
    calculate_performance();
end

task get_intra_progress(output real intra_progress);
    begin
        if (start_cnt > finish_cnt) begin
            intra_progress = clk_cnt - start_timestamp[finish_cnt];
        end else if(finish_cnt > 0) begin
            intra_progress = LATENCY_ESTIMATION;
        end else begin
            intra_progress = 0;
        end
        intra_progress = intra_progress / LATENCY_ESTIMATION;
    end
endtask

task print_progress();
    real intra_progress;
    begin
        if (LATENCY_ESTIMATION > 0) begin
            get_intra_progress(intra_progress);
            $display("// RTL Simulation : %0d / %0d [%2.2f%%] @ \"%0t\"", finish_cnt, AUTOTB_TRANSACTION_NUM, intra_progress * 100, $time);
        end else begin
            $display("// RTL Simulation : %0d / %0d [n/a] @ \"%0t\"", finish_cnt, AUTOTB_TRANSACTION_NUM, $time);
        end
    end
endtask

task calculate_performance();
    integer i;
    integer fp;
    reg [31:0] latency [0:AUTOTB_TRANSACTION_NUM - 1];
    reg [31:0] latency_min;
    reg [31:0] latency_max;
    reg [31:0] latency_total;
    reg [31:0] latency_average;
    reg [31:0] interval [0:AUTOTB_TRANSACTION_NUM - 2];
    reg [31:0] interval_min;
    reg [31:0] interval_max;
    reg [31:0] interval_total;
    reg [31:0] interval_average;
    reg [31:0] total_execute_time;
    begin
        latency_min = -1;
        latency_max = 0;
        latency_total = 0;
        interval_min = -1;
        interval_max = 0;
        interval_total = 0;
        total_execute_time = lat_total;

        for (i = 0; i < AUTOTB_TRANSACTION_NUM; i = i + 1) begin
            // calculate latency
            latency[i] = finish_timestamp[i] - start_timestamp[i];
            if (latency[i] > latency_max) latency_max = latency[i];
            if (latency[i] < latency_min) latency_min = latency[i];
            latency_total = latency_total + latency[i];
            // calculate interval
            if (AUTOTB_TRANSACTION_NUM == 1) begin
                interval[i] = 0;
                interval_max = 0;
                interval_min = 0;
                interval_total = 0;
            end else if (i < AUTOTB_TRANSACTION_NUM - 1) begin
                interval[i] = start_timestamp[i + 1] - start_timestamp[i];
                if (interval[i] > interval_max) interval_max = interval[i];
                if (interval[i] < interval_min) interval_min = interval[i];
                interval_total = interval_total + interval[i];
            end
        end

        latency_average = latency_total / AUTOTB_TRANSACTION_NUM;
        if (AUTOTB_TRANSACTION_NUM == 1) begin
            interval_average = 0;
        end else begin
            interval_average = interval_total / (AUTOTB_TRANSACTION_NUM - 1);
        end

        fp = $fopen(`AUTOTB_LAT_RESULT_FILE, "w");

        $fdisplay(fp, "$MAX_LATENCY = \"%0d\"", latency_max);
        $fdisplay(fp, "$MIN_LATENCY = \"%0d\"", latency_min);
        $fdisplay(fp, "$AVER_LATENCY = \"%0d\"", latency_average);
        $fdisplay(fp, "$MAX_THROUGHPUT = \"%0d\"", interval_max);
        $fdisplay(fp, "$MIN_THROUGHPUT = \"%0d\"", interval_min);
        $fdisplay(fp, "$AVER_THROUGHPUT = \"%0d\"", interval_average);
        $fdisplay(fp, "$TOTAL_EXECUTE_TIME = \"%0d\"", total_execute_time);

        $fclose(fp);

        fp = $fopen(`AUTOTB_PER_RESULT_TRANS_FILE, "w");

        $fdisplay(fp, "%20s%16s%16s", "", "latency", "interval");
        if (AUTOTB_TRANSACTION_NUM == 1) begin
            i = 0;
            $fdisplay(fp, "transaction%8d:%16d%16d", i, latency[i], interval[i]);
        end else begin
            for (i = 0; i < AUTOTB_TRANSACTION_NUM; i = i + 1) begin
                if (i < AUTOTB_TRANSACTION_NUM - 1) begin
                    $fdisplay(fp, "transaction%8d:%16d%16d", i, latency[i], interval[i]);
                end else begin
                    $fdisplay(fp, "transaction%8d:%16d               x", i, latency[i]);
                end
            end
        end

        $fclose(fp);
    end
endtask


////////////////////////////////////////////
// Dependence Check
////////////////////////////////////////////

`ifndef POST_SYN

`endif
///////////////////////////////////////////////////////
// dataflow status monitor
///////////////////////////////////////////////////////
dataflow_monitor U_dataflow_monitor(
    .clock(AESL_clock),
    .reset(~rst),
    .finish(all_finish));

`include "fifo_para.vh"

endmodule
