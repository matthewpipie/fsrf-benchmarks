; ModuleID = '/home/centos/src/project_data/triangle/triangle_prj/solution1/.autopilot/db/a.g.ld.5.gdce.bc'
source_filename = "llvm-link"
target datalayout = "e-m:e-i64:64-i128:128-i256:256-i512:512-i1024:1024-i2048:2048-i4096:4096-n8:16:32:64-S128-v16:16-v24:32-v32:32-v48:64-v96:128-v192:256-v256:256-v512:512-v1024:1024"
target triple = "fpga64-xilinx-none"

%struct.ap_uint = type { %struct.ap_int_base }
%struct.ap_int_base = type { %struct.ssdm_int }
%struct.ssdm_int = type { i512 }
%struct.Ret = type { [3 x i64], [3 x i64], i64 }

; Function Attrs: noinline
define internal fastcc void @copy_in([40 x i64]* readonly, [40 x i64]* noalias align 512, [40 x i64]* readonly, [40 x i64]* noalias align 512, [40 x i64]* readonly, [40 x i64]* noalias align 512, [5 x %struct.ap_uint]*, [5 x %struct.ap_uint]* noalias align 512) unnamed_addr #0 {
entry:
  call fastcc void @onebyonecpy_hls.p0a40i64([40 x i64]* align 512 %1, [40 x i64]* %0)
  call fastcc void @onebyonecpy_hls.p0a40i64([40 x i64]* align 512 %3, [40 x i64]* %2)
  call fastcc void @onebyonecpy_hls.p0a40i64([40 x i64]* align 512 %5, [40 x i64]* %4)
  call fastcc void @onebyonecpy_hls.p0a5struct.ap_uint([5 x %struct.ap_uint]* align 512 %7, [5 x %struct.ap_uint]* %6)
  ret void
}

; Function Attrs: argmemonly noinline
define internal fastcc void @onebyonecpy_hls.p0a40i64([40 x i64]* noalias align 512, [40 x i64]* noalias readonly) unnamed_addr #1 {
entry:
  %2 = icmp eq [40 x i64]* %0, null
  %3 = icmp eq [40 x i64]* %1, null
  %4 = or i1 %2, %3
  br i1 %4, label %ret, label %copy

copy:                                             ; preds = %entry
  br label %for.loop

for.loop:                                         ; preds = %for.loop, %copy
  %for.loop.idx3 = phi i64 [ 0, %copy ], [ %for.loop.idx.next, %for.loop ]
  %dst.addr.gep1 = getelementptr [40 x i64], [40 x i64]* %0, i64 0, i64 %for.loop.idx3
  %5 = bitcast i64* %dst.addr.gep1 to i8*
  %src.addr.gep2 = getelementptr [40 x i64], [40 x i64]* %1, i64 0, i64 %for.loop.idx3
  %6 = bitcast i64* %src.addr.gep2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 1 %5, i8* align 1 %6, i64 8, i1 false)
  %for.loop.idx.next = add nuw nsw i64 %for.loop.idx3, 1
  %exitcond = icmp ne i64 %for.loop.idx.next, 40
  br i1 %exitcond, label %for.loop, label %ret

ret:                                              ; preds = %for.loop, %entry
  ret void
}

; Function Attrs: argmemonly nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture writeonly, i8* nocapture readonly, i64, i1) #2

; Function Attrs: noinline
define internal fastcc void @onebyonecpy_hls.p0a5struct.ap_uint([5 x %struct.ap_uint]* noalias align 512, [5 x %struct.ap_uint]* noalias) unnamed_addr #3 {
entry:
  %2 = icmp eq [5 x %struct.ap_uint]* %0, null
  %3 = icmp eq [5 x %struct.ap_uint]* %1, null
  %4 = or i1 %2, %3
  br i1 %4, label %ret, label %copy

copy:                                             ; preds = %entry
  br label %for.loop

for.loop:                                         ; preds = %for.loop.head, %copy
  %for.loop.idx9 = phi i64 [ 0, %copy ], [ %for.loop.idx.next, %for.loop.head ]
  %dst.addr = getelementptr [5 x %struct.ap_uint], [5 x %struct.ap_uint]* %0, i64 0, i64 %for.loop.idx9
  %src.addr = getelementptr [5 x %struct.ap_uint], [5 x %struct.ap_uint]* %1, i64 0, i64 %for.loop.idx9
  %5 = bitcast %struct.ap_uint* %src.addr to i8*
  %6 = call i1 @fpga_fifo_exist_64(i8* %5)
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %for.loop
  call fastcc void @streamcpy_hls.p0struct.ap_uint(%struct.ap_uint* align 64 %dst.addr, %struct.ap_uint* %src.addr)
  br label %for.loop.head

; <label>:8:                                      ; preds = %for.loop
  %src.addr.01 = getelementptr [5 x %struct.ap_uint], [5 x %struct.ap_uint]* %1, i64 0, i64 %for.loop.idx9, i32 0
  %dst.addr.02 = getelementptr [5 x %struct.ap_uint], [5 x %struct.ap_uint]* %0, i64 0, i64 %for.loop.idx9, i32 0
  %9 = bitcast %struct.ap_int_base* %src.addr.01 to i8*
  %10 = call i1 @fpga_fifo_exist_64(i8* %9)
  br i1 %10, label %11, label %12

; <label>:11:                                     ; preds = %8
  call fastcc void @streamcpy_hls.p0struct.ap_int_base(%struct.ap_int_base* align 64 %dst.addr.02, %struct.ap_int_base* %src.addr.01)
  br label %for.loop.head

; <label>:12:                                     ; preds = %8
  %src.addr.0.03 = getelementptr [5 x %struct.ap_uint], [5 x %struct.ap_uint]* %1, i64 0, i64 %for.loop.idx9, i32 0, i32 0
  %dst.addr.0.04 = getelementptr [5 x %struct.ap_uint], [5 x %struct.ap_uint]* %0, i64 0, i64 %for.loop.idx9, i32 0, i32 0
  %13 = bitcast %struct.ssdm_int* %src.addr.0.03 to i8*
  %14 = call i1 @fpga_fifo_exist_64(i8* %13)
  br i1 %14, label %15, label %16

; <label>:15:                                     ; preds = %12
  call fastcc void @streamcpy_hls.p0struct.ssdm_int(%struct.ssdm_int* align 64 %dst.addr.0.04, %struct.ssdm_int* %src.addr.0.03)
  br label %for.loop.head

; <label>:16:                                     ; preds = %12
  %dst.addr.0.0.06.gep7 = getelementptr [5 x %struct.ap_uint], [5 x %struct.ap_uint]* %0, i64 0, i64 %for.loop.idx9, i32 0, i32 0, i32 0
  %17 = bitcast i512* %dst.addr.0.0.06.gep7 to i8*
  %src.addr.0.0.05.gep8 = getelementptr [5 x %struct.ap_uint], [5 x %struct.ap_uint]* %1, i64 0, i64 %for.loop.idx9, i32 0, i32 0, i32 0
  %18 = bitcast i512* %src.addr.0.0.05.gep8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 1 %17, i8* align 1 %18, i64 64, i1 false)
  br label %for.loop.head

for.loop.head:                                    ; preds = %16, %15, %11, %7
  %for.loop.idx.next = add nuw nsw i64 %for.loop.idx9, 1
  %exitcond = icmp ne i64 %for.loop.idx.next, 5
  br i1 %exitcond, label %for.loop, label %ret

ret:                                              ; preds = %for.loop.head, %entry
  ret void
}

declare i1 @fpga_fifo_exist_64(i8*) local_unnamed_addr

; Function Attrs: argmemonly noinline
define internal fastcc void @streamcpy_hls.p0struct.ap_uint(%struct.ap_uint* noalias nocapture align 64, %struct.ap_uint* noalias nocapture) unnamed_addr #4 {
entry:
  %2 = alloca %struct.ap_uint
  br label %empty

empty:                                            ; preds = %push, %entry
  %3 = bitcast %struct.ap_uint* %1 to i8*
  %4 = call i1 @fpga_fifo_not_empty_64(i8* %3)
  br i1 %4, label %push, label %ret

push:                                             ; preds = %empty
  %5 = bitcast %struct.ap_uint* %2 to i8*
  %6 = bitcast %struct.ap_uint* %1 to i8*
  call void @fpga_fifo_pop_64(i8* %5, i8* %6)
  %7 = load volatile %struct.ap_uint, %struct.ap_uint* %2
  %8 = bitcast %struct.ap_uint* %2 to i8*
  %9 = bitcast %struct.ap_uint* %0 to i8*
  call void @fpga_fifo_push_64(i8* %8, i8* %9)
  br label %empty, !llvm.loop !5

ret:                                              ; preds = %empty
  %10 = bitcast %struct.ap_uint* %1 to i8*
  %11 = bitcast %struct.ap_uint* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 1 %11, i8* align 1 %10, i64 64, i1 false)
  ret void
}

; Function Attrs: argmemonly noinline
define internal fastcc void @streamcpy_hls.p0struct.ap_int_base(%struct.ap_int_base* noalias nocapture align 64, %struct.ap_int_base* noalias nocapture) unnamed_addr #4 {
entry:
  %2 = alloca %struct.ap_int_base
  br label %empty

empty:                                            ; preds = %push, %entry
  %3 = bitcast %struct.ap_int_base* %1 to i8*
  %4 = call i1 @fpga_fifo_not_empty_64(i8* %3)
  br i1 %4, label %push, label %ret

push:                                             ; preds = %empty
  %5 = bitcast %struct.ap_int_base* %2 to i8*
  %6 = bitcast %struct.ap_int_base* %1 to i8*
  call void @fpga_fifo_pop_64(i8* %5, i8* %6)
  %7 = load volatile %struct.ap_int_base, %struct.ap_int_base* %2
  %8 = bitcast %struct.ap_int_base* %2 to i8*
  %9 = bitcast %struct.ap_int_base* %0 to i8*
  call void @fpga_fifo_push_64(i8* %8, i8* %9)
  br label %empty, !llvm.loop !7

ret:                                              ; preds = %empty
  %10 = bitcast %struct.ap_int_base* %1 to i8*
  %11 = bitcast %struct.ap_int_base* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 1 %11, i8* align 1 %10, i64 64, i1 false)
  ret void
}

; Function Attrs: argmemonly noinline
define internal fastcc void @streamcpy_hls.p0struct.ssdm_int(%struct.ssdm_int* noalias nocapture align 64, %struct.ssdm_int* noalias nocapture) unnamed_addr #4 {
entry:
  %2 = alloca %struct.ssdm_int
  br label %empty

empty:                                            ; preds = %push, %entry
  %3 = bitcast %struct.ssdm_int* %1 to i8*
  %4 = call i1 @fpga_fifo_not_empty_64(i8* %3)
  br i1 %4, label %push, label %ret

push:                                             ; preds = %empty
  %5 = bitcast %struct.ssdm_int* %2 to i8*
  %6 = bitcast %struct.ssdm_int* %1 to i8*
  call void @fpga_fifo_pop_64(i8* %5, i8* %6)
  %7 = load volatile %struct.ssdm_int, %struct.ssdm_int* %2
  %8 = bitcast %struct.ssdm_int* %2 to i8*
  %9 = bitcast %struct.ssdm_int* %0 to i8*
  call void @fpga_fifo_push_64(i8* %8, i8* %9)
  br label %empty, !llvm.loop !8

ret:                                              ; preds = %empty
  %10 = bitcast %struct.ssdm_int* %1 to i8*
  %11 = bitcast %struct.ssdm_int* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 1 %11, i8* align 1 %10, i64 64, i1 false)
  ret void
}

; Function Attrs: noinline
define internal fastcc void @copy_out([40 x i64]*, [40 x i64]* noalias readonly align 512, [40 x i64]*, [40 x i64]* noalias readonly align 512, [40 x i64]*, [40 x i64]* noalias readonly align 512, [5 x %struct.ap_uint]*, [5 x %struct.ap_uint]* noalias align 512) unnamed_addr #5 {
entry:
  call fastcc void @onebyonecpy_hls.p0a40i64([40 x i64]* %0, [40 x i64]* align 512 %1)
  call fastcc void @onebyonecpy_hls.p0a40i64([40 x i64]* %2, [40 x i64]* align 512 %3)
  call fastcc void @onebyonecpy_hls.p0a40i64([40 x i64]* %4, [40 x i64]* align 512 %5)
  call fastcc void @onebyonecpy_hls.p0a5struct.ap_uint([5 x %struct.ap_uint]* %6, [5 x %struct.ap_uint]* align 512 %7)
  ret void
}

declare void @apatb_triangle_hw(%struct.Ret*, i64*, i64*, i64*, i64, %struct.ap_uint*)

define void @triangle_hw_stub_wrapper(%struct.Ret*, i64*, i64*, i64*, i64, %struct.ap_uint*) #6 {
entry:
  %6 = bitcast i64* %1 to [40 x i64]*
  %7 = bitcast i64* %2 to [40 x i64]*
  %8 = bitcast i64* %3 to [40 x i64]*
  %9 = bitcast %struct.ap_uint* %5 to [5 x %struct.ap_uint]*
  call void @copy_out([40 x i64]* null, [40 x i64]* %6, [40 x i64]* null, [40 x i64]* %7, [40 x i64]* null, [40 x i64]* %8, [5 x %struct.ap_uint]* null, [5 x %struct.ap_uint]* %9)
  %10 = bitcast [40 x i64]* %6 to i64*
  %11 = bitcast [40 x i64]* %7 to i64*
  %12 = bitcast [40 x i64]* %8 to i64*
  %13 = bitcast [5 x %struct.ap_uint]* %9 to %struct.ap_uint*
  call void @triangle_hw_stub(%struct.Ret* %0, i64* %10, i64* %11, i64* %12, i64 %4, %struct.ap_uint* %13)
  call void @copy_in([40 x i64]* null, [40 x i64]* %6, [40 x i64]* null, [40 x i64]* %7, [40 x i64]* null, [40 x i64]* %8, [5 x %struct.ap_uint]* null, [5 x %struct.ap_uint]* %9)
  ret void
}

declare void @triangle_hw_stub(%struct.Ret*, i64*, i64*, i64*, i64, %struct.ap_uint*)

; Function Attrs: noinline
define void @apatb_triangle_ir(i448* %ret, i64* %mem1, i64* %mem2, i64* %mem3, i64 %len_in_big_words, %struct.ap_uint* %outs) #7 {
entry:
  %mem1_copy = alloca [40 x i64], align 512
  %mem2_copy = alloca [40 x i64], align 512
  %mem3_copy = alloca [40 x i64], align 512
  %outs_copy = alloca [5 x %struct.ap_uint], align 512
  %0 = bitcast i64* %mem1 to [40 x i64]*
  %1 = bitcast i64* %mem2 to [40 x i64]*
  %2 = bitcast i64* %mem3 to [40 x i64]*
  %3 = bitcast %struct.ap_uint* %outs to [5 x %struct.ap_uint]*
  call fastcc void @copy_in([40 x i64]* %0, [40 x i64]* nonnull align 512 %mem1_copy, [40 x i64]* %1, [40 x i64]* nonnull align 512 %mem2_copy, [40 x i64]* %2, [40 x i64]* nonnull align 512 %mem3_copy, [5 x %struct.ap_uint]* %3, [5 x %struct.ap_uint]* nonnull align 512 %outs_copy)
  %4 = getelementptr inbounds [40 x i64], [40 x i64]* %mem1_copy, i32 0, i32 0
  %5 = getelementptr inbounds [40 x i64], [40 x i64]* %mem2_copy, i32 0, i32 0
  %6 = getelementptr inbounds [40 x i64], [40 x i64]* %mem3_copy, i32 0, i32 0
  %7 = getelementptr inbounds [5 x %struct.ap_uint], [5 x %struct.ap_uint]* %outs_copy, i32 0, i32 0
  %8 = alloca %struct.Ret
  call void @apatb_triangle_hw(%struct.Ret* %8, i64* %4, i64* %5, i64* %6, i64 %len_in_big_words, %struct.ap_uint* %7)
  %9 = load volatile %struct.Ret, %struct.Ret* %8
  call fastcc void @copy_out([40 x i64]* %0, [40 x i64]* nonnull align 512 %mem1_copy, [40 x i64]* %1, [40 x i64]* nonnull align 512 %mem2_copy, [40 x i64]* %2, [40 x i64]* nonnull align 512 %mem3_copy, [5 x %struct.ap_uint]* %3, [5 x %struct.ap_uint]* nonnull align 512 %outs_copy)
  %10 = bitcast %struct.Ret* %8 to i448*
  %11 = load volatile i448, i448* %10
  store i448 %11, i448* %ret
  ret void
}

declare i1 @fpga_fifo_not_empty_64(i8*)

declare void @fpga_fifo_pop_64(i8*, i8*)

declare void @fpga_fifo_push_64(i8*, i8*)

attributes #0 = { noinline "fpga.wrapper.func"="copyin" }
attributes #1 = { argmemonly noinline "fpga.wrapper.func"="onebyonecpy_hls" }
attributes #2 = { argmemonly nounwind }
attributes #3 = { noinline "fpga.wrapper.func"="onebyonecpy_hls" }
attributes #4 = { argmemonly noinline "fpga.wrapper.func"="streamcpy_hls" }
attributes #5 = { noinline "fpga.wrapper.func"="copyout" }
attributes #6 = { "fpga.wrapper.func"="stub" }
attributes #7 = { noinline "fpga.wrapper.func"="wrapper" }

!llvm.dbg.cu = !{}
!llvm.ident = !{!0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0, !0}
!llvm.module.flags = !{!1, !2, !3}
!blackbox_cfg = !{!4}

!0 = !{!"clang version 7.0.0 "}
!1 = !{i32 2, !"Dwarf Version", i32 4}
!2 = !{i32 2, !"Debug Info Version", i32 3}
!3 = !{i32 1, !"wchar_size", i32 4}
!4 = !{}
!5 = distinct !{!5, !6}
!6 = !{!"llvm.loop.rotate.disable"}
!7 = distinct !{!7, !6}
!8 = distinct !{!8, !6}
