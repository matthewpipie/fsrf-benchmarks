#include "hyperloglog.hpp"
#include "hyperloglog.cpp"

template <int app_num>
void bll_input(volatile ap_uint<512> *input__, uint64_t N__, hls::stream<net_axis<line_width> > & s_axis_input_tuple) {
	for (uint64_t i = 0; i < N__; i++) {
	#pragma HLS PIPELINE II=1
		net_axis<line_width> data_in;
		ap_uint<line_width> data_sent;
		// put data into stream and call HLL
		data_sent = input__[i];
		data_in.data = data_sent;
		data_in.last = 0;
		data_in.keep = 0xFFFFFFFFFFFFFFFF;
		s_axis_input_tuple.write(data_in);
	}
	{
		net_axis<line_width> data_in;
		ap_uint<line_width> data_sent;
		// put data into stream and call HLL
		data_sent = input__[N__-1];
		data_in.data = data_sent;
		data_in.last = 1;
		data_in.keep = 0xFFFFFFFFFFFFFFFF;
		s_axis_input_tuple.write(data_in);
	}

	/*for(uint64_t i=1; i<=2*num_buckets_m;i++) {
#pragma HLS PIPELINE II=1
		net_axis<line_width> data_in;
		ap_uint<line_width> data_sent;
		// put data into stream and call HLL
		data_sent = input__[N__-1];
		data_in.data = data_sent;
		data_in.last = 1;
		data_in.keep = 0;
		s_axis_input_tuple.write(data_in);
	}*/
}
/*
template <int app_num>
void hyperloglog(
		hls::stream<net_axis<line_width> > & s_axis_input_tuple,
		//hls::stream<memCmd>&  m_axis_write_cmd,
		hls::stream<hll_out>&  m_axis_write_data,
		ap_uint<64>   regBaseAddr
				);
*/
template <int app_num>
void bll(uint64_t N__, hls::stream<net_axis<line_width> > & s_axis_input_tuple, hls::stream<hll_out> & m_axis_write_data) {
	ap_uint<64>   regBaseAddr = 0xAAAAAAAA;
	for (uint64_t i = 0; i < (N__ + 1 + 2*num_buckets_m); i++) {
		hyperloglog<app_num>(s_axis_input_tuple,
				m_axis_write_data,
				regBaseAddr);
	}
}
template <int app_num>
void bll_output(volatile ap_uint<512> *input__, uint64_t N__, hls::stream<hll_out> & m_axis_write_data) {
	/*for (uint64_t i = 0; i < N__; i++) {
#pragma HLS PIPELINE II=1
		m_axis_write_data.read();
	}*/
	hll_out cardinality = m_axis_write_data.read();
	float card = cardinality.data;
	uint32_t *vp = (uint32_t*)&card;
	uint32_t v = *vp;
	input__[N__] = v;
}

void hyperloglog_top(
		ap_uint<512> *input__, // the last 512 bits is for output
		uint64_t N__
		/*ap_uint<512> *input2__,
		ap_uint<512> *input3__,
		ap_uint<512> *input4__*/
	){
#pragma HLS INTERFACE m_axi num_write_outstanding=1 num_read_outstanding=6 max_write_burst_length=2 max_widen_bitwidth=512 max_read_burst_length=64 depth=(max_count/16+1) port=input__
//#pragma HLS INTERFACE m_axi num_write_outstanding=1 num_read_outstanding=6 max_write_burst_length=2 max_widen_bitwidth=512 max_read_burst_length=64 depth=(max_count/16+1) port=input2__ bundle=b2
//#pragma HLS INTERFACE m_axi num_write_outstanding=1 num_read_outstanding=6 max_write_burst_length=2 max_widen_bitwidth=512 max_read_burst_length=64 depth=(max_count/16+1) port=input3__ bundle=b3
//#pragma HLS INTERFACE m_axi num_write_outstanding=1 num_read_outstanding=6 max_write_burst_length=2 max_widen_bitwidth=512 max_read_burst_length=64 depth=(max_count/16+1) port=input4__ bundle=b4

	hls::stream<net_axis<line_width> > s_axis_input_tuple;
#pragma HLS stream depth=256 variable=s_axis_input_tuple
	hls::stream<hll_out>  m_axis_write_data;
#pragma HLS stream depth=256 variable=m_axis_write_data

#pragma HLS DATAFLOW

	bll_input<1>(input__, N__, s_axis_input_tuple);
	bll<1>(N__, s_axis_input_tuple, m_axis_write_data);
	bll_output<1>(input__, N__, m_axis_write_data);
/*
	hls::stream<net_axis<line_width> > s_axis_input_tuple2;
#pragma HLS stream depth=256 variable=s_axis_input_tuple2
	hls::stream<hll_out>  m_axis_write_data2;
#pragma HLS stream depth=256 variable=m_axis_write_data2

	bll_input<2>(input2__, N__, s_axis_input_tuple2);
	bll<2>(N__, s_axis_input_tuple2, m_axis_write_data2);
	bll_output<2>(input2__, N__, m_axis_write_data2);

	hls::stream<net_axis<line_width> > s_axis_input_tuple3;
#pragma HLS stream depth=256 variable=s_axis_input_tuple3
	hls::stream<hll_out>  m_axis_write_data3;
#pragma HLS stream depth=256 variable=m_axis_write_data3

	bll_input<3>(input3__, N__, s_axis_input_tuple3);
	bll<3>(N__, s_axis_input_tuple3, m_axis_write_data3);
	bll_output<3>(input3__, N__, m_axis_write_data3);

	hls::stream<net_axis<line_width> > s_axis_input_tuple4;
#pragma HLS stream depth=256 variable=s_axis_input_tuple4
	hls::stream<hll_out>  m_axis_write_data4;
#pragma HLS stream depth=256 variable=m_axis_write_data4

	bll_input<4>(input4__, N__, s_axis_input_tuple4);
	bll<4>(N__, s_axis_input_tuple4, m_axis_write_data4);
	bll_output<4>(input4__, N__, m_axis_write_data4);*/

}




