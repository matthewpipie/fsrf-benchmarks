/**
 * Copyright (c) 2020, Systems Group, ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include "axi_utils.hpp"
#include "mem_utils.hpp"

#include "globals.hpp"

#include "pipeline.hpp"
#include "estimate_cardinality.hpp"

const unsigned max_count = 1000;

const unsigned NUM_PIPELINES = 16;

struct aggrOutput
{
	rank_t	value;
	ap_uint<1>	last;
	aggrOutput() {}
	aggrOutput(rank_t value, ap_uint<1> last)
		:value(value), last(last) {}
};


//forward declarations

#include "globals.hpp"
//#include <cmath>
#ifdef soft_compare

#include <fstream>
#include <cstdlib> // for exit function
#endif

using namespace std;

using namespace hls;

template <int app_num>
void aggr_bucket(
	hls::stream<rank_t> numZerosFifo[NUM_PIPELINES],
	hls::stream<aggrOutput>& zeros
) {
#pragma HLS PIPELINE II=1
#pragma HLS INLINE off

	static bucket_cnt_t iter = 0;
	ap_uint<NUM_PIPELINES> emptyMask = ~0;

#ifdef soft_compare
	  ofstream myfile;
	  myfile.open ("../../../bucket_output.txt", std::ios_base::app);
#endif

	//static enum fState_aggr {aggr = 0, zero_readout} state_aggr;
	{
		emptyMask = ~0;
		//Check all FIFOs if they are empty
		for (int i = 0; i < NUM_PIPELINES; i++)
		{
			#pragma HLS UNROLL
			emptyMask[i] = numZerosFifo[i].empty();
		}
		//If all FIFOs are empty read from them and determine the maximum value
		if (emptyMask == 0)
		{
			rank_t maxNumZeros = 0;
			for (int i = 0; i < NUM_PIPELINES; i++)
			{
				#pragma HLS UNROLL
				rank_t const  numZerosTemp = numZerosFifo[i].read();
				if (maxNumZeros < numZerosTemp)
					maxNumZeros = numZerosTemp;
			}
			iter++;
#ifdef soft_compare
			// write the zero value and the index to a file
			  myfile << iter << " " << maxNumZeros <<endl;
			//
#endif
			zeros.write(aggrOutput(maxNumZeros, iter == num_buckets_m));
			if (iter == num_buckets_m) {
				iter = 0;
#ifdef soft_compare

				myfile.close();
#endif
			}
		}
	}

}

template <int app_num>
void divide_data(
				  hls::stream<net_axis<line_width> >& rawDataIn,
				  hls::stream<dataItem<32> >	dataFifoOut[NUM_PIPELINES]
				  )
{
#pragma HLS PIPELINE II=1
#pragma HLS INLINE off

	if (!rawDataIn.empty())
	{
		net_axis<line_width> currentInput = rawDataIn.read();

		for (int i = 0; i < line_width/32; i++)
		{
			#pragma HLS UNROLL
			if(currentInput.keep(i*4+3,i*4) == 0xF){
				dataFifoOut[i].write(dataItem<32>(currentInput.data(i*32+31, i*32), 1, currentInput.last));
			}else {
				dataFifoOut[i].write(dataItem<32>(0, 0, currentInput.last));
			}

		}

	}

}
#include "axi_utils.hpp"

template <int app_num>
void zero_counter(
	hls::stream<aggrOutput>&	buck_val_in,
	hls::stream<rank_t>&		buck_val_out,
	hls::stream<bucket_cnt_t>&	zero_counter
) {

	#pragma HLS PIPELINE II=1
	#pragma HLS INLINE off

	static bucket_cnt_t zero_count = 0;

	if(!buck_val_in.empty()){
		aggrOutput data_in = buck_val_in.read();
		if(data_in.value==0){
			zero_count++;
		}
		buck_val_out.write(data_in.value);
		zero_counter.write(zero_count);
		if (data_in.last) {
			zero_count = 0;
		}
	}
}

template <int app_num>
void accumulate (
	hls::stream<rank_t>&		buck_val,
	hls::stream<float>&			accm,
	hls::stream<ap_uint<1> >&	accm_done
) {

#pragma HLS PIPELINE II=1
#pragma HLS INLINE off
	/**
	 * Exact accumulation until float conversion for output:
	 *
	 * | <- BUCKET_BITS -> . <- HASH_SIZE-BUCKET_BITS+1 -> |
	 *
	 * Numeric Range:
	 *	0.0 .. (2^BUCKET_BITS)*1.0
	 *
	 * All values are represented exact except for the maximum final result
	 * in the case that *all* buckets report a rank of zero, i.e. empty. The
	 * accumulated sum will then saturate to one ulp lower.
	 */
	typedef ap_ufixed<HASH_SIZE+1, BUCKET_BITS, AP_RND_ZERO, AP_SAT> accu_t;

	static accu_t		summation = 0;
	static bucket_cnt_t	count = 0;
	if(!buck_val.empty()){
		rank_t const  rank = buck_val.read();
		accu_t  d = 0;		// d = 2^(-rank)
		d[HASH_SIZE-BUCKET_BITS+1 - rank] = 1;
		summation += d;
		accm.write(summation.to_float());
		count++;
		if(count == num_buckets_m){
			summation = 0;
			count = 0;
			accm_done.write(1);
		}else{
			accm_done.write(0);
		}
	}
}
template <int app_num>
void write_results_memory(
		hls::stream<float> & cardinality,
        //hls::stream<memCmd>&   m_axis_write_cmd,
        hls::stream<hll_out >&  m_axis_write_data,
        ap_uint<64>  regBaseAddr
		)
{
#pragma HLS PIPELINE II=1
#pragma HLS INLINE off

	float  tempData;
	hll_out  dataIn;

    if(!cardinality.empty()){
        tempData = cardinality.read();
        dataIn.data = tempData;
		dataIn.keep = 0xF;
		dataIn.last = 0x1;
        //m_axis_write_cmd.write(memCmd(regBaseAddr, 4));
        m_axis_write_data.write(dataIn);
    }
}

/*
template <int app_num>
void hyperloglog(
		hls::stream<net_axis<line_width> > & s_axis_input_tuple,
		//hls::stream<memCmd>&  m_axis_write_cmd,
		hls::stream<hll_out>&  m_axis_write_data,
		ap_uint<64>   regBaseAddr
				);
*/
