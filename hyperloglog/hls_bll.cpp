#include <chrono>
#include "aos.hpp"

using namespace std::chrono;

struct config {
	uint64_t src_addr;
	uint64_t num_words;
	//uint64_t num_bytes;
};

int main(int argc, char *argv[]) {
	config configs[4];
	
	uint64_t num_apps = 1;
	if (argc > 1) num_apps = atol(argv[1]);
	if (num_apps > 4 || num_apps < 1) num_apps = 1;
	
	uint64_t length = 24;
	if (argc > 2) length = atol(argv[2]);
	if (length - 1 > 34) length = 24;
	
	bool populate = false;
	if (argc > 3) populate = atoi(argv[3]);
	
	uint64_t mode = 0;
	if (argc > 4) mode = atol(argv[4]);
	if (mode > 2) mode = 0;
	
	uint64_t coyote_config = 0;
	if (argc > 5) coyote_config = atol(argv[5]);
	
	uint64_t log_prefetch_size = 9;
    if (argc > 6) log_prefetch_size = atol(argv[6]);
    if (log_prefetch_size > 9) log_prefetch_size = 9;
	
	configs[0].num_words = 1 << length;
	
	high_resolution_clock::time_point start, end;
	duration<double> diff;
	double seconds;
	
	aos_client *aos[4];
	for (uint64_t app = 0; app < num_apps; ++app) {
		aos[app] = new aos_client();
		aos[app]->set_slot_id(0);
		aos[app]->set_app_id(app);
		aos[app]->connect();
		aos[app]->aos_set_mode(mode, coyote_config);
		aos[app]->aos_set_mode(3, 1 << log_prefetch_size);
	}
	
	int fd[4];
	const char *fnames[4] = {"/mnt/nvme0/file0.bin", "/mnt/nvme0/file1.bin",
	                         "/mnt/nvme0/file2.bin", "/mnt/nvme0/file3.bin"};
	for (uint64_t app = 0; app < num_apps; ++app) {
		aos[app]->aos_file_open(fnames[app], fd[app]);
		//printf("App %lu opened file %d\n", app, fd[app]);
	}
	
	for (uint64_t app = 0; app < num_apps; ++app) {
		void *addr = nullptr;
		int flags = populate ? MAP_POPULATE : 0;
		
		start = high_resolution_clock::now();
		length = configs[0].num_words * 64 + 64;
		aos[app]->aos_mmap(addr, length, PROT_READ | PROT_WRITE, flags, fd[app], 0);
		configs[app].src_addr = (uint64_t)addr;
		end = high_resolution_clock::now();
		
		diff = end - start;
		seconds = diff.count();
		printf("App %lu mmaped file %d at 0x%lX in %g\n", app,
		       fd[app], configs[app].src_addr, seconds);
	}
	
	for (int i = 0; i < 1; ++i) {
		// start runs
		start = high_resolution_clock::now();
		for (uint64_t app = 0; app < num_apps; ++app) {
			aos[app]->aos_cntrlreg_write(0x20, configs[app].src_addr);
			aos[app]->aos_cntrlreg_write(0x30, configs[0].num_words);
uint64_t val;
			aos[app]->aos_cntrlreg_read(0x30, val);
printf("its %lu\n", val);
			aos[app]->aos_cntrlreg_write(0x00, 0x1);
		}
		
		// end runs
		for (uint64_t app = 0; app < num_apps; ++app) {
			uint64_t status = 0;
			do {
				aos[app]->aos_cntrlreg_read(0x0, status);
			} while (!(status & 0x2));
			aos[app]->aos_cntrlreg_write(0x00, 0x10);
		}
		end = high_resolution_clock::now();
		
		// print stats
		uint64_t total_bytes = num_apps * configs[0].num_words * 64;
		diff = end - start;
		seconds = diff.count();
		double throughput = ((double)total_bytes)/seconds/(1<<20);
		//printf("%lu sha e2e: %lu bytes in %g seconds for %g MiB/s\n", num_apps, total_bytes, seconds, throughput);
		printf("%lu hll e2e %lu %g %g\n", num_apps, total_bytes, seconds, throughput);
	}
	
	length = configs[0].num_words * 64+64;
	for (uint64_t app = 0; app < num_apps; ++app) {
		aos[app]->aos_munmap((void*)configs[app].src_addr, length);
		aos[app]->aos_file_close(fd[app]);
	}
	
	return 0;
}
