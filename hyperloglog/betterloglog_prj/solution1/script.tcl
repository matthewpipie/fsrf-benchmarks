############################################################
## This file is generated automatically by Vitis HLS.
## Please DO NOT edit it.
## Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
############################################################
open_project betterloglog_prj
set_top hyperloglog_top
add_files betterloglog/src/accumulate.cpp
add_files betterloglog/src/aggr_bucket.cpp
add_files betterloglog/src/axi_utils.cpp
add_files betterloglog/src/axi_utils.hpp
add_files betterloglog/src/bit_utils.hpp
add_files betterloglog/src/bucket_num_zero_detector.hpp
add_files betterloglog/src/divide_data.cpp
add_files betterloglog/src/estimate_cardinality.hpp
add_files betterloglog/src/fill_bucket.hpp
add_files betterloglog/src/globals.hpp
add_files betterloglog/src/hyperloglog.cpp
add_files betterloglog/src/hyperloglog.hpp
add_files betterloglog/src/hyperloglog_top.cpp
add_files betterloglog/src/mem_utils.hpp
add_files betterloglog/src/murmur3.cpp
add_files betterloglog/src/murmur3.hpp
add_files betterloglog/src/pipeline.hpp
add_files betterloglog/src/write_results_memory.cpp
add_files betterloglog/src/zero_counter.cpp
add_files -tb betterloglog/src/hll_test_bench.cpp -cflags "-Wno-unknown-pragmas" -csimflags "-Wno-unknown-pragmas"
add_files -tb betterloglog/src/hll_test_bench_new.cpp -cflags "-Wno-unknown-pragmas" -csimflags "-Wno-unknown-pragmas"
open_solution "solution1" -flow_target vitis
set_part {xcvu9p-flgb2104-2-i}
create_clock -period 2.5 -name default
config_rtl -register_reset_num 3
config_interface -m_axi_alignment_byte_size 64 -m_axi_latency 64 -m_axi_max_widen_bitwidth 512 -m_axi_offset slave -s_axilite_data64
config_dataflow -default_channel fifo -fifo_depth 8 -scalar_fifo_depth 8 -start_fifo_depth 8 -task_level_fifo_depth 8
source "./betterloglog_prj/solution1/directives.tcl"
csim_design
csynth_design
cosim_design -trace_level port
export_design -rtl verilog -format xo
