# ==============================================================
# Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
# Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
# ==============================================================
CSIM_DESIGN = 1

__SIM_FPO__ = 1

__SIM_MATHHLS__ = 1

__SIM_FFT__ = 1

__SIM_FIR__ = 1

__SIM_DDS__ = 1

ObjDir = obj

HLS_SOURCES = ../../../../betterloglog/src/hll_test_bench.cpp ../../../../betterloglog/src/hll_test_bench_new.cpp ../../../../betterloglog/src/accumulate.cpp ../../../../betterloglog/src/aggr_bucket.cpp ../../../../betterloglog/src/axi_utils.cpp ../../../../betterloglog/src/divide_data.cpp ../../../../betterloglog/src/hyperloglog.cpp ../../../../betterloglog/src/hyperloglog_top.cpp ../../../../betterloglog/src/murmur3.cpp ../../../../betterloglog/src/write_results_memory.cpp ../../../../betterloglog/src/zero_counter.cpp

override TARGET := csim.exe

AUTOPILOT_ROOT := /opt/Xilinx/Vitis_HLS/2020.2
AUTOPILOT_MACH := lnx64
ifdef AP_GCC_M32
  AUTOPILOT_MACH := Linux_x86
  IFLAG += -m32
endif
IFLAG += -fPIC
ifndef AP_GCC_PATH
  AP_GCC_PATH := /opt/Xilinx/Vitis_HLS/2020.2/tps/lnx64/gcc-6.2.0/bin
endif
AUTOPILOT_TOOL := ${AUTOPILOT_ROOT}/${AUTOPILOT_MACH}/tools
AP_CLANG_PATH := ${AUTOPILOT_TOOL}/clang-3.9/bin
AUTOPILOT_TECH := ${AUTOPILOT_ROOT}/common/technology


IFLAG += -I "${AUTOPILOT_TOOL}/systemc/include"
IFLAG += -I "${AUTOPILOT_ROOT}/include"
IFLAG += -I "${AUTOPILOT_ROOT}/include/ap_sysc"
IFLAG += -I "${AUTOPILOT_TECH}/generic/SystemC"
IFLAG += -I "${AUTOPILOT_TECH}/generic/SystemC/AESL_FP_comp"
IFLAG += -I "${AUTOPILOT_TECH}/generic/SystemC/AESL_comp"
IFLAG += -I "${AUTOPILOT_TOOL}/auto_cc/include"
IFLAG += -D__VITIS_HLS__

IFLAG += -D__SIM_FPO__

IFLAG += -D__SIM_FFT__

IFLAG += -D__SIM_FIR__

IFLAG += -D__SIM_DDS__

IFLAG += -D__DSP48E2__
IFLAG += -Wno-unknown-pragmas 
IFLAG += -g
DFLAG += -D__xilinx_ip_top= -DAESL_TB
CCFLAG += -Werror=return-type
TOOLCHAIN += 



include ./Makefile.rules

all: $(TARGET)



$(ObjDir)/hll_test_bench.o: ../../../../betterloglog/src/hll_test_bench.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../betterloglog/src/hll_test_bench.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD -Wno-unknown-pragmas -Wno-unknown-pragmas  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/hll_test_bench.d

$(ObjDir)/hll_test_bench_new.o: ../../../../betterloglog/src/hll_test_bench_new.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../betterloglog/src/hll_test_bench_new.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD -Wno-unknown-pragmas -Wno-unknown-pragmas  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/hll_test_bench_new.d

$(ObjDir)/accumulate.o: ../../../../betterloglog/src/accumulate.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../betterloglog/src/accumulate.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/accumulate.d

$(ObjDir)/aggr_bucket.o: ../../../../betterloglog/src/aggr_bucket.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../betterloglog/src/aggr_bucket.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/aggr_bucket.d

$(ObjDir)/axi_utils.o: ../../../../betterloglog/src/axi_utils.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../betterloglog/src/axi_utils.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/axi_utils.d

$(ObjDir)/divide_data.o: ../../../../betterloglog/src/divide_data.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../betterloglog/src/divide_data.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/divide_data.d

$(ObjDir)/hyperloglog.o: ../../../../betterloglog/src/hyperloglog.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../betterloglog/src/hyperloglog.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/hyperloglog.d

$(ObjDir)/hyperloglog_top.o: ../../../../betterloglog/src/hyperloglog_top.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../betterloglog/src/hyperloglog_top.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/hyperloglog_top.d

$(ObjDir)/murmur3.o: ../../../../betterloglog/src/murmur3.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../betterloglog/src/murmur3.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/murmur3.d

$(ObjDir)/write_results_memory.o: ../../../../betterloglog/src/write_results_memory.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../betterloglog/src/write_results_memory.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/write_results_memory.d

$(ObjDir)/zero_counter.o: ../../../../betterloglog/src/zero_counter.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../betterloglog/src/zero_counter.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/zero_counter.d
