// ==============================================================
// RTL generated by Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Version: 2020.2
// Copyright (C) Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module hyperloglog_top_fill_bucket_1_8_s (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_continue,
        ap_idle,
        ap_ready,
        bucketMetaFifo_1_dout,
        bucketMetaFifo_1_empty_n,
        bucketMetaFifo_1_read,
        bucket_fifo_V_V_8_din,
        bucket_fifo_V_V_8_full_n,
        bucket_fifo_V_V_8_write
);

parameter    ap_ST_fsm_pp0_stage0 = 1'd1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
input   ap_continue;
output   ap_idle;
output   ap_ready;
input  [47:0] bucketMetaFifo_1_dout;
input   bucketMetaFifo_1_empty_n;
output   bucketMetaFifo_1_read;
output  [4:0] bucket_fifo_V_V_8_din;
input   bucket_fifo_V_V_8_full_n;
output   bucket_fifo_V_V_8_write;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg bucketMetaFifo_1_read;
reg bucket_fifo_V_V_8_write;

reg    ap_done_reg;
(* fsm_encoding = "none" *) reg   [0:0] ap_CS_fsm;
wire    ap_CS_fsm_pp0_stage0;
wire    ap_enable_reg_pp0_iter0;
reg    ap_enable_reg_pp0_iter1;
reg    ap_enable_reg_pp0_iter2;
reg    ap_enable_reg_pp0_iter3;
reg    ap_enable_reg_pp0_iter4;
reg    ap_enable_reg_pp0_iter5;
reg    ap_idle_pp0;
reg    ap_block_state1_pp0_stage0_iter0;
wire   [0:0] state_1_load_load_fu_174_p1;
wire   [0:0] tmp_i_nbreadreq_fu_78_p3;
reg    ap_predicate_op18_read_state2;
reg    ap_block_state2_pp0_stage0_iter1;
wire    ap_block_state3_pp0_stage0_iter2;
wire    ap_block_state4_pp0_stage0_iter3;
reg   [0:0] state_1_load_reg_442;
reg   [0:0] state_1_load_reg_442_pp0_iter3_reg;
reg    ap_block_state5_pp0_stage0_iter4;
wire    ap_block_state6_pp0_stage0_iter5;
reg    ap_block_pp0_stage0_11001;
reg   [0:0] state_1;
reg   [15:0] prev_bucketNum_V_1;
reg   [4:0] prev_rank_V_1;
reg   [15:0] prev_prev_bucketNum_V_1;
reg   [4:0] prev_prev_rank_V_1;
reg   [15:0] prev_prev_prev_bucketNum_V_1;
reg   [4:0] prev_prev_prev_rank_V_1;
reg   [15:0] prev_prev_prev_prev_bucketNum_V_1;
reg   [4:0] prev_prev_prev_prev_rank_V_1;
reg   [15:0] buckets_V_1_address0;
reg    buckets_V_1_ce0;
reg    buckets_V_1_we0;
reg   [4:0] buckets_V_1_d0;
reg   [15:0] buckets_V_1_address1;
reg    buckets_V_1_ce1;
wire   [4:0] buckets_V_1_q1;
reg   [16:0] i_V_1;
reg    bucketMetaFifo_1_blk_n;
wire    ap_block_pp0_stage0;
reg    bucket_fifo_V_V_8_blk_n;
reg   [4:0] reg_169;
reg   [0:0] state_1_load_reg_442_pp0_iter2_reg;
reg   [0:0] tmp_i_reg_446;
reg   [0:0] tmp_i_reg_446_pp0_iter2_reg;
reg   [0:0] icmp_ln870_5_reg_463;
reg   [0:0] icmp_ln870_5_reg_463_pp0_iter2_reg;
reg   [0:0] icmp_ln870_7_reg_477;
reg   [0:0] icmp_ln870_7_reg_477_pp0_iter2_reg;
reg   [0:0] icmp_ln870_6_reg_471;
reg   [0:0] icmp_ln870_6_reg_471_pp0_iter2_reg;
reg   [0:0] icmp_ln870_8_reg_483;
reg   [0:0] icmp_ln870_8_reg_483_pp0_iter2_reg;
reg   [0:0] state_1_load_reg_442_pp0_iter4_reg;
reg   [0:0] tmp_i_reg_446_pp0_iter3_reg;
reg   [0:0] tmp_i_reg_446_pp0_iter4_reg;
wire   [15:0] tmp_bucketNum_V_fu_178_p1;
reg   [15:0] tmp_bucketNum_V_reg_450;
reg   [4:0] tmp_numZeros_V_reg_455;
reg   [4:0] tmp_numZeros_V_reg_455_pp0_iter2_reg;
reg   [4:0] tmp_numZeros_V_reg_455_pp0_iter3_reg;
wire   [0:0] icmp_ln870_5_fu_204_p2;
reg   [0:0] icmp_ln870_5_reg_463_pp0_iter3_reg;
wire   [0:0] icmp_ln870_6_fu_218_p2;
reg   [0:0] icmp_ln870_6_reg_471_pp0_iter3_reg;
wire   [0:0] icmp_ln870_7_fu_224_p2;
reg   [0:0] icmp_ln870_7_reg_477_pp0_iter3_reg;
wire   [0:0] icmp_ln870_8_fu_234_p2;
reg   [0:0] icmp_ln870_8_reg_483_pp0_iter3_reg;
reg   [15:0] buckets_V_1_addr_reg_488;
reg   [15:0] buckets_V_1_addr_reg_488_pp0_iter2_reg;
reg   [15:0] buckets_V_1_addr_reg_488_pp0_iter3_reg;
reg   [15:0] buckets_V_1_addr_reg_488_pp0_iter4_reg;
reg   [15:0] buckets_V_1_addr_1_reg_497;
reg   [15:0] buckets_V_1_addr_1_reg_497_pp0_iter3_reg;
reg   [15:0] buckets_V_1_addr_1_reg_497_pp0_iter4_reg;
wire   [4:0] rank_V_fu_302_p2;
reg   [4:0] rank_V_reg_503;
wire   [0:0] icmp_ln886_fu_430_p2;
reg   [0:0] icmp_ln886_reg_508;
reg    ap_block_pp0_stage0_subdone;
reg   [0:0] ap_phi_mux_i_V_29_flag_0_i_phi_fu_127_p10;
wire   [0:0] ap_phi_reg_pp0_iter1_i_V_29_flag_0_i_reg_124;
wire   [0:0] icmp_ln870_fu_286_p2;
wire   [0:0] tmp_last_V_fu_192_p3;
reg   [16:0] ap_phi_mux_i_V_29_new_0_i_phi_fu_147_p10;
wire   [16:0] ap_phi_reg_pp0_iter1_i_V_29_new_0_i_reg_144;
wire   [16:0] add_ln691_fu_279_p2;
wire   [63:0] zext_ln534_fu_274_p1;
wire   [63:0] zext_ln534_2_fu_298_p1;
reg    ap_block_pp0_stage0_01001;
wire   [0:0] or_ln870_fu_323_p2;
wire   [0:0] or_ln870_5_fu_327_p2;
wire   [0:0] xor_ln870_fu_332_p2;
wire   [0:0] xor_ln870_3_fu_343_p2;
wire   [0:0] xor_ln870_4_fu_353_p2;
wire   [0:0] and_ln870_4_fu_359_p2;
wire   [0:0] and_ln870_3_fu_348_p2;
wire   [0:0] and_ln870_fu_338_p2;
wire   [0:0] or_ln870_6_fu_372_p2;
wire   [4:0] select_ln870_fu_364_p3;
wire   [4:0] select_ln870_4_fu_378_p3;
wire   [0:0] or_ln870_7_fu_385_p2;
wire   [0:0] or_ln870_8_fu_398_p2;
wire   [4:0] select_ln870_5_fu_390_p3;
wire   [4:0] current_rank_V_10_fu_404_p3;
reg   [0:0] ap_NS_fsm;
reg    ap_idle_pp0_0to4;
reg    ap_reset_idle_pp0;
reg    ap_block_pp0;
reg    ap_predicate_op52_load_state3;
reg    ap_enable_operation_52;
reg    ap_enable_state3_pp0_iter2_stage0;
reg    ap_predicate_op54_load_state4;
reg    ap_enable_operation_54;
reg    ap_enable_state4_pp0_iter3_stage0;
reg    ap_predicate_op83_store_state6;
reg    ap_enable_operation_83;
reg    ap_enable_state6_pp0_iter5_stage0;
reg    ap_enable_operation_85;
reg    ap_enable_operation_53;
reg    ap_enable_operation_55;
wire    ap_enable_pp0;
reg    ap_condition_311;
reg    ap_condition_298;
wire    ap_ce_reg;

// power-on initialization
initial begin
#0 ap_done_reg = 1'b0;
#0 ap_CS_fsm = 1'd1;
#0 ap_enable_reg_pp0_iter1 = 1'b0;
#0 ap_enable_reg_pp0_iter2 = 1'b0;
#0 ap_enable_reg_pp0_iter3 = 1'b0;
#0 ap_enable_reg_pp0_iter4 = 1'b0;
#0 ap_enable_reg_pp0_iter5 = 1'b0;
#0 state_1 = 1'd0;
#0 prev_bucketNum_V_1 = 16'd0;
#0 prev_rank_V_1 = 5'd0;
#0 prev_prev_bucketNum_V_1 = 16'd0;
#0 prev_prev_rank_V_1 = 5'd0;
#0 prev_prev_prev_bucketNum_V_1 = 16'd0;
#0 prev_prev_prev_rank_V_1 = 5'd0;
#0 prev_prev_prev_prev_bucketNum_V_1 = 16'd0;
#0 prev_prev_prev_prev_rank_V_1 = 5'd0;
#0 i_V_1 = 17'd0;
end

hyperloglog_top_fill_bucket_1_0_s_buckets_V_8 #(
    .DataWidth( 5 ),
    .AddressRange( 65536 ),
    .AddressWidth( 16 ))
buckets_V_1_U(
    .clk(ap_clk),
    .reset(ap_rst),
    .address0(buckets_V_1_address0),
    .ce0(buckets_V_1_ce0),
    .we0(buckets_V_1_we0),
    .d0(buckets_V_1_d0),
    .address1(buckets_V_1_address1),
    .ce1(buckets_V_1_ce1),
    .q1(buckets_V_1_q1)
);

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_fsm_pp0_stage0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_done_reg <= 1'b0;
    end else begin
        if ((ap_continue == 1'b1)) begin
            ap_done_reg <= 1'b0;
        end else if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter5 == 1'b1))) begin
            ap_done_reg <= 1'b1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter1 <= 1'b0;
    end else begin
        if (((1'b0 == ap_block_pp0_stage0_subdone) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
            ap_enable_reg_pp0_iter1 <= ap_start;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter2 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter2 <= ap_enable_reg_pp0_iter1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter3 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter3 <= ap_enable_reg_pp0_iter2;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter4 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter4 <= ap_enable_reg_pp0_iter3;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter5 <= 1'b0;
    end else begin
        if ((1'b0 == ap_block_pp0_stage0_subdone)) begin
            ap_enable_reg_pp0_iter5 <= ap_enable_reg_pp0_iter4;
        end
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_i_nbreadreq_fu_78_p3 == 1'd1) & (state_1 == 1'd0) & (ap_enable_reg_pp0_iter1 == 1'b1) & (tmp_last_V_fu_192_p3 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        state_1 <= 1'd1;
    end else if ((((1'b0 == ap_block_pp0_stage0_11001) & (tmp_i_nbreadreq_fu_78_p3 == 1'd0) & (state_1 == 1'd0) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0)) | ((1'b0 == ap_block_pp0_stage0_11001) & (state_1_load_load_fu_174_p1 == 1'd1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (icmp_ln870_fu_286_p2 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0)))) begin
        state_1 <= 1'd0;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (state_1_load_reg_442 == 1'd0) & (tmp_i_reg_446 == 1'd1))) begin
        buckets_V_1_addr_1_reg_497 <= zext_ln534_2_fu_298_p1;
    end
end

always @ (posedge ap_clk) begin
    if ((1'b0 == ap_block_pp0_stage0_11001)) begin
        buckets_V_1_addr_1_reg_497_pp0_iter3_reg <= buckets_V_1_addr_1_reg_497;
        buckets_V_1_addr_1_reg_497_pp0_iter4_reg <= buckets_V_1_addr_1_reg_497_pp0_iter3_reg;
        buckets_V_1_addr_reg_488_pp0_iter2_reg <= buckets_V_1_addr_reg_488;
        buckets_V_1_addr_reg_488_pp0_iter3_reg <= buckets_V_1_addr_reg_488_pp0_iter2_reg;
        buckets_V_1_addr_reg_488_pp0_iter4_reg <= buckets_V_1_addr_reg_488_pp0_iter3_reg;
        icmp_ln870_5_reg_463_pp0_iter2_reg <= icmp_ln870_5_reg_463;
        icmp_ln870_5_reg_463_pp0_iter3_reg <= icmp_ln870_5_reg_463_pp0_iter2_reg;
        icmp_ln870_6_reg_471_pp0_iter2_reg <= icmp_ln870_6_reg_471;
        icmp_ln870_6_reg_471_pp0_iter3_reg <= icmp_ln870_6_reg_471_pp0_iter2_reg;
        icmp_ln870_7_reg_477_pp0_iter2_reg <= icmp_ln870_7_reg_477;
        icmp_ln870_7_reg_477_pp0_iter3_reg <= icmp_ln870_7_reg_477_pp0_iter2_reg;
        icmp_ln870_8_reg_483_pp0_iter2_reg <= icmp_ln870_8_reg_483;
        icmp_ln870_8_reg_483_pp0_iter3_reg <= icmp_ln870_8_reg_483_pp0_iter2_reg;
        state_1_load_reg_442_pp0_iter2_reg <= state_1_load_reg_442;
        state_1_load_reg_442_pp0_iter3_reg <= state_1_load_reg_442_pp0_iter2_reg;
        state_1_load_reg_442_pp0_iter4_reg <= state_1_load_reg_442_pp0_iter3_reg;
        tmp_i_reg_446_pp0_iter2_reg <= tmp_i_reg_446;
        tmp_i_reg_446_pp0_iter3_reg <= tmp_i_reg_446_pp0_iter2_reg;
        tmp_i_reg_446_pp0_iter4_reg <= tmp_i_reg_446_pp0_iter3_reg;
        tmp_numZeros_V_reg_455_pp0_iter2_reg <= tmp_numZeros_V_reg_455;
        tmp_numZeros_V_reg_455_pp0_iter3_reg <= tmp_numZeros_V_reg_455_pp0_iter2_reg;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (state_1_load_load_fu_174_p1 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        buckets_V_1_addr_reg_488 <= zext_ln534_fu_274_p1;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter1 == 1'b1) & (ap_phi_mux_i_V_29_flag_0_i_phi_fu_127_p10 == 1'd1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        i_V_1 <= ap_phi_mux_i_V_29_new_0_i_phi_fu_147_p10;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_i_nbreadreq_fu_78_p3 == 1'd1) & (state_1 == 1'd0) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        icmp_ln870_5_reg_463 <= icmp_ln870_5_fu_204_p2;
        icmp_ln870_6_reg_471 <= icmp_ln870_6_fu_218_p2;
        icmp_ln870_7_reg_477 <= icmp_ln870_7_fu_224_p2;
        icmp_ln870_8_reg_483 <= icmp_ln870_8_fu_234_p2;
        tmp_bucketNum_V_reg_450 <= tmp_bucketNum_V_fu_178_p1;
        tmp_numZeros_V_reg_455 <= {{bucketMetaFifo_1_dout[20:16]}};
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (state_1_load_reg_442_pp0_iter3_reg == 1'd0) & (tmp_i_reg_446_pp0_iter3_reg == 1'd1))) begin
        icmp_ln886_reg_508 <= icmp_ln886_fu_430_p2;
        rank_V_reg_503 <= rank_V_fu_302_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (tmp_i_nbreadreq_fu_78_p3 == 1'd1) & (state_1 == 1'd0) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        prev_bucketNum_V_1 <= tmp_bucketNum_V_fu_178_p1;
        prev_prev_bucketNum_V_1 <= prev_bucketNum_V_1;
        prev_prev_prev_bucketNum_V_1 <= prev_prev_bucketNum_V_1;
        prev_prev_prev_prev_bucketNum_V_1 <= prev_prev_prev_bucketNum_V_1;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (state_1_load_reg_442_pp0_iter3_reg == 1'd0) & (ap_enable_reg_pp0_iter4 == 1'b1) & (tmp_i_reg_446_pp0_iter3_reg == 1'd1))) begin
        prev_prev_prev_prev_rank_V_1 <= prev_prev_prev_rank_V_1;
        prev_prev_prev_rank_V_1 <= prev_prev_rank_V_1;
        prev_prev_rank_V_1 <= prev_rank_V_1;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (state_1_load_reg_442_pp0_iter3_reg == 1'd0) & (ap_enable_reg_pp0_iter4 == 1'b1) & (icmp_ln886_fu_430_p2 == 1'd1) & (tmp_i_reg_446_pp0_iter3_reg == 1'd1))) begin
        prev_rank_V_1 <= rank_V_fu_302_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter3 == 1'b1) & (state_1_load_reg_442_pp0_iter2_reg == 1'd1)) | ((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter3 == 1'b1) & (icmp_ln870_8_reg_483_pp0_iter2_reg == 1'd0) & (icmp_ln870_6_reg_471_pp0_iter2_reg == 1'd0) & (icmp_ln870_7_reg_477_pp0_iter2_reg == 1'd0) & (icmp_ln870_5_reg_463_pp0_iter2_reg == 1'd0) & (tmp_i_reg_446_pp0_iter2_reg == 1'd1) & (state_1_load_reg_442_pp0_iter2_reg == 1'd0)))) begin
        reg_169 <= buckets_V_1_q1;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        state_1_load_reg_442 <= state_1;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (state_1 == 1'd0) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        tmp_i_reg_446 <= tmp_i_nbreadreq_fu_78_p3;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter5 == 1'b1))) begin
        ap_done = 1'b1;
    end else begin
        ap_done = ap_done_reg;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (ap_idle_pp0 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_idle = 1'b1;
    end else begin
        ap_idle = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter5 == 1'b0) & (ap_enable_reg_pp0_iter4 == 1'b0) & (ap_enable_reg_pp0_iter3 == 1'b0) & (ap_enable_reg_pp0_iter2 == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b0))) begin
        ap_idle_pp0 = 1'b1;
    end else begin
        ap_idle_pp0 = 1'b0;
    end
end

always @ (*) begin
    if (((ap_enable_reg_pp0_iter4 == 1'b0) & (ap_enable_reg_pp0_iter3 == 1'b0) & (ap_enable_reg_pp0_iter2 == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b0) & (ap_enable_reg_pp0_iter0 == 1'b0))) begin
        ap_idle_pp0_0to4 = 1'b1;
    end else begin
        ap_idle_pp0_0to4 = 1'b0;
    end
end

always @ (*) begin
    if ((((tmp_i_nbreadreq_fu_78_p3 == 1'd1) & (state_1 == 1'd0) & (tmp_last_V_fu_192_p3 == 1'd0)) | ((tmp_i_nbreadreq_fu_78_p3 == 1'd1) & (state_1 == 1'd0) & (tmp_last_V_fu_192_p3 == 1'd1)) | ((tmp_i_nbreadreq_fu_78_p3 == 1'd0) & (state_1 == 1'd0)))) begin
        ap_phi_mux_i_V_29_flag_0_i_phi_fu_127_p10 = 1'd0;
    end else if ((((state_1_load_load_fu_174_p1 == 1'd1) & (icmp_ln870_fu_286_p2 == 1'd0)) | ((state_1_load_load_fu_174_p1 == 1'd1) & (icmp_ln870_fu_286_p2 == 1'd1)))) begin
        ap_phi_mux_i_V_29_flag_0_i_phi_fu_127_p10 = 1'd1;
    end else begin
        ap_phi_mux_i_V_29_flag_0_i_phi_fu_127_p10 = ap_phi_reg_pp0_iter1_i_V_29_flag_0_i_reg_124;
    end
end

always @ (*) begin
    if ((state_1_load_load_fu_174_p1 == 1'd1)) begin
        if ((icmp_ln870_fu_286_p2 == 1'd0)) begin
            ap_phi_mux_i_V_29_new_0_i_phi_fu_147_p10 = add_ln691_fu_279_p2;
        end else if ((icmp_ln870_fu_286_p2 == 1'd1)) begin
            ap_phi_mux_i_V_29_new_0_i_phi_fu_147_p10 = 17'd0;
        end else begin
            ap_phi_mux_i_V_29_new_0_i_phi_fu_147_p10 = ap_phi_reg_pp0_iter1_i_V_29_new_0_i_reg_144;
        end
    end else begin
        ap_phi_mux_i_V_29_new_0_i_phi_fu_147_p10 = ap_phi_reg_pp0_iter1_i_V_29_new_0_i_reg_144;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_start == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_ready = 1'b1;
    end else begin
        ap_ready = 1'b0;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (ap_idle_pp0_0to4 == 1'b1))) begin
        ap_reset_idle_pp0 = 1'b1;
    end else begin
        ap_reset_idle_pp0 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0) & (ap_predicate_op18_read_state2 == 1'b1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        bucketMetaFifo_1_blk_n = bucketMetaFifo_1_empty_n;
    end else begin
        bucketMetaFifo_1_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (ap_predicate_op18_read_state2 == 1'b1) & (ap_enable_reg_pp0_iter1 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        bucketMetaFifo_1_read = 1'b1;
    end else begin
        bucketMetaFifo_1_read = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0) & (state_1_load_reg_442_pp0_iter3_reg == 1'd1) & (ap_enable_reg_pp0_iter4 == 1'b1))) begin
        bucket_fifo_V_V_8_blk_n = bucket_fifo_V_V_8_full_n;
    end else begin
        bucket_fifo_V_V_8_blk_n = 1'b1;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0_11001) & (state_1_load_reg_442_pp0_iter3_reg == 1'd1) & (ap_enable_reg_pp0_iter4 == 1'b1))) begin
        bucket_fifo_V_V_8_write = 1'b1;
    end else begin
        bucket_fifo_V_V_8_write = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0) & (ap_enable_reg_pp0_iter5 == 1'b1))) begin
        if ((state_1_load_reg_442_pp0_iter4_reg == 1'd1)) begin
            buckets_V_1_address0 = buckets_V_1_addr_reg_488_pp0_iter4_reg;
        end else if ((1'b1 == ap_condition_311)) begin
            buckets_V_1_address0 = buckets_V_1_addr_1_reg_497_pp0_iter4_reg;
        end else begin
            buckets_V_1_address0 = 'bx;
        end
    end else begin
        buckets_V_1_address0 = 'bx;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0) & (ap_enable_reg_pp0_iter2 == 1'b1))) begin
        if ((state_1_load_reg_442 == 1'd1)) begin
            buckets_V_1_address1 = buckets_V_1_addr_reg_488;
        end else if ((1'b1 == ap_condition_298)) begin
            buckets_V_1_address1 = zext_ln534_2_fu_298_p1;
        end else begin
            buckets_V_1_address1 = 'bx;
        end
    end else begin
        buckets_V_1_address1 = 'bx;
    end
end

always @ (*) begin
    if ((((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter5 == 1'b1) & (state_1_load_reg_442_pp0_iter4_reg == 1'd1)) | ((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter5 == 1'b1) & (icmp_ln886_reg_508 == 1'd1) & (tmp_i_reg_446_pp0_iter4_reg == 1'd1) & (state_1_load_reg_442_pp0_iter4_reg == 1'd0)))) begin
        buckets_V_1_ce0 = 1'b1;
    end else begin
        buckets_V_1_ce0 = 1'b0;
    end
end

always @ (*) begin
    if ((((1'b0 == ap_block_pp0_stage0_11001) & (state_1_load_reg_442 == 1'd1) & (ap_enable_reg_pp0_iter2 == 1'b1)) | ((1'b0 == ap_block_pp0_stage0_11001) & (state_1_load_reg_442 == 1'd0) & (ap_enable_reg_pp0_iter2 == 1'b1) & (icmp_ln870_8_reg_483 == 1'd0) & (icmp_ln870_6_reg_471 == 1'd0) & (icmp_ln870_7_reg_477 == 1'd0) & (icmp_ln870_5_reg_463 == 1'd0) & (tmp_i_reg_446 == 1'd1)))) begin
        buckets_V_1_ce1 = 1'b1;
    end else begin
        buckets_V_1_ce1 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_block_pp0_stage0) & (ap_enable_reg_pp0_iter5 == 1'b1))) begin
        if ((state_1_load_reg_442_pp0_iter4_reg == 1'd1)) begin
            buckets_V_1_d0 = 5'd0;
        end else if ((1'b1 == ap_condition_311)) begin
            buckets_V_1_d0 = rank_V_reg_503;
        end else begin
            buckets_V_1_d0 = 'bx;
        end
    end else begin
        buckets_V_1_d0 = 'bx;
    end
end

always @ (*) begin
    if ((((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter5 == 1'b1) & (state_1_load_reg_442_pp0_iter4_reg == 1'd1)) | ((1'b0 == ap_block_pp0_stage0_11001) & (ap_enable_reg_pp0_iter5 == 1'b1) & (icmp_ln886_reg_508 == 1'd1) & (tmp_i_reg_446_pp0_iter4_reg == 1'd1) & (state_1_load_reg_442_pp0_iter4_reg == 1'd0)))) begin
        buckets_V_1_we0 = 1'b1;
    end else begin
        buckets_V_1_we0 = 1'b0;
    end
end

always @ (*) begin
    case (ap_CS_fsm)
        ap_ST_fsm_pp0_stage0 : begin
            ap_NS_fsm = ap_ST_fsm_pp0_stage0;
        end
        default : begin
            ap_NS_fsm = 'bx;
        end
    endcase
end

assign add_ln691_fu_279_p2 = (i_V_1 + 17'd1);

assign and_ln870_3_fu_348_p2 = (xor_ln870_3_fu_343_p2 & icmp_ln870_6_reg_471_pp0_iter3_reg);

assign and_ln870_4_fu_359_p2 = (xor_ln870_4_fu_353_p2 & icmp_ln870_7_reg_477_pp0_iter3_reg);

assign and_ln870_fu_338_p2 = (xor_ln870_fu_332_p2 & icmp_ln870_8_reg_483_pp0_iter3_reg);

assign ap_CS_fsm_pp0_stage0 = ap_CS_fsm[32'd0];

always @ (*) begin
    ap_block_pp0 = ((ap_ST_fsm_pp0_stage0 == ap_CS_fsm) & (1'b1 == ap_block_pp0_stage0_subdone));
end

assign ap_block_pp0_stage0 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_block_pp0_stage0_01001 = ((ap_done_reg == 1'b1) | ((state_1_load_reg_442_pp0_iter3_reg == 1'd1) & (bucket_fifo_V_V_8_full_n == 1'b0) & (ap_enable_reg_pp0_iter4 == 1'b1)) | ((ap_predicate_op18_read_state2 == 1'b1) & (bucketMetaFifo_1_empty_n == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b1)) | ((ap_start == 1'b1) & (ap_done_reg == 1'b1)));
end

always @ (*) begin
    ap_block_pp0_stage0_11001 = ((ap_done_reg == 1'b1) | ((state_1_load_reg_442_pp0_iter3_reg == 1'd1) & (bucket_fifo_V_V_8_full_n == 1'b0) & (ap_enable_reg_pp0_iter4 == 1'b1)) | ((ap_predicate_op18_read_state2 == 1'b1) & (bucketMetaFifo_1_empty_n == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b1)) | ((ap_start == 1'b1) & (ap_done_reg == 1'b1)));
end

always @ (*) begin
    ap_block_pp0_stage0_subdone = ((ap_done_reg == 1'b1) | ((state_1_load_reg_442_pp0_iter3_reg == 1'd1) & (bucket_fifo_V_V_8_full_n == 1'b0) & (ap_enable_reg_pp0_iter4 == 1'b1)) | ((ap_predicate_op18_read_state2 == 1'b1) & (bucketMetaFifo_1_empty_n == 1'b0) & (ap_enable_reg_pp0_iter1 == 1'b1)) | ((ap_start == 1'b1) & (ap_done_reg == 1'b1)));
end

always @ (*) begin
    ap_block_state1_pp0_stage0_iter0 = (ap_done_reg == 1'b1);
end

always @ (*) begin
    ap_block_state2_pp0_stage0_iter1 = ((ap_predicate_op18_read_state2 == 1'b1) & (bucketMetaFifo_1_empty_n == 1'b0));
end

assign ap_block_state3_pp0_stage0_iter2 = ~(1'b1 == 1'b1);

assign ap_block_state4_pp0_stage0_iter3 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_block_state5_pp0_stage0_iter4 = ((state_1_load_reg_442_pp0_iter3_reg == 1'd1) & (bucket_fifo_V_V_8_full_n == 1'b0));
end

assign ap_block_state6_pp0_stage0_iter5 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_condition_298 = ((state_1_load_reg_442 == 1'd0) & (icmp_ln870_8_reg_483 == 1'd0) & (icmp_ln870_6_reg_471 == 1'd0) & (icmp_ln870_7_reg_477 == 1'd0) & (icmp_ln870_5_reg_463 == 1'd0) & (tmp_i_reg_446 == 1'd1));
end

always @ (*) begin
    ap_condition_311 = ((icmp_ln886_reg_508 == 1'd1) & (tmp_i_reg_446_pp0_iter4_reg == 1'd1) & (state_1_load_reg_442_pp0_iter4_reg == 1'd0));
end

always @ (*) begin
    ap_enable_operation_52 = (ap_predicate_op52_load_state3 == 1'b1);
end

always @ (*) begin
    ap_enable_operation_53 = (state_1_load_reg_442 == 1'd1);
end

always @ (*) begin
    ap_enable_operation_54 = (ap_predicate_op54_load_state4 == 1'b1);
end

always @ (*) begin
    ap_enable_operation_55 = (state_1_load_reg_442_pp0_iter2_reg == 1'd1);
end

always @ (*) begin
    ap_enable_operation_83 = (ap_predicate_op83_store_state6 == 1'b1);
end

always @ (*) begin
    ap_enable_operation_85 = (state_1_load_reg_442_pp0_iter4_reg == 1'd1);
end

assign ap_enable_pp0 = (ap_idle_pp0 ^ 1'b1);

assign ap_enable_reg_pp0_iter0 = ap_start;

always @ (*) begin
    ap_enable_state3_pp0_iter2_stage0 = ((ap_enable_reg_pp0_iter2 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0));
end

always @ (*) begin
    ap_enable_state4_pp0_iter3_stage0 = ((ap_enable_reg_pp0_iter3 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0));
end

always @ (*) begin
    ap_enable_state6_pp0_iter5_stage0 = ((ap_enable_reg_pp0_iter5 == 1'b1) & (1'b1 == ap_CS_fsm_pp0_stage0));
end

assign ap_phi_reg_pp0_iter1_i_V_29_flag_0_i_reg_124 = 'bx;

assign ap_phi_reg_pp0_iter1_i_V_29_new_0_i_reg_144 = 'bx;

always @ (*) begin
    ap_predicate_op18_read_state2 = ((tmp_i_nbreadreq_fu_78_p3 == 1'd1) & (state_1 == 1'd0));
end

always @ (*) begin
    ap_predicate_op52_load_state3 = ((state_1_load_reg_442 == 1'd0) & (icmp_ln870_8_reg_483 == 1'd0) & (icmp_ln870_6_reg_471 == 1'd0) & (icmp_ln870_7_reg_477 == 1'd0) & (icmp_ln870_5_reg_463 == 1'd0) & (tmp_i_reg_446 == 1'd1));
end

always @ (*) begin
    ap_predicate_op54_load_state4 = ((icmp_ln870_8_reg_483_pp0_iter2_reg == 1'd0) & (icmp_ln870_6_reg_471_pp0_iter2_reg == 1'd0) & (icmp_ln870_7_reg_477_pp0_iter2_reg == 1'd0) & (icmp_ln870_5_reg_463_pp0_iter2_reg == 1'd0) & (tmp_i_reg_446_pp0_iter2_reg == 1'd1) & (state_1_load_reg_442_pp0_iter2_reg == 1'd0));
end

always @ (*) begin
    ap_predicate_op83_store_state6 = ((icmp_ln886_reg_508 == 1'd1) & (tmp_i_reg_446_pp0_iter4_reg == 1'd1) & (state_1_load_reg_442_pp0_iter4_reg == 1'd0));
end

assign bucket_fifo_V_V_8_din = reg_169;

assign current_rank_V_10_fu_404_p3 = ((or_ln870_8_fu_398_p2[0:0] == 1'b1) ? select_ln870_5_fu_390_p3 : reg_169);

assign icmp_ln870_5_fu_204_p2 = ((tmp_bucketNum_V_fu_178_p1 == prev_bucketNum_V_1) ? 1'b1 : 1'b0);

assign icmp_ln870_6_fu_218_p2 = ((tmp_bucketNum_V_fu_178_p1 == prev_prev_bucketNum_V_1) ? 1'b1 : 1'b0);

assign icmp_ln870_7_fu_224_p2 = ((tmp_bucketNum_V_fu_178_p1 == prev_prev_prev_bucketNum_V_1) ? 1'b1 : 1'b0);

assign icmp_ln870_8_fu_234_p2 = ((tmp_bucketNum_V_fu_178_p1 == prev_prev_prev_prev_bucketNum_V_1) ? 1'b1 : 1'b0);

assign icmp_ln870_fu_286_p2 = ((add_ln691_fu_279_p2 == 17'd65536) ? 1'b1 : 1'b0);

assign icmp_ln886_fu_430_p2 = ((rank_V_fu_302_p2 > current_rank_V_10_fu_404_p3) ? 1'b1 : 1'b0);

assign or_ln870_5_fu_327_p2 = (or_ln870_fu_323_p2 | icmp_ln870_7_reg_477_pp0_iter3_reg);

assign or_ln870_6_fu_372_p2 = (and_ln870_4_fu_359_p2 | and_ln870_3_fu_348_p2);

assign or_ln870_7_fu_385_p2 = (icmp_ln870_5_reg_463_pp0_iter3_reg | and_ln870_fu_338_p2);

assign or_ln870_8_fu_398_p2 = (or_ln870_7_fu_385_p2 | or_ln870_6_fu_372_p2);

assign or_ln870_fu_323_p2 = (icmp_ln870_6_reg_471_pp0_iter3_reg | icmp_ln870_5_reg_463_pp0_iter3_reg);

assign rank_V_fu_302_p2 = (tmp_numZeros_V_reg_455_pp0_iter3_reg + 5'd1);

assign select_ln870_4_fu_378_p3 = ((icmp_ln870_5_reg_463_pp0_iter3_reg[0:0] == 1'b1) ? prev_rank_V_1 : prev_prev_prev_prev_rank_V_1);

assign select_ln870_5_fu_390_p3 = ((or_ln870_6_fu_372_p2[0:0] == 1'b1) ? select_ln870_fu_364_p3 : select_ln870_4_fu_378_p3);

assign select_ln870_fu_364_p3 = ((and_ln870_4_fu_359_p2[0:0] == 1'b1) ? prev_prev_prev_rank_V_1 : prev_prev_rank_V_1);

assign state_1_load_load_fu_174_p1 = state_1;

assign tmp_bucketNum_V_fu_178_p1 = bucketMetaFifo_1_dout[15:0];

assign tmp_i_nbreadreq_fu_78_p3 = bucketMetaFifo_1_empty_n;

assign tmp_last_V_fu_192_p3 = bucketMetaFifo_1_dout[32'd32];

assign xor_ln870_3_fu_343_p2 = (icmp_ln870_5_reg_463_pp0_iter3_reg ^ 1'd1);

assign xor_ln870_4_fu_353_p2 = (or_ln870_fu_323_p2 ^ 1'd1);

assign xor_ln870_fu_332_p2 = (or_ln870_5_fu_327_p2 ^ 1'd1);

assign zext_ln534_2_fu_298_p1 = tmp_bucketNum_V_reg_450;

assign zext_ln534_fu_274_p1 = i_V_1;

endmodule //hyperloglog_top_fill_bucket_1_8_s
