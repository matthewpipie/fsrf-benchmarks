// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xhyperloglog_top.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XHyperloglog_top_CfgInitialize(XHyperloglog_top *InstancePtr, XHyperloglog_top_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Control_BaseAddress = ConfigPtr->Control_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XHyperloglog_top_Start(XHyperloglog_top *InstancePtr) {
    u64 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHyperloglog_top_ReadReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_AP_CTRL) & 0x80;
    XHyperloglog_top_WriteReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_AP_CTRL, Data | 0x01);
}

u64 XHyperloglog_top_IsDone(XHyperloglog_top *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHyperloglog_top_ReadReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u64 XHyperloglog_top_IsIdle(XHyperloglog_top *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHyperloglog_top_ReadReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u64 XHyperloglog_top_IsReady(XHyperloglog_top *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHyperloglog_top_ReadReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XHyperloglog_top_Continue(XHyperloglog_top *InstancePtr) {
    u64 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHyperloglog_top_ReadReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_AP_CTRL) & 0x80;
    XHyperloglog_top_WriteReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_AP_CTRL, Data | 0x10);
}

void XHyperloglog_top_EnableAutoRestart(XHyperloglog_top *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHyperloglog_top_WriteReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_AP_CTRL, 0x80);
}

void XHyperloglog_top_DisableAutoRestart(XHyperloglog_top *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHyperloglog_top_WriteReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_AP_CTRL, 0);
}

void XHyperloglog_top_Set_input_s(XHyperloglog_top *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHyperloglog_top_WriteReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_INPUT_S_DATA, Data);
}

u64 XHyperloglog_top_Get_input_s(XHyperloglog_top *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHyperloglog_top_ReadReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_INPUT_S_DATA);
    return Data;
}

void XHyperloglog_top_Set_N_s(XHyperloglog_top *InstancePtr, u64 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHyperloglog_top_WriteReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_N_S_DATA, Data);
}

u64 XHyperloglog_top_Get_N_s(XHyperloglog_top *InstancePtr) {
    u64 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XHyperloglog_top_ReadReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_N_S_DATA);
    return Data;
}

void XHyperloglog_top_InterruptGlobalEnable(XHyperloglog_top *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHyperloglog_top_WriteReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_GIE, 1);
}

void XHyperloglog_top_InterruptGlobalDisable(XHyperloglog_top *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHyperloglog_top_WriteReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_GIE, 0);
}

void XHyperloglog_top_InterruptEnable(XHyperloglog_top *InstancePtr, u64 Mask) {
    u64 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XHyperloglog_top_ReadReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_IER);
    XHyperloglog_top_WriteReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_IER, Register | Mask);
}

void XHyperloglog_top_InterruptDisable(XHyperloglog_top *InstancePtr, u64 Mask) {
    u64 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XHyperloglog_top_ReadReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_IER);
    XHyperloglog_top_WriteReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_IER, Register & (~Mask));
}

void XHyperloglog_top_InterruptClear(XHyperloglog_top *InstancePtr, u64 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XHyperloglog_top_WriteReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_ISR, Mask);
}

u64 XHyperloglog_top_InterruptGetEnabled(XHyperloglog_top *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XHyperloglog_top_ReadReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_IER);
}

u64 XHyperloglog_top_InterruptGetStatus(XHyperloglog_top *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XHyperloglog_top_ReadReg(InstancePtr->Control_BaseAddress, XHYPERLOGLOG_TOP_CONTROL_ADDR_ISR);
}

