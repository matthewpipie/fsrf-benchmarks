// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XHYPERLOGLOG_TOP_H
#define XHYPERLOGLOG_TOP_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xhyperloglog_top_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
#else
typedef struct {
    u16 DeviceId;
    u32 Control_BaseAddress;
} XHyperloglog_top_Config;
#endif

typedef struct {
    u64 Control_BaseAddress;
    u64 IsReady;
} XHyperloglog_top;

typedef u64 word_type;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XHyperloglog_top_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out64((BaseAddress) + (RegOffset), (u64)(Data))
#define XHyperloglog_top_ReadReg(BaseAddress, RegOffset) \
    Xil_In64((BaseAddress) + (RegOffset))
#else
#define XHyperloglog_top_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u64*)((BaseAddress) + (RegOffset)) = (u64)(Data)
#define XHyperloglog_top_ReadReg(BaseAddress, RegOffset) \
    *(volatile u64*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XHyperloglog_top_Initialize(XHyperloglog_top *InstancePtr, u16 DeviceId);
XHyperloglog_top_Config* XHyperloglog_top_LookupConfig(u16 DeviceId);
int XHyperloglog_top_CfgInitialize(XHyperloglog_top *InstancePtr, XHyperloglog_top_Config *ConfigPtr);
#else
int XHyperloglog_top_Initialize(XHyperloglog_top *InstancePtr, const char* InstanceName);
int XHyperloglog_top_Release(XHyperloglog_top *InstancePtr);
#endif

void XHyperloglog_top_Start(XHyperloglog_top *InstancePtr);
u64 XHyperloglog_top_IsDone(XHyperloglog_top *InstancePtr);
u64 XHyperloglog_top_IsIdle(XHyperloglog_top *InstancePtr);
u64 XHyperloglog_top_IsReady(XHyperloglog_top *InstancePtr);
void XHyperloglog_top_Continue(XHyperloglog_top *InstancePtr);
void XHyperloglog_top_EnableAutoRestart(XHyperloglog_top *InstancePtr);
void XHyperloglog_top_DisableAutoRestart(XHyperloglog_top *InstancePtr);

void XHyperloglog_top_Set_input_s(XHyperloglog_top *InstancePtr, u64 Data);
u64 XHyperloglog_top_Get_input_s(XHyperloglog_top *InstancePtr);
void XHyperloglog_top_Set_N_s(XHyperloglog_top *InstancePtr, u64 Data);
u64 XHyperloglog_top_Get_N_s(XHyperloglog_top *InstancePtr);

void XHyperloglog_top_InterruptGlobalEnable(XHyperloglog_top *InstancePtr);
void XHyperloglog_top_InterruptGlobalDisable(XHyperloglog_top *InstancePtr);
void XHyperloglog_top_InterruptEnable(XHyperloglog_top *InstancePtr, u64 Mask);
void XHyperloglog_top_InterruptDisable(XHyperloglog_top *InstancePtr, u64 Mask);
void XHyperloglog_top_InterruptClear(XHyperloglog_top *InstancePtr, u64 Mask);
u64 XHyperloglog_top_InterruptGetEnabled(XHyperloglog_top *InstancePtr);
u64 XHyperloglog_top_InterruptGetStatus(XHyperloglog_top *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
