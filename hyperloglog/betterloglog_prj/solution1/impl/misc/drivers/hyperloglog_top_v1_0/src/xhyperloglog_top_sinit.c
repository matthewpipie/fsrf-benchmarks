// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xhyperloglog_top.h"

extern XHyperloglog_top_Config XHyperloglog_top_ConfigTable[];

XHyperloglog_top_Config *XHyperloglog_top_LookupConfig(u16 DeviceId) {
	XHyperloglog_top_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XHYPERLOGLOG_TOP_NUM_INSTANCES; Index++) {
		if (XHyperloglog_top_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XHyperloglog_top_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XHyperloglog_top_Initialize(XHyperloglog_top *InstancePtr, u16 DeviceId) {
	XHyperloglog_top_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XHyperloglog_top_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XHyperloglog_top_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

