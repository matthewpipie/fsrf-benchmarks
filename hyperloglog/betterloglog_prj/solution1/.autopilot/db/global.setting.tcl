
set TopModule "hyperloglog_top"
set ClockPeriod 2.5
set ClockList ap_clk
set HasVivadoClockPeriod 0
set CombLogicFlag 0
set PipelineFlag 1
set DataflowTaskPipelineFlag 1
set TrivialPipelineFlag 0
set noPortSwitchingFlag 0
set FloatingPointFlag 1
set FftOrFirFlag 0
set NbRWValue 0
set intNbAccess 0
set NewDSPMapping 1
set HasDSPModule 0
set ResetLevelFlag 0
set ResetStyle control
set ResetSyncFlag 1
set ResetRegisterFlag 1
set ResetVariableFlag 0
set ResetRegisterNum 3
set FsmEncStyle onehot
set MaxFanout 0
set RtlPrefix {}
set RtlSubPrefix hyperloglog_top_
set ExtraCCFlags {}
set ExtraCLdFlags {}
set SynCheckOptions {}
set PresynOptions {}
set PreprocOptions {}
set SchedOptions {}
set BindOptions {}
set RtlGenOptions {}
set RtlWriterOptions {}
set CbcGenFlag {}
set CasGenFlag {}
set CasMonitorFlag {}
set AutoSimOptions {}
set ExportMCPathFlag 0
set SCTraceFileName mytrace
set SCTraceFileFormat vcd
set SCTraceOption all
set TargetInfo xcvu9p:-flgb2104:-2-i
set SourceFiles {sc {} c {../../betterloglog/src/zero_counter.cpp ../../betterloglog/src/write_results_memory.cpp ../../betterloglog/src/murmur3.cpp ../../betterloglog/src/hyperloglog_top.cpp ../../betterloglog/src/hyperloglog.cpp ../../betterloglog/src/divide_data.cpp ../../betterloglog/src/axi_utils.cpp ../../betterloglog/src/aggr_bucket.cpp ../../betterloglog/src/accumulate.cpp}}
set SourceFlags {sc {} c {{} {} {} {} {} {} {} {} {}}}
set DirectiveFile /home/centos/src/project_data/betterloglog_prj/solution1/solution1.directive
set TBFiles {verilog {../../betterloglog/src/hll_test_bench_new.cpp ../../betterloglog/src/hll_test_bench.cpp} bc {../../betterloglog/src/hll_test_bench_new.cpp ../../betterloglog/src/hll_test_bench.cpp} sc {../../betterloglog/src/hll_test_bench_new.cpp ../../betterloglog/src/hll_test_bench.cpp} vhdl {../../betterloglog/src/hll_test_bench_new.cpp ../../betterloglog/src/hll_test_bench.cpp} c {} cas {../../betterloglog/src/hll_test_bench_new.cpp ../../betterloglog/src/hll_test_bench.cpp}}
set SpecLanguage C
set TVInFiles {bc {} c {} sc {} cas {} vhdl {} verilog {}}
set TVOutFiles {bc {} c {} sc {} cas {} vhdl {} verilog {}}
set TBTops {verilog {} bc {} sc {} vhdl {} c {} cas {}}
set TBInstNames {verilog {} bc {} sc {} vhdl {} c {} cas {}}
set XDCFiles {}
set ExtraGlobalOptions {"area_timing" 1 "clock_gate" 1 "impl_flow" map "power_gate" 0}
set TBTVFileNotFound {}
set AppFile ../hls.app
set ApsFile solution1.aps
set AvePath ../..
set DefaultPlatform DefaultPlatform
set multiClockList {}
set SCPortClockMap {}
set intNbAccess 0
set PlatformFiles {{DefaultPlatform {xilinx/virtexuplus/virtexuplus xilinx/virtexuplus/virtexuplus_fpv7}}}
set HPFPO 0
