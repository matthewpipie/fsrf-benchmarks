# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 7 \
    name s_axis_input_tuple \
    type fifo \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_s_axis_input_tuple \
    op interface \
    ports { s_axis_input_tuple_dout { I 1024 vector } s_axis_input_tuple_empty_n { I 1 bit } s_axis_input_tuple_read { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 8 \
    name dataFifo_V_data_V_0 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_0 \
    op interface \
    ports { dataFifo_V_data_V_0_din { O 32 vector } dataFifo_V_data_V_0_full_n { I 1 bit } dataFifo_V_data_V_0_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 9 \
    name dataFifo_V_valid_V_0 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_0 \
    op interface \
    ports { dataFifo_V_valid_V_0_din { O 1 vector } dataFifo_V_valid_V_0_full_n { I 1 bit } dataFifo_V_valid_V_0_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 10 \
    name dataFifo_V_last_V_0 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_0 \
    op interface \
    ports { dataFifo_V_last_V_0_din { O 1 vector } dataFifo_V_last_V_0_full_n { I 1 bit } dataFifo_V_last_V_0_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 11 \
    name dataFifo_V_data_V_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_1 \
    op interface \
    ports { dataFifo_V_data_V_1_din { O 32 vector } dataFifo_V_data_V_1_full_n { I 1 bit } dataFifo_V_data_V_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 12 \
    name dataFifo_V_valid_V_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_1 \
    op interface \
    ports { dataFifo_V_valid_V_1_din { O 1 vector } dataFifo_V_valid_V_1_full_n { I 1 bit } dataFifo_V_valid_V_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 13 \
    name dataFifo_V_last_V_1 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_1 \
    op interface \
    ports { dataFifo_V_last_V_1_din { O 1 vector } dataFifo_V_last_V_1_full_n { I 1 bit } dataFifo_V_last_V_1_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 14 \
    name dataFifo_V_data_V_2 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_2 \
    op interface \
    ports { dataFifo_V_data_V_2_din { O 32 vector } dataFifo_V_data_V_2_full_n { I 1 bit } dataFifo_V_data_V_2_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 15 \
    name dataFifo_V_valid_V_2 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_2 \
    op interface \
    ports { dataFifo_V_valid_V_2_din { O 1 vector } dataFifo_V_valid_V_2_full_n { I 1 bit } dataFifo_V_valid_V_2_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 16 \
    name dataFifo_V_last_V_2 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_2 \
    op interface \
    ports { dataFifo_V_last_V_2_din { O 1 vector } dataFifo_V_last_V_2_full_n { I 1 bit } dataFifo_V_last_V_2_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 17 \
    name dataFifo_V_data_V_3 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_3 \
    op interface \
    ports { dataFifo_V_data_V_3_din { O 32 vector } dataFifo_V_data_V_3_full_n { I 1 bit } dataFifo_V_data_V_3_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 18 \
    name dataFifo_V_valid_V_3 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_3 \
    op interface \
    ports { dataFifo_V_valid_V_3_din { O 1 vector } dataFifo_V_valid_V_3_full_n { I 1 bit } dataFifo_V_valid_V_3_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 19 \
    name dataFifo_V_last_V_3 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_3 \
    op interface \
    ports { dataFifo_V_last_V_3_din { O 1 vector } dataFifo_V_last_V_3_full_n { I 1 bit } dataFifo_V_last_V_3_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 20 \
    name dataFifo_V_data_V_4 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_4 \
    op interface \
    ports { dataFifo_V_data_V_4_din { O 32 vector } dataFifo_V_data_V_4_full_n { I 1 bit } dataFifo_V_data_V_4_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 21 \
    name dataFifo_V_valid_V_4 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_4 \
    op interface \
    ports { dataFifo_V_valid_V_4_din { O 1 vector } dataFifo_V_valid_V_4_full_n { I 1 bit } dataFifo_V_valid_V_4_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 22 \
    name dataFifo_V_last_V_4 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_4 \
    op interface \
    ports { dataFifo_V_last_V_4_din { O 1 vector } dataFifo_V_last_V_4_full_n { I 1 bit } dataFifo_V_last_V_4_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 23 \
    name dataFifo_V_data_V_5 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_5 \
    op interface \
    ports { dataFifo_V_data_V_5_din { O 32 vector } dataFifo_V_data_V_5_full_n { I 1 bit } dataFifo_V_data_V_5_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 24 \
    name dataFifo_V_valid_V_5 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_5 \
    op interface \
    ports { dataFifo_V_valid_V_5_din { O 1 vector } dataFifo_V_valid_V_5_full_n { I 1 bit } dataFifo_V_valid_V_5_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 25 \
    name dataFifo_V_last_V_5 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_5 \
    op interface \
    ports { dataFifo_V_last_V_5_din { O 1 vector } dataFifo_V_last_V_5_full_n { I 1 bit } dataFifo_V_last_V_5_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 26 \
    name dataFifo_V_data_V_6 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_6 \
    op interface \
    ports { dataFifo_V_data_V_6_din { O 32 vector } dataFifo_V_data_V_6_full_n { I 1 bit } dataFifo_V_data_V_6_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 27 \
    name dataFifo_V_valid_V_6 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_6 \
    op interface \
    ports { dataFifo_V_valid_V_6_din { O 1 vector } dataFifo_V_valid_V_6_full_n { I 1 bit } dataFifo_V_valid_V_6_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 28 \
    name dataFifo_V_last_V_6 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_6 \
    op interface \
    ports { dataFifo_V_last_V_6_din { O 1 vector } dataFifo_V_last_V_6_full_n { I 1 bit } dataFifo_V_last_V_6_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 29 \
    name dataFifo_V_data_V_7 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_7 \
    op interface \
    ports { dataFifo_V_data_V_7_din { O 32 vector } dataFifo_V_data_V_7_full_n { I 1 bit } dataFifo_V_data_V_7_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 30 \
    name dataFifo_V_valid_V_7 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_7 \
    op interface \
    ports { dataFifo_V_valid_V_7_din { O 1 vector } dataFifo_V_valid_V_7_full_n { I 1 bit } dataFifo_V_valid_V_7_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 31 \
    name dataFifo_V_last_V_7 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_7 \
    op interface \
    ports { dataFifo_V_last_V_7_din { O 1 vector } dataFifo_V_last_V_7_full_n { I 1 bit } dataFifo_V_last_V_7_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 32 \
    name dataFifo_V_data_V_8 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_8 \
    op interface \
    ports { dataFifo_V_data_V_8_din { O 32 vector } dataFifo_V_data_V_8_full_n { I 1 bit } dataFifo_V_data_V_8_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 33 \
    name dataFifo_V_valid_V_8 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_8 \
    op interface \
    ports { dataFifo_V_valid_V_8_din { O 1 vector } dataFifo_V_valid_V_8_full_n { I 1 bit } dataFifo_V_valid_V_8_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 34 \
    name dataFifo_V_last_V_8 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_8 \
    op interface \
    ports { dataFifo_V_last_V_8_din { O 1 vector } dataFifo_V_last_V_8_full_n { I 1 bit } dataFifo_V_last_V_8_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 35 \
    name dataFifo_V_data_V_9 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_9 \
    op interface \
    ports { dataFifo_V_data_V_9_din { O 32 vector } dataFifo_V_data_V_9_full_n { I 1 bit } dataFifo_V_data_V_9_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 36 \
    name dataFifo_V_valid_V_9 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_9 \
    op interface \
    ports { dataFifo_V_valid_V_9_din { O 1 vector } dataFifo_V_valid_V_9_full_n { I 1 bit } dataFifo_V_valid_V_9_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 37 \
    name dataFifo_V_last_V_9 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_9 \
    op interface \
    ports { dataFifo_V_last_V_9_din { O 1 vector } dataFifo_V_last_V_9_full_n { I 1 bit } dataFifo_V_last_V_9_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 38 \
    name dataFifo_V_data_V_10 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_10 \
    op interface \
    ports { dataFifo_V_data_V_10_din { O 32 vector } dataFifo_V_data_V_10_full_n { I 1 bit } dataFifo_V_data_V_10_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 39 \
    name dataFifo_V_valid_V_10 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_10 \
    op interface \
    ports { dataFifo_V_valid_V_10_din { O 1 vector } dataFifo_V_valid_V_10_full_n { I 1 bit } dataFifo_V_valid_V_10_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 40 \
    name dataFifo_V_last_V_10 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_10 \
    op interface \
    ports { dataFifo_V_last_V_10_din { O 1 vector } dataFifo_V_last_V_10_full_n { I 1 bit } dataFifo_V_last_V_10_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 41 \
    name dataFifo_V_data_V_11 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_11 \
    op interface \
    ports { dataFifo_V_data_V_11_din { O 32 vector } dataFifo_V_data_V_11_full_n { I 1 bit } dataFifo_V_data_V_11_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 42 \
    name dataFifo_V_valid_V_11 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_11 \
    op interface \
    ports { dataFifo_V_valid_V_11_din { O 1 vector } dataFifo_V_valid_V_11_full_n { I 1 bit } dataFifo_V_valid_V_11_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 43 \
    name dataFifo_V_last_V_11 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_11 \
    op interface \
    ports { dataFifo_V_last_V_11_din { O 1 vector } dataFifo_V_last_V_11_full_n { I 1 bit } dataFifo_V_last_V_11_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 44 \
    name dataFifo_V_data_V_12 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_12 \
    op interface \
    ports { dataFifo_V_data_V_12_din { O 32 vector } dataFifo_V_data_V_12_full_n { I 1 bit } dataFifo_V_data_V_12_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 45 \
    name dataFifo_V_valid_V_12 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_12 \
    op interface \
    ports { dataFifo_V_valid_V_12_din { O 1 vector } dataFifo_V_valid_V_12_full_n { I 1 bit } dataFifo_V_valid_V_12_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 46 \
    name dataFifo_V_last_V_12 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_12 \
    op interface \
    ports { dataFifo_V_last_V_12_din { O 1 vector } dataFifo_V_last_V_12_full_n { I 1 bit } dataFifo_V_last_V_12_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 47 \
    name dataFifo_V_data_V_13 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_13 \
    op interface \
    ports { dataFifo_V_data_V_13_din { O 32 vector } dataFifo_V_data_V_13_full_n { I 1 bit } dataFifo_V_data_V_13_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 48 \
    name dataFifo_V_valid_V_13 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_13 \
    op interface \
    ports { dataFifo_V_valid_V_13_din { O 1 vector } dataFifo_V_valid_V_13_full_n { I 1 bit } dataFifo_V_valid_V_13_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 49 \
    name dataFifo_V_last_V_13 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_13 \
    op interface \
    ports { dataFifo_V_last_V_13_din { O 1 vector } dataFifo_V_last_V_13_full_n { I 1 bit } dataFifo_V_last_V_13_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 50 \
    name dataFifo_V_data_V_14 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_14 \
    op interface \
    ports { dataFifo_V_data_V_14_din { O 32 vector } dataFifo_V_data_V_14_full_n { I 1 bit } dataFifo_V_data_V_14_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 51 \
    name dataFifo_V_valid_V_14 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_14 \
    op interface \
    ports { dataFifo_V_valid_V_14_din { O 1 vector } dataFifo_V_valid_V_14_full_n { I 1 bit } dataFifo_V_valid_V_14_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 52 \
    name dataFifo_V_last_V_14 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_14 \
    op interface \
    ports { dataFifo_V_last_V_14_din { O 1 vector } dataFifo_V_last_V_14_full_n { I 1 bit } dataFifo_V_last_V_14_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 53 \
    name dataFifo_V_data_V_15 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_data_V_15 \
    op interface \
    ports { dataFifo_V_data_V_15_din { O 32 vector } dataFifo_V_data_V_15_full_n { I 1 bit } dataFifo_V_data_V_15_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 54 \
    name dataFifo_V_valid_V_15 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_valid_V_15 \
    op interface \
    ports { dataFifo_V_valid_V_15_din { O 1 vector } dataFifo_V_valid_V_15_full_n { I 1 bit } dataFifo_V_valid_V_15_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 55 \
    name dataFifo_V_last_V_15 \
    type fifo \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_dataFifo_V_last_V_15 \
    op interface \
    ports { dataFifo_V_last_V_15_din { O 1 vector } dataFifo_V_last_V_15_full_n { I 1 bit } dataFifo_V_last_V_15_write { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


