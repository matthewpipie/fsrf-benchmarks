# This script segment is generated automatically by AutoPilot

set axilite_register_dict [dict create]
set port_control {
input_s { 
	dir I
	width 64
	depth 1
	mode ap_none
	offset 32
	offset_end 47
}
N_s { 
	dir I
	width 64
	depth 1
	mode ap_none
	offset 48
	offset_end 63
}
ap_start { }
ap_done { }
ap_ready { }
ap_continue { }
ap_idle { }
}
dict set axilite_register_dict control $port_control


