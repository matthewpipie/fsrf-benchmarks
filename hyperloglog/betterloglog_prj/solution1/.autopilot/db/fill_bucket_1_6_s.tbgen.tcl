set moduleName fill_bucket_1_6_s
set isTopModule 0
set isCombinational 0
set isDatapathOnly 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {fill_bucket<1, 6>}
set C_modelType { void 0 }
set C_modelArgList {
	{ bucketMetaFifo_10 int 48 regular {fifo 0 volatile } {global 0}  }
	{ bucket_fifo_V_V_6 int 5 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "bucketMetaFifo_10", "interface" : "fifo", "bitwidth" : 48, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "bucket_fifo_V_V_6", "interface" : "fifo", "bitwidth" : 5, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 13
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ bucketMetaFifo_10_dout sc_in sc_lv 48 signal 0 } 
	{ bucketMetaFifo_10_empty_n sc_in sc_logic 1 signal 0 } 
	{ bucketMetaFifo_10_read sc_out sc_logic 1 signal 0 } 
	{ bucket_fifo_V_V_6_din sc_out sc_lv 5 signal 1 } 
	{ bucket_fifo_V_V_6_full_n sc_in sc_logic 1 signal 1 } 
	{ bucket_fifo_V_V_6_write sc_out sc_logic 1 signal 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "bucketMetaFifo_10_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":48, "type": "signal", "bundle":{"name": "bucketMetaFifo_10", "role": "dout" }} , 
 	{ "name": "bucketMetaFifo_10_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "bucketMetaFifo_10", "role": "empty_n" }} , 
 	{ "name": "bucketMetaFifo_10_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "bucketMetaFifo_10", "role": "read" }} , 
 	{ "name": "bucket_fifo_V_V_6_din", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "bucket_fifo_V_V_6", "role": "din" }} , 
 	{ "name": "bucket_fifo_V_V_6_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "bucket_fifo_V_V_6", "role": "full_n" }} , 
 	{ "name": "bucket_fifo_V_V_6_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "bucket_fifo_V_V_6", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1"],
		"CDFG" : "fill_bucket_1_6_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "5", "EstimateLatencyMin" : "5", "EstimateLatencyMax" : "5",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"DependenceCheck" : [
			{"FromInitialState" : "ap_enable_state3_pp0_iter2_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter2", "FromInitialOperation" : "ap_enable_operation_52", "FromInitialSV" : "2", "FromFinalState" : "ap_enable_state4_pp0_iter3_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter3", "FromFinalOperation" : "ap_enable_operation_54", "FromFinalSV" : "3", "FromAddress" : "buckets_V_10_address1", "FromType" : "R", "ToInitialState" : "ap_enable_state6_pp0_iter5_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter5", "ToInitialNextIteration" : "null", "ToInitialOperation" : "ap_enable_operation_83", "ToInitialSV" : "5", "ToFinalState" : "ap_enable_state6_pp0_iter5_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter5", "ToFinalOperation" : "ap_enable_operation_83", "ToFinalSV" : "5", "ToAddress" : "buckets_V_10_address0", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "16", "II" : "1", "Pragma" : "(betterloglog/src/fill_bucket.hpp:38:1)", "Type" : "WAR"},
			{"FromInitialState" : "ap_enable_state3_pp0_iter2_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter2", "FromInitialOperation" : "ap_enable_operation_52", "FromInitialSV" : "2", "FromFinalState" : "ap_enable_state4_pp0_iter3_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter3", "FromFinalOperation" : "ap_enable_operation_54", "FromFinalSV" : "3", "FromAddress" : "buckets_V_10_address1", "FromType" : "R", "ToInitialState" : "ap_enable_state6_pp0_iter5_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter5", "ToInitialNextIteration" : "null", "ToInitialOperation" : "ap_enable_operation_85", "ToInitialSV" : "5", "ToFinalState" : "ap_enable_state6_pp0_iter5_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter5", "ToFinalOperation" : "ap_enable_operation_85", "ToFinalSV" : "5", "ToAddress" : "buckets_V_10_address0", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "16", "II" : "1", "Pragma" : "(betterloglog/src/fill_bucket.hpp:38:1)", "Type" : "WAR"},
			{"FromInitialState" : "ap_enable_state3_pp0_iter2_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter2", "FromInitialOperation" : "ap_enable_operation_53", "FromInitialSV" : "2", "FromFinalState" : "ap_enable_state4_pp0_iter3_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter3", "FromFinalOperation" : "ap_enable_operation_55", "FromFinalSV" : "3", "FromAddress" : "buckets_V_10_address1", "FromType" : "R", "ToInitialState" : "ap_enable_state6_pp0_iter5_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter5", "ToInitialNextIteration" : "null", "ToInitialOperation" : "ap_enable_operation_83", "ToInitialSV" : "5", "ToFinalState" : "ap_enable_state6_pp0_iter5_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter5", "ToFinalOperation" : "ap_enable_operation_83", "ToFinalSV" : "5", "ToAddress" : "buckets_V_10_address0", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "16", "II" : "1", "Pragma" : "(betterloglog/src/fill_bucket.hpp:38:1)", "Type" : "WAR"},
			{"FromInitialState" : "ap_enable_state3_pp0_iter2_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter2", "FromInitialOperation" : "ap_enable_operation_53", "FromInitialSV" : "2", "FromFinalState" : "ap_enable_state4_pp0_iter3_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter3", "FromFinalOperation" : "ap_enable_operation_55", "FromFinalSV" : "3", "FromAddress" : "buckets_V_10_address1", "FromType" : "R", "ToInitialState" : "ap_enable_state6_pp0_iter5_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter5", "ToInitialNextIteration" : "null", "ToInitialOperation" : "ap_enable_operation_85", "ToInitialSV" : "5", "ToFinalState" : "ap_enable_state6_pp0_iter5_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter5", "ToFinalOperation" : "ap_enable_operation_85", "ToFinalSV" : "5", "ToAddress" : "buckets_V_10_address0", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "16", "II" : "1", "Pragma" : "(betterloglog/src/fill_bucket.hpp:38:1)", "Type" : "WAR"},
			{"FromInitialState" : "ap_enable_state6_pp0_iter5_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter5", "FromInitialOperation" : "ap_enable_operation_83", "FromInitialSV" : "5", "FromFinalState" : "ap_enable_state6_pp0_iter5_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter5", "FromFinalOperation" : "ap_enable_operation_83", "FromFinalSV" : "5", "FromAddress" : "buckets_V_10_address0", "FromType" : "W", "ToInitialState" : "ap_enable_state3_pp0_iter2_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter2", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter3", "ToInitialOperation" : "ap_enable_operation_52", "ToInitialSV" : "2", "ToFinalState" : "ap_enable_state4_pp0_iter3_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter3", "ToFinalOperation" : "ap_enable_operation_54", "ToFinalSV" : "3", "ToAddress" : "buckets_V_10_address1", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "16", "II" : "1", "Pragma" : "(betterloglog/src/fill_bucket.hpp:38:1)", "Type" : "RAW", "StateEnableSignalListForFifoShift" : ["ap_enable_state3_pp0_iter2_stage0", "ap_enable_state4_pp0_iter3_stage0", "ap_enable_state6_pp0_iter5_stage0"]},
			{"FromInitialState" : "ap_enable_state6_pp0_iter5_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter5", "FromInitialOperation" : "ap_enable_operation_83", "FromInitialSV" : "5", "FromFinalState" : "ap_enable_state6_pp0_iter5_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter5", "FromFinalOperation" : "ap_enable_operation_83", "FromFinalSV" : "5", "FromAddress" : "buckets_V_10_address0", "FromType" : "W", "ToInitialState" : "ap_enable_state3_pp0_iter2_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter2", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter3", "ToInitialOperation" : "ap_enable_operation_53", "ToInitialSV" : "2", "ToFinalState" : "ap_enable_state4_pp0_iter3_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter3", "ToFinalOperation" : "ap_enable_operation_55", "ToFinalSV" : "3", "ToAddress" : "buckets_V_10_address1", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "16", "II" : "1", "Pragma" : "(betterloglog/src/fill_bucket.hpp:38:1)", "Type" : "RAW", "StateEnableSignalListForFifoShift" : ["ap_enable_state3_pp0_iter2_stage0", "ap_enable_state4_pp0_iter3_stage0", "ap_enable_state6_pp0_iter5_stage0"]},
			{"FromInitialState" : "ap_enable_state6_pp0_iter5_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter5", "FromInitialOperation" : "ap_enable_operation_83", "FromInitialSV" : "5", "FromFinalState" : "ap_enable_state6_pp0_iter5_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter5", "FromFinalOperation" : "ap_enable_operation_83", "FromFinalSV" : "5", "FromAddress" : "buckets_V_10_address0", "FromType" : "W", "ToInitialState" : "ap_enable_state6_pp0_iter5_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter5", "ToInitialNextIteration" : "null", "ToInitialOperation" : "ap_enable_operation_85", "ToInitialSV" : "5", "ToFinalState" : "ap_enable_state6_pp0_iter5_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter5", "ToFinalOperation" : "ap_enable_operation_85", "ToFinalSV" : "5", "ToAddress" : "buckets_V_10_address0", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "16", "II" : "1", "Pragma" : "(betterloglog/src/fill_bucket.hpp:38:1)", "Type" : "WAW"},
			{"FromInitialState" : "ap_enable_state6_pp0_iter5_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter5", "FromInitialOperation" : "ap_enable_operation_85", "FromInitialSV" : "5", "FromFinalState" : "ap_enable_state6_pp0_iter5_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter5", "FromFinalOperation" : "ap_enable_operation_85", "FromFinalSV" : "5", "FromAddress" : "buckets_V_10_address0", "FromType" : "W", "ToInitialState" : "ap_enable_state3_pp0_iter2_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter2", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter3", "ToInitialOperation" : "ap_enable_operation_52", "ToInitialSV" : "2", "ToFinalState" : "ap_enable_state4_pp0_iter3_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter3", "ToFinalOperation" : "ap_enable_operation_54", "ToFinalSV" : "3", "ToAddress" : "buckets_V_10_address1", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "16", "II" : "1", "Pragma" : "(betterloglog/src/fill_bucket.hpp:38:1)", "Type" : "RAW", "StateEnableSignalListForFifoShift" : ["ap_enable_state3_pp0_iter2_stage0", "ap_enable_state4_pp0_iter3_stage0", "ap_enable_state6_pp0_iter5_stage0"]},
			{"FromInitialState" : "ap_enable_state6_pp0_iter5_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter5", "FromInitialOperation" : "ap_enable_operation_85", "FromInitialSV" : "5", "FromFinalState" : "ap_enable_state6_pp0_iter5_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter5", "FromFinalOperation" : "ap_enable_operation_85", "FromFinalSV" : "5", "FromAddress" : "buckets_V_10_address0", "FromType" : "W", "ToInitialState" : "ap_enable_state3_pp0_iter2_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter2", "ToInitialNextIteration" : "ap_enable_reg_pp0_iter3", "ToInitialOperation" : "ap_enable_operation_53", "ToInitialSV" : "2", "ToFinalState" : "ap_enable_state4_pp0_iter3_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter3", "ToFinalOperation" : "ap_enable_operation_55", "ToFinalSV" : "3", "ToAddress" : "buckets_V_10_address1", "ToType" : "R", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "16", "II" : "1", "Pragma" : "(betterloglog/src/fill_bucket.hpp:38:1)", "Type" : "RAW", "StateEnableSignalListForFifoShift" : ["ap_enable_state3_pp0_iter2_stage0", "ap_enable_state4_pp0_iter3_stage0", "ap_enable_state6_pp0_iter5_stage0"]},
			{"FromInitialState" : "ap_enable_state6_pp0_iter5_stage0", "FromInitialIteration" : "ap_enable_reg_pp0_iter5", "FromInitialOperation" : "ap_enable_operation_85", "FromInitialSV" : "5", "FromFinalState" : "ap_enable_state6_pp0_iter5_stage0", "FromFinalIteration" : "ap_enable_reg_pp0_iter5", "FromFinalOperation" : "ap_enable_operation_85", "FromFinalSV" : "5", "FromAddress" : "buckets_V_10_address0", "FromType" : "W", "ToInitialState" : "ap_enable_state6_pp0_iter5_stage0", "ToInitialIteration" : "ap_enable_reg_pp0_iter5", "ToInitialNextIteration" : "null", "ToInitialOperation" : "ap_enable_operation_83", "ToInitialSV" : "5", "ToFinalState" : "ap_enable_state6_pp0_iter5_stage0", "ToFinalIteration" : "ap_enable_reg_pp0_iter5", "ToFinalOperation" : "ap_enable_operation_83", "ToFinalSV" : "5", "ToAddress" : "buckets_V_10_address0", "ToType" : "W", "PipelineBlock" : "ap_block_pp0", "AddressWidth" : "16", "II" : "1", "Pragma" : "(betterloglog/src/fill_bucket.hpp:38:1)", "Type" : "WAW"}],
		"Port" : [
			{"Name" : "state_10", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "bucketMetaFifo_10", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0", "DependentChanDepth" : "256", "DependentChanType" : "0",
				"BlockSignal" : [
					{"Name" : "bucketMetaFifo_10_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "prev_bucketNum_V_10", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prev_rank_V_10", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prev_prev_bucketNum_V_10", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prev_prev_rank_V_10", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prev_prev_prev_bucketNum_V_10", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prev_prev_prev_rank_V_10", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prev_prev_prev_prev_bucketNum_V_10", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "prev_prev_prev_prev_rank_V_10", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "buckets_V_10", "Type" : "Memory", "Direction" : "IO"},
			{"Name" : "i_V_10", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "bucket_fifo_V_V_6", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0", "DependentChanDepth" : "256", "DependentChanType" : "0",
				"BlockSignal" : [
					{"Name" : "bucket_fifo_V_V_6_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.buckets_V_10_U", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	fill_bucket_1_6_s {
		state_10 {Type IO LastRead -1 FirstWrite -1}
		bucketMetaFifo_10 {Type I LastRead 1 FirstWrite -1}
		prev_bucketNum_V_10 {Type IO LastRead -1 FirstWrite -1}
		prev_rank_V_10 {Type IO LastRead -1 FirstWrite -1}
		prev_prev_bucketNum_V_10 {Type IO LastRead -1 FirstWrite -1}
		prev_prev_rank_V_10 {Type IO LastRead -1 FirstWrite -1}
		prev_prev_prev_bucketNum_V_10 {Type IO LastRead -1 FirstWrite -1}
		prev_prev_prev_rank_V_10 {Type IO LastRead -1 FirstWrite -1}
		prev_prev_prev_prev_bucketNum_V_10 {Type IO LastRead -1 FirstWrite -1}
		prev_prev_prev_prev_rank_V_10 {Type IO LastRead -1 FirstWrite -1}
		buckets_V_10 {Type IO LastRead -1 FirstWrite -1}
		i_V_10 {Type IO LastRead -1 FirstWrite -1}
		bucket_fifo_V_V_6 {Type O LastRead -1 FirstWrite 4}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "5", "Max" : "5"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	bucketMetaFifo_10 { ap_fifo {  { bucketMetaFifo_10_dout fifo_data 0 48 }  { bucketMetaFifo_10_empty_n fifo_status 0 1 }  { bucketMetaFifo_10_read fifo_update 1 1 } } }
	bucket_fifo_V_V_6 { ap_fifo {  { bucket_fifo_V_V_6_din fifo_data 1 5 }  { bucket_fifo_V_V_6_full_n fifo_status 0 1 }  { bucket_fifo_V_V_6_write fifo_update 1 1 } } }
}
