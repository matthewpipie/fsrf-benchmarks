set moduleName accumulate_1_s
set isTopModule 0
set isCombinational 0
set isDatapathOnly 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {accumulate<1>}
set C_modelType { void 0 }
set C_modelArgList {
	{ numzeros_out int 5 regular {fifo 0 volatile } {global 0}  }
	{ accm int 32 regular {fifo 1 volatile } {global 1}  }
	{ done_accm int 1 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "numzeros_out", "interface" : "fifo", "bitwidth" : 5, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "accm", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY", "extern" : 0} , 
 	{ "Name" : "done_accm", "interface" : "fifo", "bitwidth" : 1, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 16
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ numzeros_out_dout sc_in sc_lv 5 signal 0 } 
	{ numzeros_out_empty_n sc_in sc_logic 1 signal 0 } 
	{ numzeros_out_read sc_out sc_logic 1 signal 0 } 
	{ done_accm_din sc_out sc_lv 1 signal 2 } 
	{ done_accm_full_n sc_in sc_logic 1 signal 2 } 
	{ done_accm_write sc_out sc_logic 1 signal 2 } 
	{ accm_din sc_out sc_lv 32 signal 1 } 
	{ accm_full_n sc_in sc_logic 1 signal 1 } 
	{ accm_write sc_out sc_logic 1 signal 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "numzeros_out_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "numzeros_out", "role": "dout" }} , 
 	{ "name": "numzeros_out_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "numzeros_out", "role": "empty_n" }} , 
 	{ "name": "numzeros_out_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "numzeros_out", "role": "read" }} , 
 	{ "name": "done_accm_din", "direction": "out", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "done_accm", "role": "din" }} , 
 	{ "name": "done_accm_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "done_accm", "role": "full_n" }} , 
 	{ "name": "done_accm_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "done_accm", "role": "write" }} , 
 	{ "name": "accm_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "accm", "role": "din" }} , 
 	{ "name": "accm_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "accm", "role": "full_n" }} , 
 	{ "name": "accm_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "accm", "role": "write" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "",
		"CDFG" : "accumulate_1_s",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1", "real_start" : "0",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "6", "EstimateLatencyMin" : "6", "EstimateLatencyMax" : "6",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "numzeros_out", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0", "DependentChanDepth" : "256", "DependentChanType" : "0",
				"BlockSignal" : [
					{"Name" : "numzeros_out_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "summation_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "accm", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0", "DependentChanDepth" : "256", "DependentChanType" : "0",
				"BlockSignal" : [
					{"Name" : "accm_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "count_V", "Type" : "OVld", "Direction" : "IO"},
			{"Name" : "done_accm", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0", "DependentChanDepth" : "256", "DependentChanType" : "0",
				"BlockSignal" : [
					{"Name" : "done_accm_blk_n", "Type" : "RtlSignal"}]}]}]}


set ArgLastReadFirstWriteLatency {
	accumulate_1_s {
		numzeros_out {Type I LastRead 0 FirstWrite -1}
		summation_V {Type IO LastRead -1 FirstWrite -1}
		accm {Type O LastRead -1 FirstWrite 6}
		count_V {Type IO LastRead -1 FirstWrite -1}
		done_accm {Type O LastRead -1 FirstWrite 2}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "6", "Max" : "6"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	numzeros_out { ap_fifo {  { numzeros_out_dout fifo_data 0 5 }  { numzeros_out_empty_n fifo_status 0 1 }  { numzeros_out_read fifo_update 1 1 } } }
	accm { ap_fifo {  { accm_din fifo_data 1 32 }  { accm_full_n fifo_status 0 1 }  { accm_write fifo_update 1 1 } } }
	done_accm { ap_fifo {  { done_accm_din fifo_data 1 1 }  { done_accm_full_n fifo_status 0 1 }  { done_accm_write fifo_update 1 1 } } }
}
