// ==============================================================
// Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
// control
// 0x00 : Control signals
//        bit 0  - ap_start (Read/Write/COH)
//        bit 1  - ap_done (Read)
//        bit 2  - ap_idle (Read)
//        bit 3  - ap_ready (Read)
//        bit 4  - ap_continue (Read/Write/SC)
//        bit 7  - auto_restart (Read/Write)
//        others - reserved
// 0x08 : Global Interrupt Enable Register
//        bit 0  - Global Interrupt Enable (Read/Write)
//        others - reserved
// 0x10 : IP Interrupt Enable Register (Read/Write)
//        bit 0  - enable ap_done interrupt (Read/Write)
//        bit 1  - enable ap_ready interrupt (Read/Write)
//        others - reserved
// 0x18 : IP Interrupt Status Register (Read/TOW)
//        bit 0  - ap_done (COR/TOW)
//        bit 1  - ap_ready (COR/TOW)
//        others - reserved
// 0x20 : Data signal of input_s
//        bit 63~0 - input_s[63:0] (Read/Write)
// 0x28 : reserved
// 0x30 : Data signal of N_s
//        bit 63~0 - N_s[63:0] (Read/Write)
// 0x38 : reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define XHYPERLOGLOG_TOP_CONTROL_ADDR_AP_CTRL      0x00
#define XHYPERLOGLOG_TOP_CONTROL_ADDR_GIE          0x08
#define XHYPERLOGLOG_TOP_CONTROL_ADDR_IER          0x10
#define XHYPERLOGLOG_TOP_CONTROL_ADDR_ISR          0x18
#define XHYPERLOGLOG_TOP_CONTROL_ADDR_INPUT_S_DATA 0x20
#define XHYPERLOGLOG_TOP_CONTROL_BITS_INPUT_S_DATA 64
#define XHYPERLOGLOG_TOP_CONTROL_ADDR_N_S_DATA     0x30
#define XHYPERLOGLOG_TOP_CONTROL_BITS_N_S_DATA     64

