set moduleName murmur3_1_66
set isTopModule 0
set isCombinational 0
set isDatapathOnly 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {murmur3<1>66}
set C_modelType { void 0 }
set C_modelArgList {
	{ dataFifo_V_data_V_11 int 32 regular {fifo 0 volatile } {global 0}  }
	{ dataFifo_V_valid_V_11 int 1 regular {fifo 0 volatile } {global 0}  }
	{ dataFifo_V_last_V_11 int 1 regular {fifo 0 volatile } {global 0}  }
	{ hashFifo_6 int 64 regular {fifo 1 volatile } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "dataFifo_V_data_V_11", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "dataFifo_V_valid_V_11", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "dataFifo_V_last_V_11", "interface" : "fifo", "bitwidth" : 1, "direction" : "READONLY", "extern" : 0} , 
 	{ "Name" : "hashFifo_6", "interface" : "fifo", "bitwidth" : 64, "direction" : "WRITEONLY", "extern" : 0} ]}
# RTL Port declarations: 
set portNum 22
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ start_full_n sc_in sc_logic 1 signal -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ dataFifo_V_data_V_11_dout sc_in sc_lv 32 signal 0 } 
	{ dataFifo_V_data_V_11_empty_n sc_in sc_logic 1 signal 0 } 
	{ dataFifo_V_data_V_11_read sc_out sc_logic 1 signal 0 } 
	{ dataFifo_V_valid_V_11_dout sc_in sc_lv 1 signal 1 } 
	{ dataFifo_V_valid_V_11_empty_n sc_in sc_logic 1 signal 1 } 
	{ dataFifo_V_valid_V_11_read sc_out sc_logic 1 signal 1 } 
	{ dataFifo_V_last_V_11_dout sc_in sc_lv 1 signal 2 } 
	{ dataFifo_V_last_V_11_empty_n sc_in sc_logic 1 signal 2 } 
	{ dataFifo_V_last_V_11_read sc_out sc_logic 1 signal 2 } 
	{ hashFifo_6_din sc_out sc_lv 64 signal 3 } 
	{ hashFifo_6_full_n sc_in sc_logic 1 signal 3 } 
	{ hashFifo_6_write sc_out sc_logic 1 signal 3 } 
	{ start_out sc_out sc_logic 1 signal -1 } 
	{ start_write sc_out sc_logic 1 signal -1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "start_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "start_full_n", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "dataFifo_V_data_V_11_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "dataFifo_V_data_V_11", "role": "dout" }} , 
 	{ "name": "dataFifo_V_data_V_11_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataFifo_V_data_V_11", "role": "empty_n" }} , 
 	{ "name": "dataFifo_V_data_V_11_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataFifo_V_data_V_11", "role": "read" }} , 
 	{ "name": "dataFifo_V_valid_V_11_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dataFifo_V_valid_V_11", "role": "dout" }} , 
 	{ "name": "dataFifo_V_valid_V_11_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataFifo_V_valid_V_11", "role": "empty_n" }} , 
 	{ "name": "dataFifo_V_valid_V_11_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataFifo_V_valid_V_11", "role": "read" }} , 
 	{ "name": "dataFifo_V_last_V_11_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":1, "type": "signal", "bundle":{"name": "dataFifo_V_last_V_11", "role": "dout" }} , 
 	{ "name": "dataFifo_V_last_V_11_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataFifo_V_last_V_11", "role": "empty_n" }} , 
 	{ "name": "dataFifo_V_last_V_11_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "dataFifo_V_last_V_11", "role": "read" }} , 
 	{ "name": "hashFifo_6_din", "direction": "out", "datatype": "sc_lv", "bitwidth":64, "type": "signal", "bundle":{"name": "hashFifo_6", "role": "din" }} , 
 	{ "name": "hashFifo_6_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "hashFifo_6", "role": "full_n" }} , 
 	{ "name": "hashFifo_6_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "hashFifo_6", "role": "write" }} , 
 	{ "name": "start_out", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "start_out", "role": "default" }} , 
 	{ "name": "start_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "start_write", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3", "4", "5", "6"],
		"CDFG" : "murmur3_1_66",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "1", "ap_start" : "1", "ap_ready" : "1", "ap_done" : "1", "ap_continue" : "1", "ap_idle" : "1", "real_start" : "1",
		"Pipeline" : "Aligned", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "1",
		"VariableLatency" : "0", "ExactLatency" : "22", "EstimateLatencyMin" : "22", "EstimateLatencyMax" : "22",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "1",
		"HasNonBlockingOperation" : "1",
		"Port" : [
			{"Name" : "dataFifo_V_data_V_11", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0", "DependentChanDepth" : "256", "DependentChanType" : "0",
				"BlockSignal" : [
					{"Name" : "dataFifo_V_data_V_11_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataFifo_V_valid_V_11", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0", "DependentChanDepth" : "256", "DependentChanType" : "0",
				"BlockSignal" : [
					{"Name" : "dataFifo_V_valid_V_11_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "dataFifo_V_last_V_11", "Type" : "Fifo", "Direction" : "I", "DependentProc" : "0", "DependentChan" : "0", "DependentChanDepth" : "256", "DependentChanType" : "0",
				"BlockSignal" : [
					{"Name" : "dataFifo_V_last_V_11_blk_n", "Type" : "RtlSignal"}]},
			{"Name" : "hashFifo_6", "Type" : "Fifo", "Direction" : "O", "DependentProc" : "0", "DependentChan" : "0", "DependentChanDepth" : "256", "DependentChanType" : "0",
				"BlockSignal" : [
					{"Name" : "hashFifo_6_blk_n", "Type" : "RtlSignal"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mul_32s_31s_32_5_1_U215", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mul_32s_30ns_32_5_1_U216", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mul_32s_30ns_32_5_1_U217", "Parent" : "0"},
	{"ID" : "4", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mul_32s_30s_32_5_1_U218", "Parent" : "0"},
	{"ID" : "5", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mul_32s_32s_32_5_1_U219", "Parent" : "0"},
	{"ID" : "6", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.mul_32s_31s_32_5_1_U220", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	murmur3_1_66 {
		dataFifo_V_data_V_11 {Type I LastRead 0 FirstWrite -1}
		dataFifo_V_valid_V_11 {Type I LastRead 0 FirstWrite -1}
		dataFifo_V_last_V_11 {Type I LastRead 0 FirstWrite -1}
		hashFifo_6 {Type O LastRead -1 FirstWrite 22}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "22", "Max" : "22"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "1"}
]}

set PipelineEnableSignalInfo {[
	{"Pipeline" : "0", "EnableSignal" : "ap_enable_pp0"}
]}

set Spec2ImplPortList { 
	dataFifo_V_data_V_11 { ap_fifo {  { dataFifo_V_data_V_11_dout fifo_data 0 32 }  { dataFifo_V_data_V_11_empty_n fifo_status 0 1 }  { dataFifo_V_data_V_11_read fifo_update 1 1 } } }
	dataFifo_V_valid_V_11 { ap_fifo {  { dataFifo_V_valid_V_11_dout fifo_data 0 1 }  { dataFifo_V_valid_V_11_empty_n fifo_status 0 1 }  { dataFifo_V_valid_V_11_read fifo_update 1 1 } } }
	dataFifo_V_last_V_11 { ap_fifo {  { dataFifo_V_last_V_11_dout fifo_data 0 1 }  { dataFifo_V_last_V_11_empty_n fifo_status 0 1 }  { dataFifo_V_last_V_11_read fifo_update 1 1 } } }
	hashFifo_6 { ap_fifo {  { hashFifo_6_din fifo_data 1 64 }  { hashFifo_6_full_n fifo_status 0 1 }  { hashFifo_6_write fifo_update 1 1 } } }
}
